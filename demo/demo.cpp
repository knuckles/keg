#include <iostream>
#include <memory>
#include <vector>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/trigonometric.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <Keg.hpp>
#include <kgl/VertexBuffer.hpp>
#include <kgl/Material.hpp>
#include <kgl/Mesh.hpp>
#include <kgl/Program.hpp>
#include <kgl/Texture.hpp>
#include <sg/debug.hpp>
#include <sg/CameraNode.hpp>
#include <extras/Loader.hpp>
#include <render/Renderer.hpp>
#include <sg/MeshNode.hpp>
#include <sg/Model.hpp>
#include <sg/Scene.hpp>
#include <sg/Skeleton.hpp>
#include <ui/Font.hpp>
#include <ui/Text.hpp>
#include <util/enums.hpp>
#include <util/FormattingBuffer.hpp>


namespace res {
const int WIDTH = 800;
const int HEIGHT = 600;

static const std::vector<float> TRIANGLE{
   -1.0f, -1.0f, 0.0f,
   1.0f, -1.0f, 0.0f,
   0.0f,  1.0f, 0.0f,
};

const keg::gl::StrideLayout::AttributesList TRI_VERTICES{
    {keg::gl::vertex_attributes::POSITIONS, 3}  // 3 coordinates.
};
const std::vector<float> CUBE{
    -1.0f,-1.0f,-1.0f, // triangle 1 : begin
    -1.0f,-1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f, // triangle 1 : end
    1.0f, 1.0f,-1.0f, // triangle 2 : begin
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f, // triangle 2 : end
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f
};
const keg::gl::StrideLayout::AttributesList VERTEX_COLORS{
    {keg::gl::vertex_attributes::COLORS, 3}  // 3 color components starting at 0 of each row.
};
const std::vector<float> CUBE_COLORS{
    0.583f,  0.771f,  0.014f,
    0.609f,  0.115f,  0.436f,
    0.327f,  0.483f,  0.844f,
    0.822f,  0.569f,  0.201f,
    0.435f,  0.602f,  0.223f,
    0.310f,  0.747f,  0.185f,
    0.597f,  0.770f,  0.761f,
    0.559f,  0.436f,  0.730f,
    0.359f,  0.583f,  0.152f,
    0.483f,  0.596f,  0.789f,
    0.559f,  0.861f,  0.639f,
    0.195f,  0.548f,  0.859f,
    0.014f,  0.184f,  0.576f,
    0.771f,  0.328f,  0.970f,
    0.406f,  0.615f,  0.116f,
    0.676f,  0.977f,  0.133f,
    0.971f,  0.572f,  0.833f,
    0.140f,  0.616f,  0.489f,
    0.997f,  0.513f,  0.064f,
    0.945f,  0.719f,  0.592f,
    0.543f,  0.021f,  0.978f,
    0.279f,  0.317f,  0.505f,
    0.167f,  0.620f,  0.077f,
    0.347f,  0.857f,  0.137f,
    0.055f,  0.953f,  0.042f,
    0.714f,  0.505f,  0.345f,
    0.783f,  0.290f,  0.734f,
    0.722f,  0.645f,  0.174f,
    0.302f,  0.455f,  0.848f,
    0.225f,  0.587f,  0.040f,
    0.517f,  0.713f,  0.338f,
    0.053f,  0.959f,  0.120f,
    0.393f,  0.621f,  0.362f,
    0.673f,  0.211f,  0.457f,
    0.820f,  0.883f,  0.371f,
    0.982f,  0.099f,  0.879f
};  // CUBE_COLORS
const char CUBE_TEXTURE_PATH[] = "demo/res/cube_uv.tga";
const std::vector<float> CUBE_UVS{
    0.000059f, 1 - 0.000004f,
    0.000103f, 1 - 0.336048f,
    0.335973f, 1 - 0.335903f,
    1.000023f, 1 - 0.000013f,
    0.667979f, 1 - 0.335851f,
    0.999958f, 1 - 0.336064f,
    0.667979f, 1 - 0.335851f,
    0.336024f, 1 - 0.671877f,
    0.667969f, 1 - 0.671889f,
    1.000023f, 1 - 0.000013f,
    0.668104f, 1 - 0.000013f,
    0.667979f, 1 - 0.335851f,
    0.000059f, 1 - 0.000004f,
    0.335973f, 1 - 0.335903f,
    0.336098f, 1 - 0.000071f,
    0.667979f, 1 - 0.335851f,
    0.335973f, 1 - 0.335903f,
    0.336024f, 1 - 0.671877f,
    1.000004f, 1 - 0.671847f,
    0.999958f, 1 - 0.336064f,
    0.667979f, 1 - 0.335851f,
    0.668104f, 1 - 0.000013f,
    0.335973f, 1 - 0.335903f,
    0.667979f, 1 - 0.335851f,
    0.335973f, 1 - 0.335903f,
    0.668104f, 1 - 0.000013f,
    0.336098f, 1 - 0.000071f,
    0.000103f, 1 - 0.336048f,
    0.000004f, 1 - 0.671870f,
    0.336024f, 1 - 0.671877f,
    0.000103f, 1 - 0.336048f,
    0.336024f, 1 - 0.671877f,
    0.335973f, 1 - 0.335903f,
    0.667969f, 1 - 0.671889f,
    1.000004f, 1 - 0.671847f,
    0.667979f, 1 - 0.335851f,
};  // CUBE_UVS
const keg::gl::StrideLayout::AttributesList CUBE_UV_ATTRS{
    {keg::gl::vertex_attributes::UVS, 2}  // Just U & V coordinates
};

std::string_view shadingModeToStr(keg::render::ShadingMode mode) {
  using M = keg::render::ShadingMode;
  switch (mode){
  case M::MATERIAL: return "material";
  case M::VIS_NORMALS: return "normals";
  case M::VIS_POSITIONS: return "positions";
  default: return "UNKNOWN";
  }
}

}  // namespace res


namespace {
using namespace res;

void demo() {
  // TODO: This uses fixed dimensions but the window size may change later!
  keg::Keg engine(WIDTH, HEIGHT, "run/logs");

  // This is to allow our VS to set size of debug points.
  // TODO: This is too low level to do here.
  // ::gl::glEnable(::gl::GL_VERTEX_PROGRAM_POINT_SIZE);

  bool running = true;
  keg::render::ShadingMode shadingMode = keg::render::ShadingMode::MATERIAL;
  bool lighting = true;

  // == Key-bindings
  {
    using namespace std::chrono_literals;
    engine.addInputCallback(SDLK_ESCAPE, [&running]() { running = false; });
    engine.addInputCallback(SDLK_p, [&engine]() { engine.logicClock().togglePaused(); });
    engine.addInputCallback(SDLK_LEFTBRACKET, [&engine]() { engine.logicClock().adjust(-50ms); });
    engine.addInputCallback(SDLK_RIGHTBRACKET, [&engine]() { engine.logicClock().adjust(50ms); });
    engine.addInputCallback(SDLK_m, [&engine, &shadingMode]() {
      shadingMode = keg::util::stepEnumValue(shadingMode);
      if (shadingMode == keg::render::ShadingMode::INVALID) {
        shadingMode = keg::render::ShadingMode::MATERIAL;
      }
      engine.renderer().setShadingMode(shadingMode);
    });
    engine.addInputCallback(SDLK_l, [&engine, &lighting]() {
      engine.renderer().useLighting(lighting = !lighting);
    });
  }

  // Create the main scene.
  auto world = std::make_shared<keg::sg::Scene>("World");

  // == A textured 1-unit cube. Just to demo manual construction of a simple node.
  auto cubeNode = std::make_shared<keg::sg::MeshNode>(
      std::make_shared<keg::gl::Mesh>(
          keg::gl::PrimitiveKind::Triangles,
          std::vector{
            std::make_shared<keg::gl::VertexBuffer>(absl::MakeConstSpan(CUBE), TRI_VERTICES),
            std::make_shared<keg::gl::VertexBuffer>(absl::MakeConstSpan(CUBE_UVS), CUBE_UV_ATTRS),
            std::make_shared<keg::gl::VertexBuffer>(absl::MakeConstSpan(CUBE_COLORS), VERTEX_COLORS)
          }
      ),
      std::make_shared<keg::gl::Material>(
          "cube material",
          keg::gl::MaterialProperties{},
          keg::gl::MaterialTextures{
            {keg::gl::TextureKind::Diffuse, std::make_shared<keg::gl::Texture>(CUBE_TEXTURE_PATH)}
          }
      )
  );
  cubeNode->setPosition({0, 0, 0});
  cubeNode->setScale(glm::vec3{0.2f});
  world->root()->addChild(cubeNode);


  // == Load some animated models from files.

  {
    // NOTE: The Up-axis is wrong.
    keg::sg::Loader loader("demo/res/SB_WhipperNude/WhipperNude.fbx");
    auto girl = loader.loadModel();
    const auto girlAnimations = loader.loadAnimations();
    if (girlAnimations.size()) {
      girl->skeleton()->addAnimation(girlAnimations.front());
    }
    girl->setPosition({0, 0, 1});
    girl->setRotation({-3.14 / 2, 0, 0});
    world->root()->addChild(girl);
  }

  {
    keg::sg::Loader loader("demo/res/Alien Animal.dae");
    auto beast = loader.loadModel();
    const auto animations = loader.loadAnimations();
    if (animations.size()) {
      beast->skeleton()->addAnimation(animations.front());
    }
    beast->setPosition({0, 0, -2});
    beast->setScale({0.1, 0.1, 0.1});
    world->root()->addChild(beast);
  }

  {
    // Walking wolf
    keg::sg::Loader loader("demo/res/wolf/Wolf.dae");
    auto wolf1 = loader.loadModel();
    const auto animations = loader.loadAnimations();
    if (animations.size()) {
      wolf1->skeleton()->addAnimation(animations.front());
    }
    wolf1->setPosition({1, 0, 2});
    world->root()->addChild(wolf1);
  }

  {
    // Standing wolf
    keg::sg::Loader loader("demo/res/wolf/Wolf_One.dae");
    auto wolf2 = loader.loadModel();
    const auto animations = loader.loadAnimations();
    if (animations.size()) {
      wolf2->skeleton()->addAnimation(animations.front());
    }
    wolf2->setPosition({-1, 0, 2});
    world->root()->addChild(wolf2);
  }


  // == Set up camera and rendering.
  auto camera = world->camera();
  camera->setPosition(glm::vec3{0, 1, 5});
  // FIXME: camera->lookAt has no effect!
  camera->lookAt(glm::vec3{0, 0, 0}, /*upVector = */ glm::vec3{0, 1, 0});
  camera->setControler(engine.createFreeFlyCameraController());

  engine.addRenderPass({
    .kind = keg::RenderPass::Kind::World,
    .scene = world,
    .projection = keg::PerspectiveView{
      .verticalFieldOfView = 45.0f,
      .nearCullPlane = 0.01f,
      .farCullPlane = 1000.0f
    }
  });


  // == Add HUD (debug text)
  auto hud = std::make_shared<keg::sg::Scene>("HUD");

  // TODO: These paths are crap. Do something about it.
  const auto uiFont =
    std::make_shared<keg::ui::Font>("engine/res/fonts/font.json", "engine/res/fonts/font-msdf.png");

  keg::util::FormattingBuffer debugInfoTextBuf(100);
  auto debugInfoLabel =
      std::make_shared<keg::ui::Text>(uiFont, 16, debugInfoTextBuf.capacity());
  engine.addPerfCallback(
    [&] (float updateTime, float renderTime, int fps) {
      debugInfoTextBuf.clear();
      debugInfoTextBuf.append(" * Shading: {}", shadingModeToStr(shadingMode));
      debugInfoTextBuf.append(" * Lights: {}", lighting ? "yes" : "no");
      debugInfoTextBuf.append(" * FPS: {}", fps);
      debugInfoTextBuf.append(" * Update: {:.2f}ms", updateTime);
      debugInfoTextBuf.append(" * Render: {:.2f}ms", renderTime);
      debugInfoLabel->setText(debugInfoTextBuf.contents());
    }
  );

  // FIXME: Colored text looks weird.
  // debugInfoLabel->setColor({1, 0.3, 0.3, 1});
  // HUD render pass projects positions as pixels.
  debugInfoLabel->setPosition({5, 10, 0});  // Bottom left corner.
  hud->root()->addChild(debugInfoLabel);

  engine.addRenderPass({
    .kind = keg::RenderPass::Kind::HUD,
    .scene = hud,
    .projection = keg::OrthogonalView{}
  });

  // == Start running.
  engine.enterMainLoop(running);
}  // demo()

}  // namespace


int main(int, const char**) {
  demo();
  return 0;
}
