# This is intended for some CMake utility functions.


# Sets a few things common to all KEG modules:
# - compilation as position independent code;
# - public include directory pointing to the project root;
# - KEG_DEBUG preprocessor definition in debug builds.
function(setup_keg_module target)
  get_target_property(target_type ${target} TYPE)

  if (NOT target_type STREQUAL "INTERFACE_LIBRARY")
    set_property(
      TARGET ${target}

      PROPERTY
      POSITION_INDEPENDENT_CODE ON
    )

    target_compile_definitions(${target}
      PRIVATE
      $<$<CONFIG:Debug>:KEG_DEBUG>
    )
  endif()

  if (target_type STREQUAL "INTERFACE_LIBRARY")
    set(include_kind INTERFACE)
  else()
    set(include_kind PUBLIC)
  endif()

  target_include_directories(
    ${target}
    ${include_kind}
      "${PROJECT_SOURCE_DIR}/engine"
  )
endfunction()


# Declares a new Keg module. Underneath this is a library target.
# After creation, `setup_keg_module` is called on the target.
#
# @param NAME - taget name. Final target name is prepended with "keg_" prefix.
# NOTE: An ALIAS target named keg::${NAME} is also created.
# @param TYPE - library type, STATIC by default.
# @param SOURCES - list of source file names.
# @param LINK_PRIVATE, LINK_PUBLIC, LINK_INTERFACE - lists of linked libraries.
# NOTE: Each module implicitly receives fmt and g3log libraries as linked privately.
# @param DEPS - list of other dependencies
function(keg_module)
  cmake_parse_arguments(
    PARSE_ARGV 0
    KM  # Prefix.
    ""  # Flag args.
    "NAME;TYPE"  # Single value args.
    "SOURCES;LINK_PRIVATE;LINK_PUBLIC;LINK_INTERFACE;DEPS"  # Multi-value args.
  )

  if (NOT KM_NAME)
    message(SEND_ERROR "Target requires a name.")
  endif()

  if (NOT KM_TYPE)
    set(KM_TYPE STATIC)
  endif()

  add_library(
    keg_${KM_NAME} ${KM_TYPE}
    ${KM_SOURCES}
  )
  add_library(keg::${KM_NAME} ALIAS keg_${KM_NAME})

  target_link_libraries(
    keg_${KM_NAME}
    PRIVATE
      fmt::fmt
      g3log::g3log
      ${KM_LINK_PRIVATE}
    PUBLIC ${KM_LINK_PUBLIC}
    INTERFACE ${KM_LINK_INTERFACE}
  )

  if(KM_DEPS)
    add_dependencies(keg_${KM_NAME} ${KM_DEPS})
  endif()

  setup_keg_module(keg_${KM_NAME})
endfunction()


function(keg_test)
  cmake_parse_arguments(
    PARSE_ARGV 0
    KT  # Prefix.
    ""  # Flag args.
    "NAME"  # Single value args.
    "SOURCES;DEPS"  # Multi-value args.
  )

  if (NOT KT_NAME)
    message(SEND_ERROR "Target requires a name.")
  endif()

  add_executable(${KT_NAME} ${KT_SOURCES})
  target_link_libraries(
    ${KT_NAME}
    PRIVATE ${KT_DEPS}
  )
  add_test(NAME ${KT_NAME} COMMAND ${KT_NAME})
endfunction()
