# A few variables to configure our shaders.

set(SHADER_DEFS
  # Global processing limits.
  MAX_BONES_PER_MESH=200
  MAX_LIGHTS=30

  # Moding constants.
  SHADING_MODE_MATERIAL=0  # Normal mode. Shade based on material properties.
  SHADING_MODE_VIS_NORMALS=1  # Visualize normals with color.
  SHADING_MODE_VIS_POSITION=2  # Visualize normals with color.
  # ...

  # NOTE: Some say that NVidia reserves specific indices for built-in attributes. See:
  # https://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/attributes.php (404)

  # Vertex shader input locations.
  VS_LOC_VPOS=0
  VS_LOC_VCOL=1
  VS_LOC_VNORM=2
  VS_LOC_VUV=3
  VS_LOC_BONE_IDS=4
  VS_LOC_BONE_WEIGHTS=5
)
