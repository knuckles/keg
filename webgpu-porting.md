# Porting to WebGPU

WebGPU as an API is quite different from OpenGL. One notable difference that seems to matter most
for the current code base is the notion of command buffers, through which instructions are sent over
to the GPU. This is radically different from OpenGL calls which are executed immediately (or at
least they pretend to).

Although a lot of OpenGL specific code is concentrated in the keg_gl library, some of the typedefs
and APIs leak outside. Other parts of the engine also lean heavily on our GL wrapper library, so it
is a major incenvenience to reimplement them for a completely different underlying graphics API.

Taking all the things above into consideration, here's one possible way forward:

1. [ ] Refactor current rendering code to hide OpenGL calls behind internal abstractions
    - [x] Introduce the concept of render passes
    - [x] Integrate Text into the scene graph
2. [ ] Restructure/extend existing OpenGL wrappers to resemble more modern APIs
    - [ ] add the concept of a command buffer, get away from "immediate effect" calls
3. [ ] Build a facade API and hide OpenGL behind it.
4. [ ] Build a WebGPU backend for that API.
5. [ ] Switch over when needed.
