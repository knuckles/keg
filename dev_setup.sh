#!/usr/bin/env bash

set -eu
set -o pipefail


PROJ_DIR=`readlink -f $( dirname -- "$0"; )`
BUILD_DIR_NAME=".build"

cd "${PROJ_DIR}"
git submodule update --init --recursive

mkdir "${BUILD_DIR_NAME}"
cd "${BUILD_DIR_NAME}"

cmake \
  -G Ninja \
  -D CMAKE_BUILD_TYPE=RelWithDebInfo \
  ..
