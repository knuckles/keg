# KEG (K Engine for Games)

An experiment to try to implement/compose a 3D game engine based on OpenGL programmable
pipeline and a scene graph.

# Features
- Scene-graph based rendering (pretty basic in term of efficiency at the moment);
- 3D-model formats support via ASSIMP and SOIL libraries;
- Skeletal animations on GPU using vertex shaders;
- Rendering using vertex buffers (VBOs/VAOs);
- Scalable text rendering using signed distance fields.

# Design goals
- Very simple, light-weight abstractions over OpenGL;
- Open, extensible, embeddable (this is not a framework);
- Having fun from exploration and experimentation.

# Building

## Dependencies

The following tools and libraries are requied to build:

 - C++17 compiler
 - [CMake](https://cmake.org/) (recommended with [Ninja](https://ninja-build.org/))
 - [OpenGL](https://www.opengl.org/) (version 3.3 compatible)
 - [Conan](https://docs.conan.io/) v.1! (´pip install "conan<2"´)

KEG uses Conan package manager to install most of its external dependencies. Other
libraries are attached via Git submodules.

Once the global dependencies are installed, prepare the build directory, pull in the
rest of the libraries, and configure the build by running the `dev_setup.sh` script.
Alternatively, you may run a few commands manually:

```bash
git submodule update --init --recursive
mkdir .build && cd .build
cmake ..
```

Note that CMake will automatically run `conan install` for you. In case it fails telling you that
there are no pre-built binaries available for your platform, you may need to tune your
[conan profile](https://docs.conan.io/1/reference/profiles.html), or run `conan install` manually
with `--build missing` from within the build directory (`conan` subdir is recommended).
For example here's the profile that worked last time:

```ini
[settings]
os=Macos
os_build=Macos
arch=x86_64
arch_build=x86_64
compiler=apple-clang
compiler.version=13.0
compiler.libcxx=libc++
build_type=Release

[options]
[build_requires]
[env]
```


To build:

```bash
cd .build
cmake --build .
```

# Code Style
Use values from `.editorconfig` for the basic text formatting rules. Most importantly:

- Line length: 100
- Tabs: never
- Brace style: "egyptian"
- Indentation: 2 spaces; namespace contents and class access modifiers not indented.

## Naming
- Namespaces: lower_case.
- Classes, enums, unions: PascalCase.
- Variables: camelCase.
- Globals: CAPITAL_CASE.

Files:

  - either exactly as the enclosed class name - i.e. PascalCase,
  - or lower_cased, when no single primary entity inside.
  - C++ file extensions: .cpp/.hpp (.h as legacy).
  - Directories: lower_case, following the enclosed namespace.
