#include "Font.hpp"

#include <fstream>
#include <stdexcept>

#include <nlohmann/json.hpp>

#include <kgl/Texture.hpp>


namespace keg::ui {

namespace {
using json = nlohmann::json;

}  // namespace

void from_json(const json& j, Font::AtlasInfo& atlas) {
  atlas.type = [](const std::string& typeStr) {
    if (typeStr == "hardmask")
      return Font::AtlasInfo::Type::HARD_MASK;

    if (typeStr == "softmask")
      return Font::AtlasInfo::Type::SOFT_MASK;

    if (typeStr == "sdf")
      return Font::AtlasInfo::Type::SDF;

    if (typeStr == "psdf")
      return Font::AtlasInfo::Type::PSDF;

    if (typeStr == "msdf")
      return Font::AtlasInfo::Type::MSDF;

    if (typeStr == "mtsdf")
      return Font::AtlasInfo::Type::MTSDF;

    throw std::invalid_argument("Unknown font atlas type: " + typeStr);
  }(j.at("type"));

  atlas.glyphId = Font::AtlasInfo::GlyphId::UNICODE_CODEPOINT;  // FIXME: Must be detected.
  atlas.size = j.at("size");
  atlas.width = j.at("width");
  atlas.height = j.at("height");
  atlas.distanceRange = j.value("distanceRange", 0);
}

void from_json(const json& j, Font::GlyphMetrics::Box& box) {
  box.left = j.at("left");
  box.bottom = j.at("bottom");
  box.right = j.at("right");
  box.top = j.at("top");
}

void from_json(const json& j, Font::GlyphMetrics& gm) {
  gm.id = j.contains("unicode") ? j.at("unicode") : j.at("index");
  gm.advance = j.at("advance");
  if (j.contains("planeBounds"))
    gm.quad = std::make_optional<Font::GlyphMetrics::Box>(j.at("planeBounds"));
  if (j.contains("atlasBounds"))
    gm.atlas = std::make_optional<Font::GlyphMetrics::Box>(j.at("atlasBounds"));
}

void from_json(const json& j, Font::FontMetrics& fm) {
  fm.ascenderPos = j.at("ascender");
  fm.descenderPos = j.at("descender");
  fm.lineHeight = j.at("lineHeight");
  fm.underlinePos = j.at("underlineY");
  fm.underlineThickness = j.at("underlineThickness");
}


Font::Font(const std::string_view metaPath, const std::string_view atlasPath) {
  std::ifstream metaFile(metaPath);
  json meta;
  metaFile >> meta;

  _atlasInfo = meta.at("atlas");
  _metrics = meta.at("metrics");

  const json& glyphsMeta = meta.at("glyphs");
  if (!glyphsMeta.is_array())
    throw std::runtime_error("Glyphs must be a JSON array.");

  for (const json& glyph : glyphsMeta) {
    GlyphMetrics gm = glyph;
    _glyphs[gm.id] = std::move(gm);
  }

  const json& kerningMeta = meta.at("kerning");
  if (!kerningMeta.is_array())
    throw std::runtime_error("Kerning info must be a JSON array.");

  for (const json& kerningInfo : kerningMeta) {
    const uint32_t c1 = kerningInfo.at("unicode1");
    const uint32_t c2 = kerningInfo.at("unicode2");
    _kerning[{c1, c2}] = kerningInfo.at("advance");
  }

  // Verify texture size and other metadata.
  _atlas = std::make_unique<gl::Texture>(atlasPath);
  // NOTE: SOIL may enlarge the texture to the power-of-two sizes if required by the hardware.
  assert(_atlas->width() >= _atlasInfo.width);
  assert(_atlas->height() >= _atlasInfo.height);
}

double Font::kerning(const uint32_t prevChar, const uint32_t currChar) const {
  const auto it = _kerning.find({prevChar, currChar});
  return (it == _kerning.cend()) ? 0 : it->second;
}

Font::~Font() = default;

}  // namespace keg::ui
