#pragma once

#include <string>
#include <vector>

#include <glm/matrix.hpp>

#include <sg/MeshNode.hpp>
#include <util/macros.hpp>


namespace keg::gl {
class Mesh;
class Program;
class VertexBuffer;
}  // namespace keg::gl


namespace keg::ui {

class Font;

/// This scene-graph node can render user-provided string of text.
class Text : public sg::MeshNode {
public:
  NON_COPYABLE(Text)
  NON_MOVABLE(Text)

  /// Construct a renderable text object.
  /// @param font - the font to use the glyphs from.
  /// @param size - "point" (or Em) size in pixels.
  /// @param capacity - initial capacity (in characters), used to allocate internal buffer.
  /// @note that @c setText may cause the reallocation of internal buffer to acomodate the string.
  /// This is fine, but undesirable to do frequently from the performance perspective.
  Text(const std::shared_ptr<Font> font, float size, size_t capacity);
  ~Text();

  void setText(std::string_view text);
  void setColor(const glm::vec4& color) { _color = color; }

protected:
  /// @param uniforms - Rendering vars sink. The following uniforms are expected to be settable:
  /// sampler2D fontAtlas
  /// vec2 msdfUnit
  /// vec4 textColor
  void doRender(gl::ShaderUniformsSink& uniforms) const noexcept override;

private:
  gl::VertexBuffer* vbo();

  std::shared_ptr<Font> _font;
  glm::vec2 _msdfUnit;
  float _size = 1.0f;

  std::vector<float> _textMeshData;  // Reusable buffer, to minimize allocations on text change.
  glm::vec4 _color{0, 0, 0, 1};
};

}  // namespace keg::ui
