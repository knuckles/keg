#include "Text.hpp"

#include <iostream>

#include <g3log/g3log.hpp>
#include <glm/vec4.hpp>
#include <glm/gtx/transform.hpp>

#include <kgl/Program.hpp>
#include <kgl/Mesh.hpp>
#include <kgl/Texture.hpp>
#include <kgl/VertexBuffer.hpp>
#include <ui/Font.hpp>

#include <util/fs_utils.hpp>


namespace keg::ui {

namespace {
// FIXME: Exact text rendering shader should probably be passed from the outside.
std::shared_ptr<gl::Program> getTextShader() {
  static const auto p = std::make_shared<gl::Program>(
      gl::Program::ShadersInitList{
        // TODO: Pass resources root path from the outside.
        std::make_shared<gl::VertexShader>(util::readTextFile("engine/res/shaders/text.vert")),
        std::make_shared<gl::FragmentShader>(util::readTextFile("engine/res/shaders/text.frag"))
      }
  );
  return p;
}

static const gl::StrideLayout QUAD_LAYOUT {
  { gl::vertex_attributes::POSITIONS,  2 },  // X, Y
  { gl::vertex_attributes::UVS,        2 },  // U, V
};
static const auto elementsPerVertex = QUAD_LAYOUT.strideLength();
// 2 triangles, each consisting of 3 sets of quad attributes.
constexpr auto verticesPerQuad = 6;
static const auto elementsPerQuad = verticesPerQuad * elementsPerVertex;

// TODO: Need `NotNullPtr` wrapper class.
const std::shared_ptr<Font> validateFont(const std::shared_ptr<Font> font) {
  if (!font)
    throw std::runtime_error("Font is not set");
  return std::move(font);
}

}  // namespace


Text::Text(const std::shared_ptr<Font> font, float size, size_t capacity)
  : MeshNode(
      // Create an empty Mesh object which will later hold quads, one per text character.
      std::make_shared<gl::Mesh>(
        gl::PrimitiveKind::Triangles,
        std::make_shared<gl::VertexBuffer>(QUAD_LAYOUT, gl::glNumericTypeId<::gl::GLfloat>())
      )
    )
  , _font(validateFont(font))
  , _msdfUnit{
      // Actual texture size might have been enlarged to get to power of two dimensions, so we use
      // "formal" atlas dimensions.
      _font->atlasInfo().distanceRange / _font->atlasInfo().width,
      _font->atlasInfo().distanceRange / _font->atlasInfo().height
    }
  , _size(size)
{
  vbo()->reserve(capacity * verticesPerQuad);
}

Text::~Text() = default;

void Text::setText(std::string_view text) {
  const auto& glyphs = _font->glyphs();
  const auto& ai = _font->atlasInfo();

  float x = 0;

  _textMeshData.clear();
  _textMeshData.reserve(text.size() * elementsPerQuad);
  vbo()->reserve(text.size() * verticesPerQuad);

  char prevChar = 0;
  for (const char character : text) {
    const auto glyphMetricsIter = glyphs.find(character);
    if (glyphMetricsIter == glyphs.end()) {
      continue;
    }
    const auto& glyphMetrics = glyphMetricsIter->second;

    // Create a quad for each character.
    if (glyphMetrics.quad && glyphMetrics.atlas) {

      // TODO: Does this even matter?
      if (prevChar) {
        x += _font->kerning(prevChar, character);
      }

      // 1st triange:

      // top-left
      _textMeshData.push_back(x + glyphMetrics.quad->left * _size);
      _textMeshData.push_back(glyphMetrics.quad->top * _size);
      _textMeshData.push_back(glyphMetrics.atlas->left / ai.width);
      _textMeshData.push_back(glyphMetrics.atlas->top / ai.height);

      // top-right
      _textMeshData.push_back(x + glyphMetrics.quad->right * _size);
      _textMeshData.push_back(glyphMetrics.quad->top * _size);
      _textMeshData.push_back(glyphMetrics.atlas->right / ai.width);
      _textMeshData.push_back(glyphMetrics.atlas->top / ai.height);

      // bottom-left
      _textMeshData.push_back(x + glyphMetrics.quad->left * _size);
      _textMeshData.push_back(glyphMetrics.quad->bottom * _size);
      _textMeshData.push_back(glyphMetrics.atlas->left / ai.width);
      _textMeshData.push_back(glyphMetrics.atlas->bottom / ai.height);

      // 2nd triangle:

      // bottom-left (copy)
      std::copy_n(
          _textMeshData.end() - 1 * elementsPerVertex,
          elementsPerVertex,
          std::back_inserter(_textMeshData)
      );

      // top-right (copy)
      std::copy_n(
          _textMeshData.end() - 3 * elementsPerVertex,
          elementsPerVertex,
          std::back_inserter(_textMeshData)
      );

      // bottom-right
      _textMeshData.push_back(x + glyphMetrics.quad->right * _size);
      _textMeshData.push_back(glyphMetrics.quad->bottom * _size);
      _textMeshData.push_back(glyphMetrics.atlas->right / ai.width);
      _textMeshData.push_back(glyphMetrics.atlas->bottom / ai.height);
    }

    // TODO: Support line breaks by adjusting the Y coordinate too.
    x += glyphMetrics.advance * _size;
    prevChar = character;
  }  // for

  // TODO: Check if it's faster to mmap the VBO and write directly into it vertex by vertex.
  const auto overflow = vbo()->setData(gl::DataAdapter{absl::MakeConstSpan(_textMeshData)});
  if (overflow) {
    LOG(WARNING) << "Text VBO overflowed by " << overflow << "bytes";
  }
}

gl::VertexBuffer* Text::vbo() {
  return mesh().vertexData().at(0).get();
}

void Text::doRender(gl::ShaderUniformsSink& uniforms) const noexcept {
  /// So we still want Text node to know that it needs its own shaders in order to draw.
  /// But each node compiling the same shaders over and over again is not an option.
  /// Maybe we should "request" shaders from a shared pool at construction time, so they can be
  /// reused.
  /// At the same time we probably want to expose the shaders in use to the Renderer in order for it
  /// to sort and render nodes according to their shader - i.e. it should be responsible for shader
  /// activation.

  const unsigned textureUnit = 0;  // FIXME: Hardcoded texture unit.
  _font->atlas().use(textureUnit);

  uniforms.setUniformVar("textColor", _color);  // TODO: Use `Material` to set colors and shit.
  uniforms.setUniformVar("msdfUnit", _msdfUnit);
  uniforms.setUniformVar("fontAtlas", ::gl::GLint{textureUnit});

  mesh().draw();
}

}  // namespace keg::ui
