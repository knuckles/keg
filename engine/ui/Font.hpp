#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <string_view>
#include <vector>

#include <msdf-atlas-gen/types.h>


namespace keg::gl {
class Mesh;
class Texture;
}  // namespace keg::gl


namespace keg::ui {

class Font {
public:

  struct AtlasInfo {
    using Type = msdf_atlas::ImageType;
    using GlyphId = msdf_atlas::GlyphIdentifierType;

    Type type;
    GlyphId glyphId;
    double size;
    int width;
    int height;
    float distanceRange;  // Essentially the distance field "size", might be 0.
  };

  /// @note All values are in Ems, or "point" sizes.
  struct FontMetrics {
    double ascenderPos;
    double descenderPos;
    double lineHeight;
    double underlinePos;
    double underlineThickness;
  };

  struct GlyphMetrics {
    struct Box {
      double left;
      double bottom;
      double right;
      double top;
    };

    /// Unicode code point, OR a font glyph index.
    uint32_t id;

    /// Horizontal spacing before the following character.
    double advance;

    /// Relative quad bounds in "point" sizes.
    std::optional<Box> quad;

    /// Absolute glyph quad bounds inside the font atlas.
    std::optional<Box> atlas;
  };

  using Glyphs = std::map<uint32_t, GlyphMetrics>;

  explicit Font(const std::string_view metaPath, const std::string_view atlasPath);
  ~Font();

  const AtlasInfo& atlasInfo() const { return _atlasInfo; }
  const gl::Texture& atlas() const { return *_atlas; }
  const FontMetrics& metrics() const { return _metrics; };
  const Glyphs& glyphs() const { return _glyphs; };
  double kerning(uint32_t prevChar, uint32_t currChar) const;

private:
  using KerningMap = std::map<std::pair<uint32_t, uint32_t>, double>;

  AtlasInfo _atlasInfo;
  FontMetrics _metrics;
  Glyphs _glyphs;
  KerningMap _kerning;
  std::unique_ptr<gl::Texture> _atlas;
};


}  // namespace keg::ui
