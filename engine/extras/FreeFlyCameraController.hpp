#pragma once

#include <memory>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/vec3.hpp>
#include <glm/gtc/constants.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <base/keg_time.hpp>
#include <sg/Node.hpp>


namespace keg::input {
class InputBuffer;
}

namespace keg::sg {

class FreeFlyCameraController : public sg::NodeController {
public:
  FreeFlyCameraController(input::InputBuffer& inputBuffer);

  // Node::Controller implementation:
  void onAttached(const sg::Node* node) override;
  void update(sg::Node* node, const core::FrameTimes& time) override;

private:
  struct Input {
    Input();
    void update(input::InputBuffer& inputBuffer);

    bool forward = false;
    bool back = false;
    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;
    bool turnUp = false;
    bool turnDown = false;
    bool turnLeft = false;
    bool turnRight = false;
    // Additional relative turn signals in the range [0, 1].
    float turnVertBy = 0.0f;
    float turnHorBy = 0.0f;

  private:
    int _screenWidth = 0;
    int _screenHeight = 0;
  };
  Input _input;

  input::InputBuffer& _inputBuffer;
  // TODO: Get these from the node.
  float _horAngle = glm::pi<float>();  // toward -Z
  float _vertAngle = 0.0f;
};

}  // namespace keg::sg
