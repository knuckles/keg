#pragma once

#include <memory>
#include <string_view>
#include <vector>


namespace keg::sg {

class Animation;
class Model;

class Loader {
public:
  Loader(std::string_view filePath);
  ~Loader();

  /// Loads a hierarchy of nodes into GPU memory and returns a scene graph handle to it.
  std::shared_ptr<Model> loadModel();

  std::vector<std::shared_ptr<Animation>> loadAnimations();

private:
  class Guts;  // PImpl kind of thing in order to avoid leaking Assimp types.
  std::unique_ptr<Guts> _guts;
};

}  // namespace sg::keg
