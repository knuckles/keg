#include "FreeFlyCameraController.hpp"

#include <SDL_events.h>
#include <SDL_keycode.h>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/matrix.hpp>
#include <glm/vec3.hpp>
#include <glm/ext/quaternion_float.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <input/InputBuffer.hpp>
#include <sg/Node.hpp>


namespace {
const float MOVE_SPEED = 10.0f;  // units per second
const float TURN_SPEED = 1.0f;  // degrees per second
const float MOUSE_SENSIVITY = 10.0f;
}  // namespace

namespace keg::sg {

FreeFlyCameraController::FreeFlyCameraController(input::InputBuffer& inputBuffer)
  : _inputBuffer{inputBuffer}
{}

void FreeFlyCameraController::onAttached(const sg::Node* node) {
  if (!node)
    return;
}

void FreeFlyCameraController::update(sg::Node* node, const core::FrameTimes& time) {
  _input.update(_inputBuffer);

  glm::vec3 position = node->localPosition();

  if (_input.turnLeft)
    _horAngle += core::toFloatSeconds(time.timeSincePrevFrame) * TURN_SPEED;
  if (_input.turnRight)
    _horAngle -= core::toFloatSeconds(time.timeSincePrevFrame) * TURN_SPEED;

  _horAngle -= _input.turnHorBy * MOUSE_SENSIVITY;
  _vertAngle -= _input.turnVertBy * MOUSE_SENSIVITY;

  if (_input.turnUp)
    _vertAngle -= core::toFloatSeconds(time.timeSincePrevFrame) * TURN_SPEED;
  if (_input.turnDown)
    _vertAngle += core::toFloatSeconds(time.timeSincePrevFrame) * TURN_SPEED;

  const glm::vec3 direction(
      cos(_vertAngle) * sin(_horAngle),
      sin(_vertAngle),
      cos(_vertAngle) * cos(_horAngle)
  );
  const glm::vec3 right = glm::vec3(
      sin(_horAngle - glm::half_pi<float>()),
      0,
      cos(_horAngle - glm::half_pi<float>())
  );
  const glm::vec3 up = glm::cross(right, direction);

  if (_input.forward) {
      position += direction * core::toFloatSeconds(time.timeSincePrevFrame) * MOVE_SPEED;
  }
  if (_input.back) {
      position -= direction * core::toFloatSeconds(time.timeSincePrevFrame) * MOVE_SPEED;
  }
  if (_input.left) {
      position -= right * core::toFloatSeconds(time.timeSincePrevFrame) * MOVE_SPEED;
  }
  if (_input.right) {
      position += right * core::toFloatSeconds(time.timeSincePrevFrame) * MOVE_SPEED;
  }
  if (_input.up) {
      position += up * core::toFloatSeconds(time.timeSincePrevFrame) * (MOVE_SPEED / 2);
  }
  if (_input.down) {
      position -= up * core::toFloatSeconds(time.timeSincePrevFrame) * (MOVE_SPEED / 2);
  }

  node->setPosition(position);
  node->lookAt(position + direction, up);
}

FreeFlyCameraController::Input::Input() {
  SDL_Rect screenRes;
  SDL_GetDisplayBounds(0, &screenRes);
  _screenWidth = screenRes.w;
  _screenHeight = screenRes.h;
}

void FreeFlyCameraController::Input::update(input::InputBuffer& inputBuffer) {
  turnVertBy = 0;
  turnHorBy = 0;
  for (const auto& event : inputBuffer.frameEvents()) {
    switch (event.type) {
      case SDL_MOUSEMOTION: {
        turnHorBy = float(event.motion.xrel) / _screenWidth;
        turnVertBy = float(event.motion.yrel) / _screenHeight;
        break;
      }
      case SDL_KEYDOWN: {
        switch (event.key.keysym.sym) {
          // Camera controls:
          case SDLK_UP:
            forward = true;
            break;
          case SDLK_DOWN:
            back = true;
            break;
          case SDLK_LEFT:
            turnLeft = true;
            break;
          case SDLK_RIGHT:
            turnRight = true;
            break;
          case SDLK_SPACE:
            up = true;
            break;
          case SDLK_LSHIFT:
            down = true;
            break;
          case SDLK_a:
            left = true;
            break;
          case SDLK_d:
            right = true;
            break;
          case SDLK_w:
            forward = true;
            break;
          case SDLK_s:
            back = true;
            break;
        }
        break;
      }  // case SDL_KEYDOWN

      case SDL_KEYUP: {
        switch (event.key.keysym.sym) {
          // Camera controls:
          case SDLK_UP:
            forward = false;
            break;
          case SDLK_DOWN:
            back = false;
            break;
          case SDLK_LEFT:
            turnLeft = false;
            break;
          case SDLK_RIGHT:
            turnRight = false;
            break;
          case SDLK_SPACE:
            up = false;
            break;
          case SDLK_LSHIFT:
            down = false;
            break;
          case SDLK_a:
            left = false;
            break;
          case SDLK_d:
            right = false;
            break;
          case SDLK_w:
            forward = false;
            break;
          case SDLK_s:
            back = false;
            break;
        }  // switch (event.key.keysym.sym)
        break;
      }  // case SDL_KEYUP
    }  // switch (event.type)
  }  // for event loop
}


}  // namespace keg::sg
