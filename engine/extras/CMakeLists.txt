# Batteries.
keg_module(
  NAME extras
  SOURCES
    FreeFlyCameraController.cpp
    FreeFlyCameraController.hpp

    Loader.cpp
    Loader.hpp

  LINK_PRIVATE
    assimp::assimp
    Boost::filesystem
    SDL2pp::SDL2pp

  LINK_PUBLIC
    glm::glm
    keg::base
    keg::gl
    keg::sg
)
