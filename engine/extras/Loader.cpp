#include "Loader.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

#include <fmt/core.h>

#include <assimp/config.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <boost/filesystem.hpp>
#include <g3log/g3log.hpp>

#include <kgl/VertexBuffer.hpp>
#include <kgl/Material.hpp>
#include <kgl/Mesh.hpp>
#include <kgl/Texture.hpp>
#include <sg/Animation.hpp>
#include <sg/debug.hpp>
#include <sg/Node.hpp>
#include <sg/MeshNode.hpp>
#include <sg/Model.hpp>
#include <sg/Skeleton.hpp>
#include <util/containers.hpp>
#include <util/enums.hpp>
#include <util/histogram.hpp>
#include <util/logging.hpp>


namespace {
using namespace keg;
using namespace keg::gl;
using namespace keg::sg;
namespace va = vertex_attributes;
using Attr = StrideLayout::AttributeItem;
using AttrList = StrideLayout::AttributesList;

const double DEFAULT_ANIMATION_TICKS_PER_SEC = 25;

std::string_view fromAssimpString(const aiString& s) {
  return {s.C_Str(), s.length};
}

glm::vec3 assimpToGlmVector(const aiVector3D& src) {
  return glm::vec3{src.x, src.y, src.z};
}

glm::quat assimpToGlmQuat(const aiQuaternion& src) {
  return glm::quat{src.w, src.x, src.y, src.z};
}

MaterialProperties::Color convertAssimpColor(const aiColor3D& c) {
  return {c.r, c.g, c.b};
}

glm::mat4x4 assimpToGlmMatrix(const aiMatrix4x4& src) {
  // TODO: Alternatively:
  // #include <glm/gtc/type_ptr.hpp>
  // return glm::transpose(glm::make_mat4(&src.a1));
  return glm::mat4x4(
      src.a1, src.b1, src.c1, src.d1,
      src.a2, src.b2, src.c2, src.d2,
      src.a3, src.b3, src.c3, src.d3,
      src.a4, src.b4, src.c4, src.d4
  );
}

/// Push Assimp vector structure elements into the destination vector.
template <typename T>
void pushAssimpVector(const aiVector3t<T>& src, std::vector<T>& dest) {
  using StructType = std::remove_reference_t<decltype(src)>;
  constexpr auto numElements = 3;
  constexpr auto firstToLastGap = (numElements - 1) * sizeof(T);
  // This is required becaue for copying we assume all vector elements are next to each other.
  static_assert(offsetof(StructType, x) + firstToLastGap == offsetof(StructType, z));

  std::copy_n(&src.x, numElements, std::back_inserter(dest));
}

/// Push Assimp color structure elements into the destination vector.
template <typename T>
void pushAssimpColor(const aiColor4t<T>& src, std::vector<T>& dest, uint8_t numElements = 4) {
  using StructType = std::remove_reference_t<decltype(src)>;
  constexpr auto firstToLastGap = 3 * sizeof(T);
  // This is required becaue for copying we assume all color elements are next to each other.
  static_assert(offsetof(StructType, r) + firstToLastGap == offsetof(StructType, a));

  std::copy_n(&src.r, numElements, std::back_inserter(dest));
}


#define ASSIMP_PROP_IDENTS_DECL \
  const char* pKey, unsigned int type, unsigned int idx
#define ASSIMP_PROP_IDENTS pKey, type, idx

template <typename T>
T extractMaterialProperty(aiMaterial* const material, ASSIMP_PROP_IDENTS_DECL) {
  T propVal;
  material->Get(ASSIMP_PROP_IDENTS, propVal);
  return std::move(propVal);
}

MaterialProperties::Color
fromColorProperty(aiMaterial* const material, ASSIMP_PROP_IDENTS_DECL) {
  return convertAssimpColor(
      extractMaterialProperty<aiColor3D>(material, ASSIMP_PROP_IDENTS)
  );
}

#undef ASSIMP_PROP_IDENTS_DECL
#undef ASSIMP_PROP_IDENTS

// Some mesh skinning debug code.
// Outputs vertices stats related to their bones to help spot dubious data.
[[maybe_unused]]
void debugCheckSkinningStats(const std::vector<::gl::GLfloat>& weights) {
  util::Histogram<float> boneWeightsStats({0.0f, 0.95f, 1.001f, 1.1f, 2.0f});

  size_t vertices = weights.size() / keg::gl::Mesh::BONES_PER_VERTEX;
  for (size_t vi = 0; vi < vertices; ++vi) {
    const float boneWeightSum = [&]() -> float {
      float total = 0;
      for (size_t wi = 0; wi < Mesh::BONES_PER_VERTEX; ++wi) {
        total += weights[vi * Mesh::BONES_PER_VERTEX + wi];
      }
      return total;
    }();
    boneWeightsStats.putValue(boneWeightSum);
  }
  LOG(DEBUG) << "Skeleton bone weight sums:\n" << boneWeightsStats;
}

struct BoneAccumulator {
  unsigned int addBone(const std::string& name, const glm::mat4x4& offset) {
    // TODO: Implement lookup by string_view key. Here and in the Skeleton (for animations).
    // See https://medium.com/factset/modern-c-in-depth-transparent-comparisons-afef5900535b
    const auto found = indicesMap.find(name);
    if (found != indicesMap.end())
      return found->second.first;
    const auto [it, inserted] = indicesMap.insert({name, {indicesMap.size(), offset}});
    assert(inserted);
    return it->second.first;
  }

  std::map<std::string, std::pair<unsigned int, glm::mat4x4>> indicesMap;
};

/// Convert floating point animation duration to integer-represented ones.
Animation::Channel::Dur toAnimationTime(const double assimpTime, const double ticksPerSec) {
  using D = Animation::Channel::Dur;
  return D{
    D::rep(round(assimpTime * D::period::den / ticksPerSec / D::period::num))
  };
}

}  // namespace


namespace keg::sg {

class Loader::Guts {
public:
  explicit Guts(std::string_view filePath);
  ~Guts() = default;

  std::shared_ptr<Model> loadModelFromRootNode();
  std::vector<std::shared_ptr<Animation>> loadAnimations();

private:
  std::shared_ptr<Node> processAssimpNode(const aiNode* src);
  void loadSkinningData(
      const aiMesh* const assimpMesh,
      std::vector<::gl::GLfloat>& weights,
      std::vector<unsigned int>& indices
  );
  std::shared_ptr<Mesh> loadMesh(const aiMesh* const assimpMesh);
  std::shared_ptr<Texture> loadTexture(
      const aiMaterial* const assimpMaterial, aiTextureType textureType, unsigned textureIndex
  );
  std::shared_ptr<gl::Material> loadMaterial(const unsigned materialIndex);
  std::shared_ptr<sg::MeshNode> processAssimpMesh(
      std::string nodeName, const aiMesh* const assimpMesh
  );

  Assimp::Importer _importer;

  boost::filesystem::path _resourcePath;
  const aiScene* _scene;
  std::map<unsigned int, std::shared_ptr<keg::gl::Material>> _materialsCache;

  BoneAccumulator _bonesAcc;

  boost::filesystem::path _resRootPath;
};  // struct Loader::Guts


void Loader::Guts::loadSkinningData(
    const aiMesh* const assimpMesh,
    std::vector<::gl::GLfloat>& weights,
    std::vector<unsigned int>& indices
) {
  if (!assimpMesh->HasBones())
    return;

  LOG(DEBUG) << "Mesh '" << fromAssimpString(assimpMesh->mName) << "' has "
             << assimpMesh->mNumBones << " bones";

  assert(weights.size() == indices.size());

  for (unsigned int bi = 0; bi < assimpMesh->mNumBones; ++bi) {
    const aiBone* bone = assimpMesh->mBones[bi];
    const auto boneName = std::string{fromAssimpString(bone->mName)};
    const auto boneIndex = _bonesAcc.addBone(boneName, assimpToGlmMatrix(bone->mOffsetMatrix));

    // TODO: Take the most influencing bones for each vertex.
    for (size_t wi = 0; wi < bone->mNumWeights; ++wi) {
      const aiVertexWeight& weight = bone->mWeights[wi];

      // Find a free slot for bone weight and put it there.
      for (size_t wSlot = 0; wSlot < Mesh::BONES_PER_VERTEX; ++wSlot) {
        const auto insertionIndex = Mesh::BONES_PER_VERTEX * weight.mVertexId + wSlot;
        assert(insertionIndex >= 0 && insertionIndex < weights.size());
        if (weights[insertionIndex] != 0.0f)
          continue;  // Slot taken.

        weights[insertionIndex] = weight.mWeight;
        indices[insertionIndex] = boneIndex;
        break;
      }  // for Mesh::BONES_PER_VERTEX
    }
  }  // for assimpMesh->mNumBones

#if defined(KEG_DEBUG)
  debugCheckSkinningStats(weights);
#endif
}  // loadSkinningData

std::shared_ptr<Mesh> Loader::Guts::loadMesh(const aiMesh* const assimpMesh) {
  if (assimpMesh->mPrimitiveTypes != aiPrimitiveType_TRIANGLE)
    throw std::runtime_error("Imported mesh contains not just triangles.");

  // Determine the resulting vertex data buffer layout.
  // NOTE: Having all mesh data in fewer VBOs _drastically_ impoves the performance compared to it
  // being distributed betweed several ones.
  std::vector<Attr> vertex_attributes;
  vertex_attributes.reserve(vertex_attributes::ALL_VATTRIBUTE_IDS.size());  // Max number possible.

  vertex_attributes.push_back({va::POSITIONS, glm::vec3::length()});

  if (assimpMesh->mNormals) {
    vertex_attributes.push_back({va::NORMALS, glm::vec3::length()});
  }

  constexpr unsigned vertColorSet = 0;  // TODO: Handle all AI_MAX_NUMBER_OF_COLOR_SETS.
  const aiColor4D* const colorSet = assimpMesh->mColors[vertColorSet];
  if (colorSet) {
    vertex_attributes.push_back({va::COLORS, glm::vec3::length()});
  }

  constexpr unsigned texCoordsSet = 0;  // TODO: Handle all AI_MAX_NUMBER_OF_TEXTURECOORDS.
  const aiVector3D* const texCoords = assimpMesh->mTextureCoords[texCoordsSet];
  using UVCoordsType = glm::vec2;  // What we use in the shader.
  if (texCoords) {
    // Assimp can load up to 3 texture coordinates, but our shader only supports exactly 2.
    if (assimpMesh->mNumUVComponents[texCoordsSet] == UVCoordsType::length()) {
      vertex_attributes.push_back({va::UVS, UVCoordsType::length()});
    }
  }

  constexpr bool BONES_IN_PRIMARY_VBO = true;

  std::optional<std::vector<unsigned int>> boneIndices;
  std::optional<std::vector<::gl::GLfloat>> boneWeights;
  if (assimpMesh->HasBones()) {
    // Number of skinning data elements (bone indices and their weights).
    const auto numSkinningElements =
        Mesh::BONES_PER_VERTEX * static_cast<size_t>(assimpMesh->mNumVertices);
    // These vectors temporarily hold for each vertex the bone indices and weights of their
    // influence. They are passed into the shader as normal ivec4/vec4 vertex attributes.
    // Bone transformations themselves are passed as a uniform variable by the `MeshNode` class.
    boneIndices.emplace(numSkinningElements, 0);
    boneWeights.emplace(numSkinningElements, 0.0f);
    loadSkinningData(assimpMesh, *boneWeights, *boneIndices);

    static_assert(Mesh::BONES_PER_VERTEX == glm::vec4::length());
    if (BONES_IN_PRIMARY_VBO) {
      vertex_attributes.push_back({va::BONE_WEIGHTS, Mesh::BONES_PER_VERTEX});
    }
  }

  const StrideLayout layout{absl::MakeConstSpan(vertex_attributes)};

  // Interleave vertex attributes within a single buffer for better rendering performance.
  std::vector<ai_real> vertexData;
  vertexData.reserve(layout.strideLength() * sizeof(ai_real) * assimpMesh->mNumVertices);
  for (size_t vi = 0; vi < assimpMesh->mNumVertices; ++vi) {
    // Position
    pushAssimpVector(assimpMesh->mVertices[vi], vertexData);

    // Normal
    if (assimpMesh->mNormals) {
      pushAssimpVector(assimpMesh->mNormals[vi], vertexData);
    }

    // Color
    if (layout.hasAttribute(va::COLORS) && colorSet) {
      pushAssimpColor(colorSet[vi], vertexData, glm::vec3::length());
    }

    // Texture Coordinates
    if (layout.hasAttribute(va::UVS) && texCoords) {
      std::copy_n(
          &texCoords[vi].x,
          UVCoordsType::length(),
          std::back_inserter(vertexData)
      );
    }

    // Bone weights
    if (BONES_IN_PRIMARY_VBO && layout.hasAttribute(va::BONE_WEIGHTS)) {
      std::copy_n(
          &boneWeights.value()[vi * Mesh::BONES_PER_VERTEX],
          Mesh::BONES_PER_VERTEX,
          std::back_inserter(vertexData)
      );
    }
  }  // for

  // TODO: Support sharing a single buffer for multiple meshes.
  std::vector<std::shared_ptr<gl::VertexBuffer>> vbos;
  vbos.push_back(
    std::make_shared<gl::VertexBuffer>(
      gl::DataAdapter{ absl::MakeConstSpan(vertexData) },
      layout
    )
  );

  if (assimpMesh->HasBones()) {
    if (!BONES_IN_PRIMARY_VBO) {
      vbos.push_back(
        std::make_shared<gl::VertexBuffer>(
          absl::MakeConstSpan(*boneWeights),
          AttrList{ {va::BONE_WEIGHTS, Mesh::BONES_PER_VERTEX} }
        )
      );
    }

    // Unfortunately, we can't yet interleave this attribute with the others because its numeric
    // type (int) differs from the rest (floats). Technically, it should be possible, but our
    // DataAdapter class is a bit too simplistic to handle that.
    vbos.push_back(
      std::make_shared<gl::VertexBuffer>(
        absl::MakeConstSpan(*boneIndices),
        AttrList{ {va::BONE_INDICES, Mesh::BONES_PER_VERTEX} }
      )
    );
  }

  if (layout.hasAttribute(va::BONE_WEIGHTS)) {
    const auto primaryVBOVertCount = vbos[0]->countVertices();
    const auto boneVBOVertCount = vbos[1]->countVertices();
    if (primaryVBOVertCount != boneVBOVertCount) {
      LOG(WARNING) << "Vertex counts mismatch between mesh VBOs "
          << primaryVBOVertCount << " vs " << boneVBOVertCount;
    }
  }

  LOG(DEBUG) << "Creating mesh '" << fromAssimpString(assimpMesh->mName) << "' out of "
             << vbos.size() << " vertex buffers.";

  return std::make_shared<gl::Mesh>(gl::PrimitiveKind::Triangles, std::move(vbos));
}  // loadMesh

std::shared_ptr<Texture> Loader::Guts::loadTexture(
    const aiMaterial* const assimpMaterial, aiTextureType textureType, unsigned textureIndex
) {
  aiString texPathStr;
  if (assimpMaterial->GetTexture(textureType, textureIndex, &texPathStr) != aiReturn_SUCCESS) {
    throw std::runtime_error("Could not get texture path from Assimp material.");
  }

  std::string pathStr{texPathStr.C_Str()};
#if defined(FIX_WINDOWS_TEXTURE_PATHS)
  // Path might be missing due to it being a Windows path (using different separators).
  // Try to convert them to the current OS separators.
  std::replace(pathStr.data(), pathStr.data() + pathStr.size(), '\\', '/');
#endif  // FIX_WINDOWS_TEXTURE_PATHS

  boost::filesystem::path texPath{pathStr};
  texPath = texPath.generic_path();
  if (texPath.is_relative()) {
    texPath = boost::filesystem::absolute(texPath, _resRootPath);
  }

  if (!boost::filesystem::is_regular_file(texPath)) {
    LOG(WARNING) << "Texture file missing: '" << texPath << '\'';
    return nullptr;
  }

  texPath = boost::filesystem::canonical(texPath);
  return std::make_shared<gl::Texture>(texPath.string());
}

std::shared_ptr<Material> Loader::Guts::loadMaterial(const unsigned materialIndex) {
  auto iter = _materialsCache.find(materialIndex);
  if (iter != _materialsCache.end())
    return iter->second;

  if (materialIndex >= _scene->mNumMaterials)
    return nullptr;

  aiMaterial* const assimpMaterial = _scene->mMaterials[materialIndex];
  const auto materialName = fromAssimpString(assimpMaterial->GetName());

  MaterialProperties properties{};
  assimpMaterial->Get(AI_MATKEY_OPACITY,            properties.opacity);
  assimpMaterial->Get(AI_MATKEY_REFLECTIVITY,       properties.reflectivity);
  assimpMaterial->Get(AI_MATKEY_SHININESS,          properties.shininess);
  assimpMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, properties.shininessStrength);
  assimpMaterial->Get(AI_MATKEY_REFRACTI,           properties.refractionIndex);

  properties.diffuseColor     = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_DIFFUSE);
  properties.ambientColor     = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_AMBIENT);
  properties.specularColor    = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_SPECULAR);
  properties.emissiveColor    = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_EMISSIVE);
  properties.transparentColor = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_TRANSPARENT);
  properties.reflectiveColor  = fromColorProperty(assimpMaterial, AI_MATKEY_COLOR_REFLECTIVE);

  // TODO: sanitize material name for printable characters only. Some models seem to have garbage.
  LOG(DEBUG) << "Found material '" << /*materialName <<*/ "'. " << properties;

  MaterialTextures textures{};
  for (aiTextureType assimpTexType = util::stepEnumValue(aiTextureType_NONE);
       assimpTexType < aiTextureType_UNKNOWN;
       assimpTexType = util::stepEnumValue(assimpTexType)
  ) {
    const unsigned numTextures = assimpMaterial->GetTextureCount(assimpTexType);
    if (numTextures > 0) {
      LOG(DEBUG) << "'" << materialName << "' has " << numTextures
                 << " textures of type " << assimpTexType;
    }

    for (unsigned ti = 0; ti < numTextures; ++ti) {
      textures.insert({
        static_cast<TextureKind>(assimpTexType),  // Should be compatible by values.
        loadTexture(assimpMaterial, assimpTexType, ti)
      });
    }
  }

  return _materialsCache[materialIndex] = std::make_shared<Material>(
      std::string{materialName}, std::move(properties), std::move(textures)
  );
}  // loadMaterial

std::shared_ptr<sg::MeshNode> Loader::Guts::processAssimpMesh(
    std::string nodeName, const aiMesh* const assimpMesh
) {
  const auto mesh = loadMesh(assimpMesh);
  const auto material = loadMaterial(assimpMesh->mMaterialIndex);
  return std::make_shared<sg::MeshNode>(mesh, material, std::move(nodeName));
}

std::shared_ptr<Node> Loader::Guts::processAssimpNode(const aiNode* src) {
  const std::string_view nodeName{src->mName.data, src->mName.length};

  const auto numMeshes = src->mNumMeshes;
  if (numMeshes) {
    LOG(DEBUG) << "Node " << nodeName << " has " << numMeshes << " meshes.";
  }

  std::shared_ptr<sg::Node> result = (numMeshes == 1) ?
      // Optimize out unnecessary node in the case of this node having just a single mesh.
      processAssimpMesh(std::string{nodeName}, _scene->mMeshes[src->mMeshes[0]]) :
      std::make_shared<sg::Node>(std::string{nodeName});

  result->setLocalTransform(assimpToGlmMatrix(src->mTransformation));

#if defined(KEG_DEBUG) && 0
  // Append single dot mesh to visualize scene graph nodes.

  // These statics leak GL resources which is a shitty way to do things.
  // TODO: Replace this by bone visualization using geometry generating shader.
  static const auto DEBUG_MATERIAL = std::make_shared<gl::Material>();
  static const std::vector<float> RED_DOT{0, 0, 0 /* pos */,   1, 0, 0 /* color */};
  static const auto POINT_MESH = std::make_shared<gl::Mesh>(
    gl::PrimitiveKind::Points,
    std::make_shared<gl::VertexBuffer>(
      gl::DataAdapter{&RED_DOT},
      gl::VertexBuffer::AttributeMapping{
        {va::POSITIONS, {0, 3}},
        {va::COLORS,    {3, 3}},
      }
    )
  );

  result->addChild(std::make_shared<sg::MeshNode>(
      POINT_MESH, DEBUG_MATERIAL, "keg_debug_" + std::string(nodeName)
  ));
#endif

  if (numMeshes > 1) {
    for (unsigned int mi = 0; mi < numMeshes; mi++) {
      const auto globalMeshIndex = src->mMeshes[mi];
      const auto* const mesh = _scene->mMeshes[globalMeshIndex];
      result->addChild( processAssimpMesh(fmt::format("mesh #{}", globalMeshIndex), mesh) );
    }
  }

  for (unsigned c = 0; c < src->mNumChildren; c++) {
    result->addChild( processAssimpNode(src->mChildren[c]) );
  }

  return result;
}  // processAssimpMesh

Loader::Guts::Guts(std::string_view filePath) {
  LOG(DEBUG) << "Importing data from '" << filePath << "'...";

  _importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_COLORS);
  _importer.SetPropertyInteger(
      AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT | aiPrimitiveType_LINE
  );

  _scene = _importer.ReadFile(
      &filePath[0],
      // NOTE: `aiProcess_Triangulate` does not really work all that well: half the triangles become
      // invisible, and skinning must be somehow adapted for animations to work.
      // It's best if all models are be prepared for rendering already.
      // NOTE: `aiProcess_JoinIdenticalVertices` blows up meshes.
      aiProcess_RemoveComponent
      | aiProcess_ImproveCacheLocality
      | aiProcess_GenUVCoords
      | aiProcess_GenNormals
      // | aiProcess_Triangulate
  );
  if (!_scene) {
    throw std::runtime_error(
        std::string("Assimp error: ") + _importer.GetErrorString()
    );
  }
  _resourcePath = &filePath[0];
  _resRootPath = _resourcePath.parent_path();

  LOG(DEBUG) << "Resource file contains:\n"
    << "  - " << _scene->mNumTextures << " textures\n"
    << "  - " << _scene->mNumMaterials << " materials\n"
    << "  - " << _scene->mNumMeshes << " meshes\n"
    << "  - " << _scene->mNumAnimations << " animations\n"
    << "  - " << _scene->mNumLights << " lights\n"
    << "  - " << _scene->mNumCameras << " cameras"
  ;
}  // Loader::Guts::Guts

std::shared_ptr<Model> Loader::Guts::loadModelFromRootNode() {
  const auto rootNode = processAssimpNode(_scene->mRootNode);
  // Assimp only lists bones which directly affect the vertices, but skeletons may
  // contain nodes that only add structure without direct effect on the mesh.
  // For that reason we have to infer all such bones by simply going up the node tree
  // starting with nodes which are known to represent bones. This is both ineffective
  // and pessimistic - i.e. may include nodes which aren't bones. Hopefuly this will
  // have no ill effect, since they won't be animated anyway.
  // TODO: Optimize.
  for (const auto& [boneName, boneData] : _bonesAcc.indicesMap) {
    const auto boneNode = rootNode->findByName(boneName);
    assert(!!boneNode);
    boneNode->traverseUp([&] (Node* node) -> bool {
      _bonesAcc.addBone(node->label(), glm::identity<glm::mat4>());
      return true;
    });
  }

  auto skeleton = std::make_unique<Skeleton>(rootNode, std::move(_bonesAcc.indicesMap));
  auto model = std::make_shared<Model>(std::move(skeleton));
  model->addChild(rootNode);

  return model;

}

std::vector<std::shared_ptr<Animation> > Loader::Guts::loadAnimations() {
  std::vector<std::shared_ptr<Animation>> animations;
  animations.reserve(_scene->mNumAnimations);

  for (size_t ai = 0; ai < _scene->mNumAnimations; ++ai) {
      const auto* const anim = _scene->mAnimations[ai];
      const std::string_view animName = fromAssimpString(anim->mName);
      const double ticksPerSec = anim->mTicksPerSecond > 0 ?
          anim->mTicksPerSecond :
          DEFAULT_ANIMATION_TICKS_PER_SEC;

      LOG(DEBUG) << "Animation '" << animName << "':\n"
          << "  - duration: " << anim->mDuration / ticksPerSec << "\n"
          << "  - channels: " << anim->mNumChannels;

      std::vector<Animation::Channel> channels;
      channels.reserve(anim->mNumChannels);

      for (size_t ci = 0; ci < anim->mNumChannels; ++ci) {
        const auto* const channel = anim->mChannels[ci];
        LOG(TRACE) << "Channels: "
            << "P:" << channel->mNumPositionKeys << ", "
            << "R:" << channel->mNumRotationKeys << ", "
            << "S:" << channel->mNumScalingKeys << " in '"
            << fromAssimpString(channel->mNodeName);

        // TODO: Assimp's code comments claim it's possible to have animation frames with
        // negative time points. Handle those.

        Animation::Channel::PositionFrames positions;
        positions.reserve(channel->mNumPositionKeys);
        for (size_t pki = 0; pki < channel->mNumPositionKeys; ++pki) {
          const auto& posKey = channel->mPositionKeys[pki];
          positions.emplace_back(
            toAnimationTime(posKey.mTime, ticksPerSec),
            assimpToGlmVector(posKey.mValue)
          );
        }

        Animation::Channel::RotationFrames rotations;
        rotations.reserve(channel->mNumRotationKeys);
        for (size_t rki = 0; rki < channel->mNumRotationKeys; ++rki) {
          const auto& rotKey = channel->mRotationKeys[rki];
          rotations.emplace_back(
            toAnimationTime(rotKey.mTime, ticksPerSec),
            assimpToGlmQuat(rotKey.mValue)
          );
        }

        Animation::Channel::ScalingFrames scalings;
        scalings.reserve(channel->mNumScalingKeys);
        for (size_t ski = 0; ski < channel->mNumScalingKeys; ++ski) {
          const auto& scaleKey = channel->mScalingKeys[ski];
          scalings.emplace_back(
            toAnimationTime(scaleKey.mTime, ticksPerSec),
            assimpToGlmVector(scaleKey.mValue)
          );
        }

        channels.push_back(Animation::Channel{
            std::string{fromAssimpString(channel->mNodeName)},
            std::move(positions),
            std::move(rotations),
            std::move(scalings)
        });
      }
      animations.push_back(std::make_shared<Animation>(animName, std::move(channels)));
    }

  return animations;
}


Loader::Loader(std::string_view filePath)
  : _guts(new Guts(filePath)) {
}

Loader::~Loader() = default;

std::shared_ptr<Model> Loader::loadModel() {
  return _guts->loadModelFromRootNode();
}

std::vector<std::shared_ptr<Animation>> Loader::loadAnimations() {
  return _guts->loadAnimations();
}

}  // namespace keg::sg
