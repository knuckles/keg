#pragma once

#include <type_traits>


namespace keg::util {

template <typename Enum, typename Numeric = std::underlying_type_t<Enum>>
constexpr Numeric enumIntValue(Enum val) noexcept {
  return static_cast<Numeric>(val);
}

template <typename Enum>
constexpr Enum stepEnumValue(Enum val, int step = 1) noexcept {
  return static_cast<Enum>(enumIntValue(val) + step);
}

}  // namespace keg::util
