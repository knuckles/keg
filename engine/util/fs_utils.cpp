#include <util/fs_utils.hpp>

#include <fstream>
#include <stdexcept>

namespace keg::util {

std::string readTextFile(std::string_view filePath) {
  std::ifstream ifs(&filePath[0]);
  if(!ifs.good())
    throw std::runtime_error{"Can't read file '" + std::string{filePath} + "'"};
  return std::string(
      std::istreambuf_iterator<char>(ifs),
      std::istreambuf_iterator<char>()
  );
}

}  // namespace keg::util
