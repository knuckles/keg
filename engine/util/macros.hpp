#pragma once

/// Create a string literal out of an identifier.
#define STRINGIFY(s) #s


/// Declare class as default-copyable.
#define DEFAULT_COPYABLE(TypeName) \
    TypeName(const TypeName&) = default; \
    TypeName& operator=(const TypeName&)  = default;


/// Declare class as non-copyable.
#define NON_COPYABLE(TypeName) \
    TypeName(const TypeName&) = delete; \
    TypeName& operator=(const TypeName&)  = delete;


/// Declare class as default-movable.
#define DEFAULT_MOVABLE(TypeName) \
    TypeName(TypeName&&) = default; \
    TypeName& operator=(TypeName&&) = default;


/// Declare class as non-movable.
#define NON_MOVABLE(TypeName) \
    TypeName(TypeName&&) = delete; \
    TypeName& operator=(TypeName&&) = delete;
