#include "sdl_helpers.hpp"

#include <SDL.h>

namespace keg {
namespace util {

void SDLException::check(int result) {
  if (result != 0)
    throw SDLException(SDL_GetError());
}

SDLException::SDLException(const char* message)
  : std::runtime_error(message)
{}

#if 0
SDLInitializer::SDLInitializer() {
  SDLException::check(SDL_Init(SDL_INIT_VIDEO));
}

SDLInitializer::~SDLInitializer() {
  SDL_Quit();
}

SDLWindow::SDLWindow(
  const std::string& title,
  int x, int y,
  int w, int h,
  Uint32 flags
)
  : _window(SDL_CreateWindow(title.c_str(), x, y, w, h, flags)) {
  if (!_window)
    throw SDLException(SDL_GetError());
}

SDLWindow::~SDLWindow() {
  SDL_DestroyWindow(_window);
}
#endif
}  // namespace util
}  // namespace keg
