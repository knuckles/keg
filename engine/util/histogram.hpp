#pragma once

#include <ostream>
#include <vector>

namespace keg::util {

template <typename T>
class Histogram {
public:
  explicit Histogram(std::vector<T> pivotValues)
    : _pivotValues(std::move(pivotValues)),
      _buckets(_pivotValues.size() + 1, 0) {
    std::sort(_pivotValues.begin(), _pivotValues.end());
  }

  void putValue(const T value) {
    const auto iPivot = std::lower_bound(
        _pivotValues.cbegin(), _pivotValues.cend(), value
    );
    const size_t bucketIndex = std::distance(_pivotValues.cbegin(), iPivot);
    assert(bucketIndex < _buckets.size());
    _buckets[bucketIndex] += 1;
  }

  const std::vector<T>& pivotValues() const { return _pivotValues; }
  const std::vector<size_t>& buckets() const { return _buckets; }

private:
  std::vector<T> _pivotValues;
  std::vector<size_t> _buckets;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Histogram<T>& h) {
  for (size_t bi = 0; bi < h.buckets().size(); bi++) {
    const auto bucketLo = bi ?
        h.pivotValues().at(bi - 1) :
        -std::numeric_limits<float>::infinity();

    const auto bucketHi = bi < h.buckets().size() - 1 ?
        h.pivotValues().at(bi) :
        std::numeric_limits<float>::infinity();

    const auto count = h.buckets().at(bi);

    os << bucketLo << ".." << bucketHi << ": " << count << "\n";
  }
  return os;
}


}  // namespace keg::util
