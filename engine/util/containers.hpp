#pragma once

#include <optional>
#include <ostream>
#include <map>
#include <set>
#include <vector>


namespace keg::util {

namespace detail {
struct Decorators {
  std::string start;
  std::string stop;
  std::string separator;
};

template<typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& pair) {
  os << pair.first << ": " << pair.second;
  return os;
}

template<typename C>
void printContainer(std::ostream& os, const C& container, const Decorators& decors) {
  os << decors.start;
  for (const auto& el : container) {
    os << el << decors.separator;
  }
  os << decors.stop;
}
};

template<typename T>
void printAny(std::ostream& os, const std::vector<T> vec) {
  static const detail::Decorators vecDecors{"[", "]", ", "};
  printContainer(os, vec, vecDecors);
}

template<typename T, typename U>
void printAny(std::ostream& os, const std::map<T, U> set) {
  static const detail::Decorators setDecors{"{\n", "\n}", ",\n"};
  printContainer(os, set, setDecors);
}

template<typename T>
void printAny(std::ostream& os, const std::set<T> set) {
  static const detail::Decorators setDecors{"(", ")", ", "};
  printContainer(os, set, setDecors);
}

template<typename Container, typename Key, typename Value>
const Value& pick(const Container& c, const Key& key, const Value& defaultValue) {
  const auto it = c.find(key);
  if (it != c.cend())
    return *it;
  return defaultValue;
}

template<typename Value, typename Container, typename Key>
const std::optional<Value> pick(const Container& c, const Key& key) {
  const auto it = c.find(key);
  if (it != c.cend())
    return it->second;
  return std::nullopt;
}
}  // namespace keg::util
