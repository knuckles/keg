#pragma once

#include <g3log/loglevels.hpp>


// Custom logging levels:
const LEVELS TRACE {DEBUG.value - 1, {"Tracing"}};
