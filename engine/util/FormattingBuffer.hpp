#pragma once

#include <cstddef>
#include <string_view>
#include <vector>
#include <fmt/core.h>


namespace keg::util {

/// A text buffer which can be used to format text into using the {fmt} library.
/// Primary benefit is to avoid memory allocations after construction.
class FormattingBuffer
{
public:
  FormattingBuffer(std::size_t size) {
    _textBuf.reserve(size);
  }

  size_t capacity() const { return _textBuf.capacity(); }

  /// Access currently stored string.
  std::string_view contents() const {
    return std::string_view(&_textBuf[0], _outIter - _textBuf.begin());
  }

  /// @returns whether the buffer is completely filled.
  bool full() const {
    return _outIter >= _textBuf.begin() + _textBuf.capacity();
  }

  /// @returns the number of chars in the buffer already occupied by the text.
  std::size_t spaceUsed() const {
    return _outIter - _textBuf.begin();
  }

  /// @returns the number of chars left available in the buffer.
  std::size_t spaceLeft() const {
    return _textBuf.capacity() - spaceUsed();
  }

  /// Clear the buffer, resetting its contents to an empty string.
  void clear() {
    _textBuf.clear();
    _outIter = _textBuf.begin();
  }

  /// Format the text and append it to the currently stored string.
  /// @note please include additional <fmt> headers to suit your needs.
  /// @param formatStr formatting template string.
  /// @param args... formatting arguments. Passed to the {fmt} library together with the template.
  /// @returns the output size of this formatting operation.
  template <typename String, typename... Args>
  std::size_t append(const String& formatStr, const Args&... args) {
    const auto& fmtResult = fmt::format_to_n(_outIter, spaceLeft(), formatStr, args...);
    _outIter = fmtResult.out;
    return fmtResult.size;
  }

private:
  using Container = std::vector<char>;
  Container _textBuf;
  Container::iterator _outIter = _textBuf.begin();
};

}  // namespace keg::util
