#pragma once

#include <string>
#include <string_view>

namespace keg {
namespace util {

std::string readTextFile(std::string_view filePath);

}  // namespace util
}  // namespace keg
