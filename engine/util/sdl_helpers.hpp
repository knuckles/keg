#pragma once

#include <stdexcept>
#include <string>

#include <SDL_types.h>

struct SDL_Window;

namespace keg {
namespace util {

class SDLException : public std::runtime_error {
public:
  static void check(int result);

  SDLException(const char* message);
};

#if 0
class SDLInitializer {
public:
  SDLInitializer();
  virtual ~SDLInitializer();
};

class SDLWindow {
public:
  SDLWindow(
    const std::string& title,
    int x, int y,
    int w, int h,
    Uint32 flags
  );
  virtual ~SDLWindow();

private:
  SDL_Window* _window;
};
#endif

}  // namespace util
}  // namespace keg
