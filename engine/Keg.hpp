#pragma once

#include <functional>
#include <memory>
#include <string_view>
#include <vector>

#include <SDL_keycode.h>  // For input callback registration.

#include <base/types.hpp>
#include <input/pub.hpp>
#include <util/macros.hpp>
#include <render/RenderPass.hpp>


namespace keg {

namespace core {
class LogicClock;
}

namespace render {
class Renderer;
}

namespace sg {
class NodeController;
class Scene;
}  // namespace keg::sg

using PerfCallback = std::function<void(float updateTime, float renderTime, int fps)>;

class Keg final {
public:
  /// Returns current engine instance. Throws when no instance currently exists.
  /// @note This function is NOT thread safe with regards to instance lifetime.
  static Keg& instance();

  /// @brief Initialize the engine.
  /// @note Engine is essentially a singletom, so you can ever only have 1 instance of it.
  /// Constructor will trow on attempts to create more than 1 instance.
  ///
  /// @param winWidth Initial window width.
  /// @param winHeight Initial window height.
  /// @param logsPath File system directory path to store log files in.
  Keg(int winWidth, int winHeight, std::string_view logsPath);

  /// You can destroy your one instance at any time.
  ~Keg();

  NON_COPYABLE(Keg)
  NON_MOVABLE(Keg)

  core::LogicClock& logicClock();

  core::CallbackId addInputCallback(SDL_Keycode key, input::InputCallback callback);
  bool removeInputCallback(SDL_Keycode key, core::CallbackId callbackId);

  std::unique_ptr<sg::NodeController> createFreeFlyCameraController() const;

  render::Renderer& renderer();

  /// @brief Add another scene for rendering.
  /// Scenes are rendered in the order of addition.
  void addRenderPass(RenderPass pass);

  /// @brief Remove all scenes. Nothing will be rendered.
  void clearRenderPasses();

  core::CallbackId addPerfCallback(PerfCallback callback);
  bool removePerfCallback(core::CallbackId callbackId);

  void enterMainLoop(bool& canContinue);

private:
  bool processEvents();

  static Keg* _instance;

  struct Guts;
  std::unique_ptr<Guts> _guts;
  // TODO: Shove these into the guts.
  std::vector<PerfCallback> _perfCallbacks;
  std::vector<RenderPass> _renderPasses;
};

}  // namespace keg
