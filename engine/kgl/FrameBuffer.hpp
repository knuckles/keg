#pragma once

#include <vector>

#include <glm/vec4.hpp>
#include <glbinding/gl33core/types.h>

#include <kgl/GLObject.hpp>
#include <kgl/RenderBuffer.hpp>
#include <kgl/Texture.hpp>


namespace keg::gl {

class Texture;

/// A Framebuffer is a collection of buffers that can be used as the destination for rendering.
/// OpenGL has two kinds of framebuffers:
/// - the Default Framebuffer, which is provided by the OpenGL Context;
/// - and user-created framebuffers called Framebuffer Objects (FBOs).
/// The buffers for default framebuffers are part of the context and usually represent a window or
/// display device. The buffers for FBOs reference images from either Textures or Renderbuffers;
/// they are never directly visible.
class FrameBuffer : public GLObject
{
public:  // Types
  /// Operations allowed on a framebuffer.
  enum class Usage {
    ReadOnly,
    WriteOnly,
    ReadWrite,
  };

  /// Possible attachment points for textures.
  enum class TextureAttachment {
    Color
  };

public:  // Methods
  /// @return the default framebuffer, which is provided by the OpenGL context.
  static FrameBuffer& getDefaultFrameBuffer();

  /// @return the max number of attachments which can be attached for collor output.
  static ::gl::GLsizei getMaxColorAttachments();

  /// Create a new framebuffer.
  FrameBuffer();

  using GLObject::bind;

  /// Attach a texture for color output.
  /// @returns future texture unit index which this texture is going to be attached to whenever
  /// @c `useAllColorTextures` is called.
  ::gl::GLint attachColorTexture(Texture&& texture);

  [[nodiscard]] std::vector<Texture> detachAllColorTextures();

  /// Binds all attached textures to their texture unit, as returned from @c `attachColorTexture`.
  void useAllColorTextures();

  void attachDepthRenderBuffer(RenderBuffer&& rbo);

  [[nodiscard]] std::vector<RenderBuffer> detachAllRenderBuffers();

  /// Read framebuffer image data into the provided buffer.
  void readPixels(std::vector<char>& dest);

  /// Copy data from another framebuffer into this framebuffer.
  void blitFrom(
      const FrameBuffer& source,
      ::gl::ClearBufferMask componentsMask,
      const glm::ivec4& sourceRect,
      const glm::ivec4& destRect,
      ::gl::GLenum filter
  );

  /// @return the status of this framebuffer. Possible return values:
  /// @c ::gl::GL_FRAMEBUFFER_COMPLETE
  /// @c ::gl::GL_FRAMEBUFFER_UNDEFINED
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER
  /// @c ::gl::GL_FRAMEBUFFER_UNSUPPORTED
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE
  /// @c ::gl::GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS
  ::gl::GLenum status() const;

private:
  /// Create from handle.
  explicit FrameBuffer(::gl::GLuint handle);

  std::vector<Texture> _attachedTextures;
  std::vector<RenderBuffer> _attachedRenderBuffers;
};

};
