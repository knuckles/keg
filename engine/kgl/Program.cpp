#include <kgl/Program.hpp>

#include <fmt/format.h>
#define GLM_FORCE_SILENT_WARNINGS
#include <glm/matrix.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <absl/functional/bind_front.h>
#include <kgl/bindings.hpp>

#include <kgl/exceptions.hpp>

namespace keg::gl {
using namespace ::gl;

namespace {
auto toGLShaderKind(const Shader::Kind kind) -> GLenum {
  switch(kind) {
    case Shader::Kind::Fragment: return GL_FRAGMENT_SHADER;
    case Shader::Kind::Vertex: return GL_VERTEX_SHADER;
    case Shader::Kind::Geometry: return GL_GEOMETRY_SHADER;
  }
}

std::string getShaderInfoLog(GLuint handle) {
  GLint logLength = 0;
  glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLength);
  std::basic_string<GLchar> log(logLength, 0);
  glGetShaderInfoLog(handle, logLength, nullptr, &log[0]);
  return log;
}

std::string getProgramInfoLog(GLuint handle) {
  GLint logLength = 0;
  glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logLength);
  std::basic_string<GLchar> log(logLength, 0);
  glGetProgramInfoLog(handle, logLength, nullptr, &log[0]);
  return log;
}
}  // namespace

Shader::Shader(Kind kind, std::string_view program)
  : GLObject{
      [kind]() -> GLObject::TypeTag {
        switch (kind) {
          case Kind::Fragment: return GLObject::TypeTag::FShader;
          case Kind::Vertex: return GLObject::TypeTag::VShader;
          case Kind::Geometry: return GLObject::TypeTag::GShader;
        }
      }(),
      absl::bind_front(glCreateShader, toGLShaderKind(kind)),
      glDeleteShader,
      nullptr  // No "bind" function.
    }
{
  if (program.empty())
    throw std::invalid_argument("Shader program is void.");
  if (program.length() > MAX_PROGRAM_LENGTH)
    throw std::invalid_argument("Shader program is too long.");

  const GLchar* const programStart = &program.front();
  const GLint programLength = static_cast<GLint>(program.length());
  // TODO: Use some mmapped IO instead.
  ASSERT_GL_OK(glShaderSource(handle(), 1, &programStart, &programLength));
  ASSERT_GL_OK(glCompileShader(handle()));

  GLboolean status = GL_FALSE;
  glGetShaderiv(handle(), GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    throw std::runtime_error("Shader compilation error:\n" + getShaderInfoLog(handle()));
  }
}


FragmentShader::FragmentShader(std::string_view program)
  : Shader(Kind::Fragment, program) {
}


VertexShader::VertexShader(std::string_view program)
  : Shader(Kind::Vertex, program) {
}


GeometryShader::GeometryShader(std::string_view program)
  : Shader(Kind::Geometry, program) {
}


Program::Program(const ShadersInitList& shaders)
  : GLObject{
      GLObject::TypeTag::Program,
      glCreateProgram,
      glDeleteProgram,
      [](::gl::GLenum /* ignored */, Handle handle) { glUseProgram(handle); }
    }
{
  for (const auto& shader : shaders) {
    ASSERT_GL_OK(glAttachShader(handle(), shader->handle()));
  }
  ASSERT_GL_OK(glLinkProgram(handle()));
  for (const auto& shader : shaders) {
    ASSERT_GL_OK(glDetachShader(handle(), shader->handle()));
  }

  GLboolean status = GL_FALSE;
  glGetProgramiv(handle(), GL_LINK_STATUS, &status);
  if (status != GL_TRUE) {
    throw std::runtime_error("Program linking error:\n" + getProgramInfoLog(handle()));
  }
}

bool Program::varExists(std::string_view varName) const noexcept {
  return findVarIndex(varName) >= 0;
}

void Program::setUniformVar(std::string_view varName, GLint val) {
  ASSERT_GL_OK(glUniform1i(getVarIndex(varName), val));
}

void Program::setUniformVar(std::string_view varName, bool val) {
  ASSERT_GL_OK(glUniform1i(getVarIndex(varName), val ? 1 : 0));
}

#if 0
void Program::setUniformVar(std::string_view varName, GLuint val) {
  ASSERT_GL_OK(glUniform1ui(getVarIndex(varName), val));
}
#endif

void Program::setUniformVar(std::string_view varName, GLfloat val) {
  ASSERT_GL_OK(glUniform1f(getVarIndex(varName), val));
}

void Program::setUniformVar(std::string_view varName, const glm::vec2& val) {
  ASSERT_GL_OK(glUniform2f(getVarIndex(varName), val.x, val.y));
}

void Program::setUniformVar(std::string_view varName, const glm::vec3& val) {
  ASSERT_GL_OK(glUniform3f(getVarIndex(varName), val.x, val.y, val.z));
}

void Program::setUniformVar(std::string_view varName, const glm::vec4& val) {
  ASSERT_GL_OK(glUniform4f(getVarIndex(varName), val.x, val.y, val.z, val.w));
}

void Program::setUniformVar(std::string_view varName, const glm::mat4& val) {
  ASSERT_GL_OK(
      glUniformMatrix4fv(getVarIndex(varName), 1, GL_FALSE, glm::value_ptr(val))
  );
}

void Program::setUniformVar(std::string_view varName, const std::vector<glm::mat4>& val) {
  ASSERT_GL_OK(glUniformMatrix4fv(
      getVarIndex(varName),
      val.size(),
      GL_FALSE,
      glm::value_ptr(val.front())
  ));
}

void Program::makeCurrent() {
  bind(::gl::GLenum{} /* ignored */);
}

GLint Program::findVarIndex(std::string_view varName) const noexcept {
  // TODO: Use local cache?
  return glGetUniformLocation(handle(), &varName.front());
}

GLint Program::getVarIndex(std::string_view varName) const {
  const auto index = findVarIndex(varName);
  if (index < 0)
    throw std::runtime_error(
        fmt::format("Uniform variable '{}' location not found", varName)
    );
  return index;
}

}  // namespace keg::gl
