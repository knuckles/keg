#include <kgl/Texture.hpp>

#include <string>
#include <stdexcept>
#include <iostream>

#include <SOIL.h>
#include <g3log/g3log.hpp>
#include <kgl/bindings.hpp>
#include <fmt/format.h>

#include <kgl/exceptions.hpp>


namespace keg::gl {
using namespace ::gl;

namespace {

size_t numChannelsFromTextureFormat(GLenum internalFormat) {
  switch (internalFormat) {
    case GL_R3_G3_B2:
    case GL_RGB4:
    case GL_RGB5:
    case GL_RGB8:
    case GL_RGB10:
    case GL_RGB12:
    case GL_RGB16:
      return 3;

    case GL_RGBA2:
    case GL_RGBA4:
    case GL_RGB5_A1:
    case GL_RGBA8:
    case GL_RGB10_A2:
    case GL_RGBA12:
    case GL_RGBA16:
      return 4;

    default:
      return 0;
  }
}

GLObject::Handle loadTexture(std::string_view filePath) {
  const auto handle = SOIL_load_OGL_texture(
      &filePath.front(),
      SOIL_LOAD_AUTO,
      SOIL_CREATE_NEW_ID,
      SOIL_FLAG_INVERT_Y  // What about SOIL_FLAG_MIPMAPS ?
      // NOTE: SOIL will add SOIL_FLAG_POWER_OF_TWO itself if needed.
  );
  if (!handle) {
    throw std::runtime_error(
        fmt::format("Could not load texture from `{}`: {}", filePath, SOIL_last_result())
    );
  }
  return handle;
}

}  // namespace


Texture::Texture(
    gl::GLsizei width,
    gl::GLsizei height,
    GLenum internalFormat,
    GLenum colorFormat,
    GLenum numericFormat
)
  : GLObject{GLObject::TypeTag::Texture, glGenTextures, glDeleteTextures, glBindTexture}
{
  bind(GL_TEXTURE_2D);
  glTexImage2D(
    /* target   */ GL_TEXTURE_2D,
    /* LoD      */ 0,
    /* internal */ internalFormat,
    /* width    */ width,
    /* height   */ height,
    /* border   */ 0,
    /* format   */ colorFormat,
    /* type     */ numericFormat,
    /* data     */ nullptr
  );
  assertNoGLError("glTexImage2D");

  _width = width;
  _height = height;
  _numChannels = 3;

  setBasicParameters();
}

Texture::Texture(std::string_view filePath)
  : GLObject{
      GLObject::TypeTag::Texture,
      loadTexture(filePath),
      glDeleteTextures,
      glBindTexture
    }
{
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &_width);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &_height);

  {
    GLenum internalFormat;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &internalFormat);
    _numChannels = numChannelsFromTextureFormat(internalFormat);
  }

  bind(GL_TEXTURE_2D);
  setBasicParameters();

  LOG(DEBUG) << "Texture created from " << filePath;
}

Texture::Texture(const unsigned char* encoded, size_t dataLength)
  : GLObject{GLObject::TypeTag::Texture, glGenTextures, glDeleteTextures, glBindTexture}
{
  std::unique_ptr<unsigned char, decltype(&SOIL_free_image_data)> imageData(
      SOIL_load_image_from_memory(
          encoded, dataLength, &_width, &_height, &_numChannels, SOIL_LOAD_AUTO
      ),
      SOIL_free_image_data
  );
  if (!imageData) {
    throw std::invalid_argument(SOIL_last_result());
  }
  assignImageData(imageData.get());
}

void Texture::assignImageData(unsigned char* const imageData) {
  if (!imageData) {
    throw std::runtime_error(fmt::format(
        "SOIL error: {:s}", SOIL_last_result()
    ));
  }

  if (_numChannels < 3) {
    throw std::runtime_error(fmt::format(
        "Image contains only {:d} channels.", _numChannels
    ));
  }

  bind(GL_TEXTURE_2D);
  glTexImage2D(
      /* target   */ GL_TEXTURE_2D,
      /* LoD      */ 0,
      /* internal */ _numChannels > 3 ? GL_RGBA : GL_RGB,  // TODO: Can we get anything else here?
      _width, _height,
      /* border   */ 0,
      /* format   */ _numChannels > 3 ? GL_RGBA : GL_RGB,
      /* type     */ GL_UNSIGNED_BYTE,
      /* data     */ imageData
  );
  assertNoGLError("glTexImage2D");

  setBasicParameters();
}

void Texture::setBasicParameters() {
  ASSERT_GL_OK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
  ASSERT_GL_OK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
}

void Texture::use(unsigned textureUnitIdx) const {
  ASSERT_GL_OK(glActiveTexture(GL_TEXTURE0 + textureUnitIdx));
  bind(GL_TEXTURE_2D);
}

// static
void Texture::reset(unsigned textureUnitIdx) {
  ASSERT_GL_OK(glActiveTexture(GL_TEXTURE0 + textureUnitIdx));
  ASSERT_GL_OK(glBindTexture(GL_TEXTURE_2D, 0));
}

}  // namespace keg::gl
