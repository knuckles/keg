#include "RenderBuffer.hpp"

#include <kgl/bindings.hpp>

#include <kgl/exceptions.hpp>


namespace keg::gl {
using namespace ::gl;


RenderBuffer::RenderBuffer(
  ::gl::GLsizei width,
  ::gl::GLsizei height,
  ::gl::GLenum internalFormat
)
  : GLObject{GLObject::TypeTag::RBO, glGenRenderbuffers, glDeleteRenderbuffers, glBindRenderbuffer}
{
  bind(GL_RENDERBUFFER);
  ASSERT_GL_OK( glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, width, height) );
}

}  // namespace keg::gl
