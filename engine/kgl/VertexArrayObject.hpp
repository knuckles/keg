#pragma once

#include <kgl/data_types.hpp>
#include <kgl/GLObject.hpp>


namespace keg::gl {

class VertexBuffer;


/// A Vertex Array Object (or VAO) is an object stores mappings between vertex attribute indices and
/// corresponding Vertex Buffer Objects (or VBOs). This means that the VAO is not the actual object
/// storing the vertex data, but merely the descriptor of the vertex data.
/// Attribute association happens per call to @c attachVertexBuffer.
class VertexArrayObject : public GLObject {
public:
  /// Construct a new VAO.
  VertexArrayObject();

  void bind();

  /// Disable one or all vertex attributes for this VAO.
  void disableAttribute(vertex_attributes::Id attribute);
  void disableAllAttributes();

  /// Stores all vertex attribute pointers provided by the vertex buffer.
  /// In other words this merely explains the data format to the VAO, thus doesn't need to be called
  /// per each frame.
  void attachVertexBuffer(VertexBuffer& vbo);
};

}  // namespace keg::gl
