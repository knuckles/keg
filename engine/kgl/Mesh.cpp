#include <kgl/Mesh.hpp>

#include <algorithm>
#include <stdexcept>

#include <kgl/bindings.hpp>

#include <kgl/exceptions.hpp>
#include <kgl/VertexArrayObject.hpp>
#include <kgl/VertexBuffer.hpp>


namespace keg::gl {
using namespace ::gl;

namespace {

// NOTE: This must match the constant in `generic_vs.in.glsl`.
static_assert(Mesh::MAX_BONES == MAX_BONES_PER_MESH);


GLenum glModeForprimitives(PrimitiveKind p) {
  switch(p) {
    case PrimitiveKind::Points: return GL_POINTS;
    case PrimitiveKind::Lines: return GL_LINES;
    case PrimitiveKind::LineLoop: return GL_LINE_LOOP;
    case PrimitiveKind::LineStrip: return GL_LINE_STRIP;
    case PrimitiveKind::Triangles: return GL_TRIANGLES;
    case PrimitiveKind::TriangleStrip: return GL_TRIANGLE_STRIP;
    case PrimitiveKind::TriangleFan: return GL_TRIANGLE_FAN;
  }
}


/// Ensures the following:
/// - there are valid pointers to vertex buffers;
/// TODO: verify that each buffer contains its own subset of vertex attributes!
/// @returns the number of vertices in the buffers.
void validate(const std::vector<std::shared_ptr<gl::VertexBuffer>>& vertexBuffers) {
  if (vertexBuffers.empty())
    throw std::invalid_argument("No vertex data provided.");

  const bool someAreNull = std::any_of(
      vertexBuffers.begin(), vertexBuffers.end(),
      [&](const auto& buffer) -> bool { return !buffer; }
  );
  if (someAreNull)
    throw std::invalid_argument("Buffers must not be nullptr.");
}

}  // namespace


Mesh::Mesh(
    PrimitiveKind primitives,
    std::shared_ptr<gl::VertexBuffer> vertexData
)
  : _vao{std::make_unique<VertexArrayObject>()}
  , _primitives(primitives)
  , _vertexBuffers(1, std::move(vertexData))
{
  validate(_vertexBuffers);
  attachBuffers();
}

Mesh::Mesh(
    PrimitiveKind primitives,
    std::vector<std::shared_ptr<gl::VertexBuffer>> vertexData
)
  : _vao{std::make_unique<VertexArrayObject>()}
  , _primitives(primitives)
  , _vertexBuffers(std::move(vertexData))
{
  validate(_vertexBuffers);
  attachBuffers();
}

// NOTE: It might be tempting to cache the result, but now that Mesh is no longer immutable (vertex
// data exposed for possible changes) we don't have this option unless we detect changes somehow.
size_t Mesh::countVertices() const {
  size_t vertCount = std::numeric_limits<size_t>::max();

  for (const auto& bufPtr : _vertexBuffers) {
    const auto thisBufVertices = bufPtr->countVertices();
    if (vertCount > thisBufVertices) {
      vertCount = thisBufVertices;
    }
  }

  return vertCount;
}

bool Mesh::providesAttribute(vertex_attributes::Id attr) const {
  for (const auto& buffer : _vertexBuffers) {
    if (buffer->providesAttribute(attr))
      return true;
  }
  return false;
}

absl::Span<const std::shared_ptr<gl::VertexBuffer>> Mesh::vertexData() const {
  return const_cast<Mesh*>(this)->vertexData();
}

absl::Span<std::shared_ptr<gl::VertexBuffer>> Mesh::vertexData() {
  return absl::MakeSpan(_vertexBuffers);
}

void Mesh::draw() const {
  // FIXME: VAO switching should be minimized. All meshes sharing the layout should share one VAO.
  _vao->bind();
  // FIXME: This assumes that the VBO(s) contains just a single mesh data!
  const auto totalVerts = countVertices();
  ASSERT_GL_OK( glDrawArrays(glModeForprimitives(_primitives), 0, totalVerts) );

  // TODO: We could draw primitives in smaller batches as this may improve performance.
#if (defined BATCHED_VERT_RENDERING)
  // NOTE: Batch size must be divisible by the number of vertices per primitive kind drawn.
  const size_t maxBatchSize = 3'000;  // 1000 triangles.
  size_t vertsDrawn = 0U;
  while (vertsDrawn < totalVerts) {
    const size_t batchSize = std::min(maxBatchSize, totalVerts - vertsDrawn);
    ASSERT_GL_OK( glDrawArrays(glModeForprimitives(_primitives), vertsDrawn, batchSize) );
    vertsDrawn += batchSize;
  }
#endif
}

void Mesh::attachBuffers() {
  _vao->bind();
  for (auto& bufferPtr : _vertexBuffers) {
    _vao->attachVertexBuffer(*bufferPtr);
  }
}

Mesh::~Mesh() = default;

}  // namespace keg::gl
