#include "GLObject.hpp"

#include <iostream>

#include <kgl/exceptions.hpp>


namespace keg::gl {

namespace {

// helper type for the visitor #4
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

std::string_view getTypeName(GLObject::TypeTag tag) {
  switch (tag) {
    case GLObject::TypeTag::GLObject: {
      static const std::string_view name = "GLObject";
      return name;
    }
    case GLObject::TypeTag::Buffer: {
      static const std::string_view name = "Buffer";
      return name;
    }
    case GLObject::TypeTag::VBO: {
      static const std::string_view name = "VBO";
      return name;
    }
    case GLObject::TypeTag::VAO: {
      static const std::string_view name = "VAO";
      return name;
    }
    case GLObject::TypeTag::FBO: {
      static const std::string_view name = "FBO";
      return name;
    }
    case GLObject::TypeTag::RBO: {
      static const std::string_view name = "RBO";
      return name;
    }
    case GLObject::TypeTag::Shader: {
      static const std::string_view name = "Shader";
      return name;
    }
    case GLObject::TypeTag::VShader: {
      static const std::string_view name = "VShader";
      return name;
    }
    case GLObject::TypeTag::FShader: {
      static const std::string_view name = "FShader";
      return name;
    }
    case GLObject::TypeTag::GShader: {
      static const std::string_view name = "GShader";
      return name;
    }
    case GLObject::TypeTag::Program: {
      static const std::string_view name = "Program";
      return name;
    }
    case GLObject::TypeTag::Texture: {
      static const std::string_view name = "Texture";
      return name;
    }
  }
}

}  // namespace


// == GLObject ==

GLObject::GLObject(
  TypeTag typeTag, FactoryVariant factory, RecyclerVariant recycler, BindFunc bindFunc
)
  : _handleHolder{create(factory), Deleter{typeTag, std::move(recycler)}}
  , _bindFunc{bindFunc}
{
  assertValidHandle();
}

GLObject::GLObject(TypeTag typeTag, Handle handle, RecyclerVariant recycler, BindFunc bindFunc)
  : _handleHolder{handle, Deleter{typeTag, std::move(recycler)}}
  , _bindFunc{bindFunc}
{
  assertValidHandle();
}

GLObject::GLObject(TypeTag typeTag, Handle handle, BindFunc bindFunc)
  : _handleHolder{handle, Deleter{typeTag}}
  , _bindFunc{bindFunc}
{
  // NOTE: Not checking the handle value, since for "default" ones they typically have "zero" value.
}

GLObject::~GLObject() = default;

void GLObject::bind(::gl::GLenum target) const {
  if (!_bindFunc)
    return;

  ASSERT_GL_OK( _bindFunc(target, handle()) );
}

// static
GLObject::Handle GLObject::create(const GLObject::FactoryVariant& factory) {
  return std::visit(
      overloaded{
        [](const SimpleFactoryFunc& simpleFactoryFunc) {
          return simpleFactoryFunc();
        },
        [](const MultiFactoryFunc& multiFactoryFunc) {
          Handle result;
          ASSERT_GL_OK( multiFactoryFunc(1, &result) );
          return result;
        },
      },
      factory
  );
}

void GLObject::assertValidHandle() const {
  if (!_handleHolder.get()) {
    throw std::runtime_error(
        "Got invalid OpenGL object handle. " + std::string{describeGLError()}
    );
  }
}

// == GLObject::Deleter ==

GLObject::Deleter::Deleter(TypeTag typeTag)
  : _typeTag{typeTag}
{}

GLObject::Deleter::Deleter(TypeTag typeTag, RecyclerVariant recycler)
  : _recycler{std::move(recycler)}
  , _typeTag{typeTag}
{}

void GLObject::Deleter::operator()(Handle handle) {
  std::visit(
      overloaded{
        [handle](const SimpleRecyclerFunc& simpleRecyclerFunc) {
          if (!simpleRecyclerFunc)
            return;
          ASSERT_GL_OK( simpleRecyclerFunc(handle) );
        },
        [handle](const MultiRecyclerFunc& multiRecyclerFunc) {
          if (!multiRecyclerFunc)
            return;
          ASSERT_GL_OK( multiRecyclerFunc(1, &handle) );
        },
      },
      _recycler
  );
}

}  // namespace keg::gl
