#include <kgl/exceptions.hpp>

#include <map>

#include <kgl/bindings.hpp>

namespace keg::gl {
using namespace ::gl;

OpenGLException::OpenGLException(const ::gl::GLenum errorCode)
  : std::exception()
  , _errorCode(errorCode)
{}

const char* OpenGLException::what() const _NOEXCEPT {
  return describeThisGLError(_errorCode).data();
}

std::string_view describeGLError() {
  return describeThisGLError(glGetError());
}

std::string_view describeThisGLError(const ::gl::GLenum error) {
  static const std::map<GLenum, std::string_view> descriptions {
    { GL_NO_ERROR                       , "" },
    { GL_INVALID_ENUM                   , "Invalid GL enum value." },
    { GL_INVALID_VALUE                  , "GL function argument is out of range." },
    { GL_INVALID_OPERATION              , "The specified GL operation is not allowed in the current state." },
    { GL_STACK_OVERFLOW                 , "A stack pushing operation cannot be done because it would overflow the limit of that stack's size." },
    { GL_STACK_UNDERFLOW                , "A stack popping operation cannot be done because the stack is already at its lowest point." },
    { GL_OUT_OF_MEMORY                  , "Memory cannot be allocated." },
    { GL_INVALID_FRAMEBUFFER_OPERATION  , "Read/write attempt on a framebuffer that is not complete." },
    { GL_CONTEXT_LOST                   , "GL context has been lost, due to a graphics card reset." },
  };
  static const std::string_view unknownError = "Unknown GL error.";

  const auto iter = descriptions.find(error);
  if (iter != descriptions.end()) {
    return iter->second;
  }
  return unknownError;
}

void assertNoGLError(std::string_view lastOp) {
  auto errorDesc = describeGLError();
  if (errorDesc.empty())
    return;

  const auto prefix = lastOp.empty() ? std::string{} :
      std::string{"OpenGL operation "} + std::string{lastOp} + " failed: ";
  throw std::runtime_error(prefix + std::string{errorDesc});
}

}  // namespace keg::gl
