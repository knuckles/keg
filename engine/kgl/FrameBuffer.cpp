#include "FrameBuffer.hpp"

#include <array>
#include <numeric>

#include <kgl/bindings.hpp>

#include <kgl/exceptions.hpp>
#include <kgl/Texture.hpp>


namespace keg::gl {
using namespace ::gl;

// static
FrameBuffer& FrameBuffer::getDefaultFrameBuffer() {
  static FrameBuffer defaultFBO(0);
  return defaultFBO;
}

FrameBuffer::FrameBuffer()
  : GLObject{GLObject::TypeTag::FBO, glGenFramebuffers, glDeleteFramebuffers, glBindFramebuffer}
{}

FrameBuffer::FrameBuffer(gl::GLuint handle)
  : GLObject{GLObject::TypeTag::FBO, handle, glBindFramebuffer}
{}

GLint FrameBuffer::getMaxColorAttachments() {
  static const GLint value = []() {
    GLint value = 0;
    ASSERT_GL_OK( glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &value) );
    return value;
  }();
  return value;
}

gl::GLint FrameBuffer::attachColorTexture(Texture&& texture) {
  // TODO: Ensure we're not exceeding max allowed number of textures.
  const gl::GLint futureTextureUnit = _attachedTextures.size();

  bind(GL_FRAMEBUFFER);
  ASSERT_GL_OK(
      glFramebufferTexture2D(
      /* target     */ GL_FRAMEBUFFER,
      /* attachment */ GL_COLOR_ATTACHMENT0 + _attachedTextures.size(),
      /* textarget  */ GL_TEXTURE_2D,
      /* texture    */ texture.handle(),
      /* level      */ 0
      )
  );
  _attachedTextures.push_back(std::move(texture));

  std::vector<gl::GLenum> attachments(_attachedTextures.size());
  for (unsigned i = 0; i < attachments.size(); ++i) {
    attachments[i] = GL_COLOR_ATTACHMENT0 + i;
  }
  // Request the fragment shader output values to be written into the attached textures.
  // TODO: This might be wasteful to do after each added texture.
  ASSERT_GL_OK( glDrawBuffers(attachments.size(), attachments.data()) );

  return futureTextureUnit;
}

std::vector<Texture> FrameBuffer::detachAllColorTextures() {
  bind(GL_FRAMEBUFFER);
  for (unsigned slotIndex = 0; slotIndex < _attachedTextures.size(); ++slotIndex) {
    ASSERT_GL_OK(
        glFramebufferTexture2D(
        /* target     */ GL_FRAMEBUFFER,
        /* attachment */ GL_COLOR_ATTACHMENT0 + slotIndex,
        /* textarget  */ GL_TEXTURE_2D,
        /* texture    */ 0,  // This detaches the texture.
        /* level      */ 0
        )
    );
  }
  static const std::array<gl::GLenum, 1> attachments{GL_NONE};
  ASSERT_GL_OK( glDrawBuffers(attachments.size(), attachments.data()) );

  std::vector<Texture> result;
  std::swap(_attachedTextures, result);
  return result;
}

void FrameBuffer::useAllColorTextures() {
  unsigned nextTextureUnit = 0;
  for (auto& texture : _attachedTextures) {
    texture.use(nextTextureUnit);
    nextTextureUnit++;
  }
}

void FrameBuffer::attachDepthRenderBuffer(RenderBuffer&& rbo) {
  bind(GL_FRAMEBUFFER);
  ASSERT_GL_OK( glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo.handle()
  ));
  _attachedRenderBuffers.push_back(std::move(rbo));
}

std::vector<RenderBuffer> FrameBuffer::detachAllRenderBuffers() {
  bind(GL_FRAMEBUFFER);
  ASSERT_GL_OK( glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0
  ));
  std::vector<RenderBuffer> result;
  std::swap(_attachedRenderBuffers, result);
  return result;
}

//void FrameBuffer::readPixels(std::vector<char>& dest) {
  // TODO: Bind self as GL_READ_FRAMEBUFFER before calling `glReadPixels`

  // The color values read via glReadPixels may be clamped to the [0, 1] range. This is controlled
  // via this function: void glClampColor(GLenum target​, GLenum clamp​);
  //
  // The target​ must be GL_CLAMP_READ_COLOR​. The clamp​ can be set to one of the following:
  // GL_TRUE​: Clamping is always on, no matter what the format​ or type​ parameters of the read pixels
  //   call.
  // GL_FALSE​: Clamping is always off, no matter what the format​ or type​ parameters of the read
  //   pixels call.
  // GL_FIXED_ONLY​: Clamping is only on if the type of the image being read is a normalized signed
  //   or unsigned value.
  //
  // Note that the clamping behavior is not framebuffer object state. It will not be stored with the
  // current framebuffer.
//}

void FrameBuffer::blitFrom(
    const FrameBuffer& source,
    gl::ClearBufferMask componentsMask,
    const glm::ivec4& sourceRect,
    const glm::ivec4& destRect,
    gl::GLenum filter
) {
  source.bind(::gl::GL_READ_FRAMEBUFFER);
  bind(::gl::GL_DRAW_FRAMEBUFFER);

  ASSERT_GL_OK(::gl::glBlitFramebuffer(
      sourceRect.x, sourceRect.y, sourceRect.z, sourceRect.w,
      destRect.x, destRect.y, destRect.z, destRect.w,
      componentsMask,
      filter
  ));
}

GLenum FrameBuffer::status() const {
  bind(GL_FRAMEBUFFER);
  return glCheckFramebufferStatus(GL_FRAMEBUFFER);
}

};
