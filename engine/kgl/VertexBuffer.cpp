#include "VertexBuffer.hpp"

#include <kgl/bindings.hpp>

#include <kgl/exceptions.hpp>

namespace keg::gl {
using namespace ::gl;

namespace {

constexpr uint8_t sizeOfGLNumericType(::gl::GLenum numTypeId) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"  // Suppress unhandled enum values warning.
  switch (numTypeId) {
    case GL_BYTE: return sizeof(GLbyte);
    case GL_UNSIGNED_BYTE: return sizeof(GLubyte);
    case GL_SHORT: return sizeof(GLshort);
    case GL_UNSIGNED_SHORT: return sizeof(GLushort);
    case GL_INT: return sizeof(GLint);
    case GL_UNSIGNED_INT: return sizeof(GLuint);
    case GL_FLOAT: return sizeof(GLfloat);
    case GL_DOUBLE: return sizeof(GLdouble);
  }
#pragma GCC diagnostic pop
  throw std::runtime_error("Unknown GL numeric type id.");
}


constexpr GLenum toAttachmentPoint(VertexBuffer::Kind kind) {
  switch (kind) {
    case VertexBuffer::Kind::Index: return GL_ELEMENT_ARRAY_BUFFER;
    case VertexBuffer::Kind::Vertex: return GL_ARRAY_BUFFER;

    // TODO: There are other types of buffers OpenGL 3.3
    // See: https://www.khronos.org/opengl/wiki/Buffer_Object#General_use
    // GL_COPY_READ_BUFFER
    // GL_COPY_WRITE_BUFFER
    // GL_PIXEL_PACK_BUFFER
    // GL_PIXEL_UNPACK_BUFFER
    // GL_TEXTURE_BUFFER
    // GL_TRANSFORM_FEEDBACK_BUFFER
    // GL_UNIFORM_BUFFER
  }
}

// At the moment this class supports just this kind of attachment.
constexpr auto VBO_ATTACHMENT = toAttachmentPoint(VertexBuffer::Kind::Vertex);
}  // namespace


// == DataDescription

DataDescription::DataDescription(gl::GLenum dataType, gl::GLsizeiptr elements)
  : dataType{dataType}
  , elementSize{sizeOfGLNumericType(dataType)}
  , elements{elements}
  , size{elements * elementSize}
{}


// == StrideLayout

StrideLayout::StrideLayout(AttributesList attributes)
  : StrideLayout{absl::MakeConstSpan(attributes.begin(), attributes.end())}
{}

StrideLayout::StrideLayout(AttributesSpan attributes) {
  if (std::empty(attributes))
    throw std::invalid_argument("No attributes specififed.");

  for (const auto& item : attributes) {
    _attrsMap.insert({item.first, AttributeLocation{_strideLength, item.second}});
    _strideLength += item.second;
  }
}

bool StrideLayout::hasAttribute(vertex_attributes::Id attr) const {
  return _attrsMap.find(attr) != _attrsMap.end();
}


// == VertexBuffer ==

VertexBuffer::VertexBuffer(StrideLayout layout, ::gl::GLenum dataType)
  : GLObject{GLObject::TypeTag::VBO, glGenBuffers, glDeleteBuffers, glBindBuffer}
  , _reservation{dataType, 0}
  , _layout{std::move(layout)}
{}

VertexBuffer::VertexBuffer(const DataAdapter& data, StrideLayout layout)
  : GLObject{GLObject::TypeTag::VBO, glGenBuffers, glDeleteBuffers, glBindBuffer}
  , _reservation{data.description()}
  , _layout{std::move(layout)}
{
  checkDataSize(data.description());
  allocateStorage(data.description(), data.start());
}

size_t VertexBuffer::capacity() const {
  return _reservation.elements / _layout.strideLength();
}

size_t VertexBuffer::countVertices() const {
  return _elementsUsed / _layout.strideLength();
}

bool VertexBuffer::providesAttribute(vertex_attributes::Id attr) const {
  return _layout.hasAttribute(attr);
}

size_t VertexBuffer::strideSize() const {
  return _layout.strideLength() * _reservation.elementSize;
}

bool VertexBuffer::reserve(size_t numVertices) {
  const auto elementsNeeded = numVertices * _layout.strideLength();
  if (_reservation.elements >= elementsNeeded)
    return false;

  allocateStorage(DataDescription{_reservation.dataType, elementsNeeded}, nullptr);
  return true;
}

size_t VertexBuffer::setData(const DataAdapter& data) {
  checkDataSize(data.description());

  bind();

  const auto canWrite = std::min(data.description().size, _reservation.size);
  ASSERT_GL_OK(glBufferSubData(VBO_ATTACHMENT, 0, canWrite, data.start()));
  _elementsUsed = data.description().elements;

  return data.description().size - canWrite;
}

void VertexBuffer::allocateStorage(const DataDescription& dataDescr, const void* data) {
  bind();

  // TODO: Pass usage to constructor:
  // - GL_STATIC_DRAW is for vertex buffers that are rendered many times, and whose contents are
  //    specified once and never change.
  // - GL_DYNAMIC_DRAW is for vertex buffers that are rendered many times, and whose contents change
  //    during the rendering loop.
  // - GL_STREAM_DRAW is for vertex buffers that are rendered a small number of times and then
  //    discarded.
  ASSERT_GL_OK(glBufferData(VBO_ATTACHMENT, dataDescr.size, data, GL_STATIC_DRAW));

  // Validate buffer contents size.
  GLint actualSize = 0;
  glGetBufferParameteriv(VBO_ATTACHMENT, GL_BUFFER_SIZE, &actualSize);
  if (dataDescr.size != actualSize) {
    throw std::runtime_error("OpenGL buffer size mismatch: "
        + std::to_string(dataDescr.size) + " vs. " + std::to_string(actualSize));
  }
  _reservation = dataDescr;
  _elementsUsed = data ? dataDescr.elements : 0;
}

void VertexBuffer::checkDataSize(const DataDescription& dataDescr) {
  if (dataDescr.elements % _layout.strideLength() != 0) {
    // This means the data layout is likely different from what this buffer (and its user) expects.
    throw std::runtime_error("The number of data elements is not a multiple of the stride length.");
  }
}

void VertexBuffer::bind() {
  GLObject::bind(VBO_ATTACHMENT);
}

}  // namespace keg::gl
