#pragma once

#include <array>
#include <map>
#include <vector>
#include <utility>

#include <absl/types/span.h>

#include <kgl/data_types.hpp>
#include <kgl/GLObject.hpp>
#include <util/macros.hpp>


namespace keg::gl {

/// Describes the format of the vertex attribute data.
/// The data described here may represent one or more different vertex attributes.
struct DataDescription {
  template<typename E>
  static DataDescription construct(::gl::GLsizeiptr elements) {
    return DataDescription(glNumericTypeId<std::remove_cv_t<E>>(), elements);
  }

  DataDescription(::gl::GLenum dataType, ::gl::GLsizeiptr elements);

  /// Numeric type specifier. Suitable for passing into e.g. `glVertexAttribPointer`.
  ::gl::GLenum dataType;

  /// Size of the data for a single vertex, in bytes.
  uint8_t elementSize = 0;

  /// Number of elements (vertices).
  size_t elements = 0;

  /// Total data size, in bytes.
  size_t size = 0;
};


/// An adapter class used to pass the vertex data to a VBO. Data may represent one or more different
/// vertex attributes and the actual layout is described by its @c StrideLayout.
/// Automatically calculates some GL properties required to pass the data over to GPU.
class DataAdapter {
public:

  const void* start() const { return _start; }
  const DataDescription& description() const { return _description; }

  /// Initialize from a span of primitive values.
  /// @tparam E - type of the primitives inside the span (normally deduced automatically).
  /// @param in - the data span.
  template<typename E>
  DataAdapter(absl::Span<const E> in)
    : _start{ !in.empty() ? in.data() : nullptr }
    , _description{ DataDescription::construct<E>(in.size()) }
  {}

  /// Initialize from a span of structs S, consisting strictly of primitive values of type E.
  /// In this scenario struct S contains one or more vertex attributes (e.g. 3 floats for position
  /// coordinates, plus 2 floats for texture coordinates).
  /// WARNING: S must be a packed struct for the size calculation to work properly!
  /// @tparam E - type of the primitives carried inside the struct S.
  /// @tparam S - element type in the span (normally deduced automatically).
  /// @param in - the structs span.
  template<typename E, typename S>
  static DataAdapter fromStructs(absl::Span<const S> in) {
    static_assert(
        sizeof(S) % sizeof(E) == 0,
        "Struct size is not a multiple of claimed element size."
    );
    return DataAdapter{absl::MakeConstSpan(
        reinterpret_cast<const E*>(in.data()),
        in.size() * (sizeof(S) / sizeof(E))
    )};
  }

private:
  const void* _start = nullptr;
  DataDescription _description;
};


/// A stride contains attributes for a single vertex. This class represents the layout of various
/// vertex attributes inside a stride.
class StrideLayout {
public:  // Types.
  /// Denotes a single attribute within a stride.
  /// First element is the attribute Id.
  /// Second element is the number of primitives this attribute spans over in the buffer's stride.
  using AttributeItem = std::pair<vertex_attributes::Id, unsigned /*elements*/>;
  using AttributesList = std::initializer_list<AttributeItem>;
  using AttributesSpan = absl::Span<const AttributeItem>;

  /// Describes location (offset and length) of some vertex attribute inside the buffer stride.
  struct AttributeLocation {
    unsigned offsetElements = 0;  // Offset from a stride start to this attribute's data.
    unsigned elements = 0;  // How many elements this attribute spans over in a whole stride.
  };
  /// Describes the location of all vertex attributes.
  using AttributesMap = std::map<vertex_attributes::Id, AttributeLocation>;

public:  // Methods.
  /// Construct the stride layout from a sequence of attribute items.
  StrideLayout(AttributesList attributes);
  StrideLayout(AttributesSpan attributes);

  bool hasAttribute(vertex_attributes::Id attr) const;
  const AttributesMap& attributesMap() const { return _attrsMap; }

  /// Length of stride in primitive elements.
  unsigned strideLength() const { return _strideLength; }

private:
  AttributesMap _attrsMap;
  unsigned _strideLength = 0;
};


// TODO: Pass by value instead of wrapping in unique_ptr
// TODO: Make another attempt on extracting the base Buffer class.
// See: https://www.khronos.org/opengl/wiki/Buffer_Object
class VertexBuffer : public GLObject {
public:  // Decls
  enum class Kind {
    Vertex,
    Index,
  };

public:  // Initializers
  NON_COPYABLE(VertexBuffer)
  NON_MOVABLE(VertexBuffer)  // DataDescription is non-movable at the moment.

  /// Create an empty buffer. @c `setData` to populate the buffer with vertex attributes.
  /// @param layout - the layout of the data.
  /// @param dataType - enum value identifying the type of the numeric values used for data.
  VertexBuffer(StrideLayout layout, ::gl::GLenum dataType);

  /// Create a buffer and fill it with data.
  /// @param layout - describes the data layout.
  /// @param data - vertex attribute data.
  VertexBuffer(const DataAdapter& data, StrideLayout layout);

public:  // Accessors
  /// Capacity and actual number of vertices held in the buffer.
  size_t capacity() const;
  size_t countVertices() const;

  const DataDescription& dataDescr() const { return _reservation; }
  const StrideLayout& layout() const { return _layout; }
  size_t strideSize() const;
  bool providesAttribute(vertex_attributes::Id attr) const;

public:  // Mutators
  /// Use the buffer as current VBO.
  void bind();

  /// Ensure storage has the capacity to hold attributes for the given number of vertices.
  /// @note if the current capacity is below the requested value, the storage shall be reallocated
  /// loosing all currently stored data.
  /// @param numVertices - number of vertices to allocate the buffer for.
  /// @returns whether the storage has been reallocated.
  bool reserve(size_t numVertices);

  /// Copies data to the GPU storage.
  /// @returns the number of bytes which could not have been written out due to storage shortage.
  size_t setData(const DataAdapter& data);

private:
  void allocateStorage(const DataDescription& descr, const void* data);
  void checkDataSize(const DataDescription& dataDescr);

  DataDescription _reservation;
  size_t _elementsUsed;
  StrideLayout _layout;
};

}  // namespace keg::gl
