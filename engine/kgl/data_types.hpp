#pragma once

#include <array>

#include <glbinding/gl33core/types.h>
#include <NamedType/named_type.hpp>

namespace keg::gl {

namespace vertex_attributes {
  /// Identifies a vertex attribute.
  using Id = fluent::NamedType<::gl::GLuint, struct AttributeIdTag, fluent::Comparable>;

  /// These constants define the locations of various vertext attributes as expected by the shaders.
  extern const Id POSITIONS;
  extern const Id COLORS;
  extern const Id NORMALS;
  extern const Id UVS;
  extern const Id BONE_INDICES;
  extern const Id BONE_WEIGHTS;

  /// A collection of all possible vertex attribut Ids.
  extern const std::array<Id, 6> ALL_VATTRIBUTE_IDS;
};  // namespace vertex_attributes


/// Converts C++ numeric type into GL type enum value.
/// The idea here is to insulate from exact OpenGL defines which would require its complete header.
template<typename T>
::gl::GLenum glNumericTypeId() = delete;
template<>
::gl::GLenum glNumericTypeId<::gl::GLbyte>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLubyte>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLshort>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLushort>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLint>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLuint>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLfloat>();
template<>
::gl::GLenum glNumericTypeId<::gl::GLdouble>();

};  // namespace keg::gl
