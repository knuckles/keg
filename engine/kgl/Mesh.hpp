#pragma once

#include <memory>
#include <vector>

#include <absl/types/span.h>

#include <kgl/data_types.hpp>
#include <util/macros.hpp>


// TODO: Mesh class is a slightly higher level abstraction and might more appropriate in another namespace.
namespace keg::gl {

class VertexArrayObject;
class VertexBuffer;

enum class PrimitiveKind {
  Points,
  Lines,
  LineLoop,
  LineStrip,
  Triangles,
  TriangleStrip,
  TriangleFan,
};


/// This class mostly incapsulates one or more buffers containing mesh vertex attributes.
class Mesh {
public:
  // For ease of passing skinning data into the shader as vertex attributes we use ivec4/vec4.
  // Thus each vertext can only be influenced by 4 bones at most.
  // NOTE: This must be in sync with code in generic_vs.in.glsl:
  // TODO: Keep in sync with the shader code using preprocessor definitions.
  static constexpr size_t BONES_PER_VERTEX = 4;
  static constexpr size_t MAX_BONES = 200;

  NON_COPYABLE(Mesh)
  NON_MOVABLE(Mesh)

  /// Construct mesh from a single buffers holding one or more vertex attributes.
  explicit Mesh(
      PrimitiveKind primitives,
      std::shared_ptr<gl::VertexBuffer> vertexData
  );

  // TODO: Mesh should merely reference a subrange of data in a vertex buffer.
  /// Construct mesh from a number of buffers holding different vertex attributes.
  ///
  /// NOTE: Although it is possible to store different vertex attributes in separate buffers, a much
  /// more performant scenario is to have a single buffer for all mesh attributes or even those of
  /// multiple meshes.
  explicit Mesh(
      PrimitiveKind primitives,
      std::vector<std::shared_ptr<gl::VertexBuffer>> vertexData
  );

  virtual ~Mesh();

  size_t countVertices() const;
  PrimitiveKind primitives() const { return _primitives; }
  bool providesAttribute(vertex_attributes::Id attr) const;
  absl::Span<const std::shared_ptr<gl::VertexBuffer>> vertexData() const;
  absl::Span<std::shared_ptr<gl::VertexBuffer>> vertexData();

  void draw() const;

private:
  /// Attaches its buffers to the VAO, binding their attributes.
  void attachBuffers();

  /// Holds the vertex attribute mapping for our buffers.
  std::unique_ptr<VertexArrayObject> _vao;

  /// Defines the organization of the elements of all our vertex buffers.
  PrimitiveKind _primitives;

  /// Actual vertex data used to draw the mesh.
  std::vector<std::shared_ptr<gl::VertexBuffer>> _vertexBuffers;
};

}  // namespace keg::gl
