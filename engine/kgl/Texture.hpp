#pragma once

#include <string_view>

#include <glbinding/gl33core/types.h>

#include <kgl/GLObject.hpp>
#include <util/macros.hpp>


namespace keg::gl {

/// Holds an OpenGL 2D texture which may be either generated, or decoded from an external source.
/// Encoding formats are those supported by the STB library: JPEG, PNG, BMP, PSD, DDS, HDR, TGA.
class Texture : public GLObject {
public:
  NON_COPYABLE(Texture)
  DEFAULT_MOVABLE(Texture)

  /// Allocate "empty" texture.
  explicit Texture(
      ::gl::GLsizei width,
      ::gl::GLsizei height,
      ::gl::GLenum internalFormat,
      ::gl::GLenum colorFormat,
      ::gl::GLenum numericFormat
  );

  /// Create a texture by loading it from a file.
  ///
  /// @param filePath - path to file containing the texture.
  explicit Texture(std::string_view filePath);

  /// Create a texture from encoded image data.
  ///
  /// @param encodedData - pointer to the encoded image data.
  /// @param dataLength - length of encoded data.
  explicit Texture(const unsigned char* encodedData, size_t dataLength);

  ::gl::GLsizei width() const { return _width; }
  ::gl::GLsizei height() const { return _height; }
  int channels() const { return _numChannels; }

  /// Binds the texture to the specified texture unit.
  /// In order to use the texture in a shader set the sampler uniform to the value passed here.
  ///
  /// @param textureUnitIdx - index of the texture unit to bind the texture to. Values: [0, 31].
  void use(unsigned textureUnitIdx) const;

  static void reset(unsigned textureUnitIdx);

private:
  void assignImageData(unsigned char* rawImageData);

  /// Sets some sane defaults to texture parameters (e.g. filtering).
  /// TODO: These should be set from the outside.
  void setBasicParameters();

  ::gl::GLsizei _width = 0;
  ::gl::GLsizei _height = 0;
  int _numChannels = 0;  // NOTE: It's `int` because of SOIL's getter signature which expects one.
};

}  // namespace keg::gl
