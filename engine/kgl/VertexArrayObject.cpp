#include "VertexArrayObject.hpp"

#include <glbinding/gl33core/enum.h>
#include <glbinding/gl33core/functions.h>

#include <kgl/exceptions.hpp>
#include <kgl/VertexBuffer.hpp>


namespace keg::gl {
using namespace ::gl;

VertexArrayObject::VertexArrayObject()
  : GLObject{
      GLObject::TypeTag::VAO,
      glGenVertexArrays,
      glDeleteVertexArrays,
      [](::gl::GLenum /* ignored */, Handle handle) { glBindVertexArray(handle); }
    }
{}

void VertexArrayObject::bind() {
  GLObject::bind(::gl::GLenum{} /* ignored */);
}

void VertexArrayObject::disableAllAttributes() {
  bind();
  for (const auto attribute : vertex_attributes::ALL_VATTRIBUTE_IDS) {
    ASSERT_GL_OK( glDisableVertexAttribArray(attribute.get()) );
  }
}

void VertexArrayObject::attachVertexBuffer(VertexBuffer& vbo) {
  // glBindBuffer sets a "global variable", then glVertexAttribPointer only reads that global variable
  // and stores it in the VAO together with the attribute layout spec.
  // NOTE: it is an error to call the glVertexAttribPointer functions if 0 is currently bound as
  // GL_ARRAY_BUFFER - i.e. without a buffer bound.
  vbo.bind();

  for (const auto& [attributeId, loc] : vbo.layout().attributesMap()) {
    // Enabling should go first. It's claimed that some drivers dislike it otherwise.
    ASSERT_GL_OK(glEnableVertexAttribArray(attributeId.get()));

    // When we call glVertexAttribPointer, the name of the buffer object currently bound as
    // GL_ARRAY_BUFFER is recorded for the specified index and stored in the VAO. That's how a VAO
    // associates indices and buffer object. The corresponding value can be later queried with
    // glGetVertexAttribiv() and VERTEX_ATTRIB_ARRAY_BUFFER_BINDING.

    switch (vbo.dataDescr().dataType) {
      case ::gl::GL_BYTE: [[fallthrough]];
      case ::gl::GL_UNSIGNED_BYTE: [[fallthrough]];
      case ::gl::GL_SHORT: [[fallthrough]];
      case ::gl::GL_UNSIGNED_SHORT: [[fallthrough]];
      case ::gl::GL_INT: [[fallthrough]];
      case ::gl::GL_UNSIGNED_INT: {
        ASSERT_GL_OK(
          glVertexAttribIPointer(
          /* index    */ attributeId.get(),
          /* size     */ loc.elements,
          /* type     */ vbo.dataDescr().dataType,
          /* stride   */ static_cast<GLsizei>(vbo.strideSize()),
          /* pointer  */ reinterpret_cast<GLvoid*>(loc.offsetElements * vbo.dataDescr().elementSize)
          )
        );
        break;
      }
      case ::gl::GL_FLOAT: {
        ASSERT_GL_OK(
          glVertexAttribPointer(
          /* index    */ attributeId.get(),
          /* size     */ loc.elements,
          /* type     */ ::gl::GL_FLOAT,
          /* norm     */ GL_FALSE,  // TODO: Store in VertexBuffer::AttributeLocation.
          /* stride   */ static_cast<GLsizei>(vbo.strideSize()),
          /* pointer  */ reinterpret_cast<GLvoid*>(loc.offsetElements * vbo.dataDescr().elementSize)
          )
        );
        break;
      }
      case ::gl::GL_DOUBLE: {
        ASSERT_GL_OK(
          glVertexAttribLPointer(
          /* index    */ attributeId.get(),
          /* size     */ loc.elements,
          /* type     */ ::gl::GL_DOUBLE,
          /* stride   */ static_cast<GLsizei>(vbo.strideSize()),
          /* pointer  */ reinterpret_cast<GLvoid*>(loc.offsetElements * vbo.dataDescr().elementSize)
          )
        );
        break;
      }
      default:
        break;
    }
  }  // for
}

}  // namespace keg::gl
