#pragma once

#include <functional>
#include <memory>
#include <variant>

#include <glbinding/gl33core/types.h>

#include <util/macros.hpp>


namespace keg::gl {

// Here are some interesting notes about OpenGL from Chromium devs:
// https://www.chromium.org/developers/design-documents/gpu-command-buffer/#opengl-quirks-to-be-aware-of

/// Abstract GL object base class.
class GLObject {
public:  // Type declarations.
  enum class TypeTag {
    GLObject,
    Buffer,
    VBO,
    VAO,
    FBO,
    RBO,
    Shader,
    VShader,
    FShader,
    GShader,
    Program,
    Texture,
  };


  /// Native handle type.
  using Handle = ::gl::GLuint;

  /// Creates a GL object without needing any arguments.
  using SimpleFactoryFunc = std::function<Handle ()>;

  /// Creates a number of GL objects of the same type.
  using MultiFactoryFunc = std::function<void (::gl::GLsizei count, Handle* handles)>;

  /// Recycles a single GL object given its handle.
  using SimpleRecyclerFunc = std::function<void (const Handle)>;

  /// Recycles a number of GL objects.
  using MultiRecyclerFunc = std::function<void (::gl::GLsizei count, const Handle* handles)>;

  /// Type of a function binding this object to a GL context's target attachment point.
  using BindFunc = std::function<void (::gl::GLenum target, Handle)>;

public:  // Methods.
  NON_COPYABLE(GLObject)
  DEFAULT_MOVABLE(GLObject)
  virtual ~GLObject();

  /// Obtains the object's native handle.
  Handle handle() const { return _handleHolder.get(); }

protected:
  using FactoryVariant = std::variant<SimpleFactoryFunc, MultiFactoryFunc>;
  using RecyclerVariant = std::variant<SimpleRecyclerFunc, MultiRecyclerFunc>;

  /// Create a GL object using the provided factory function.
  ///
  /// @param factory A factory creating instances of this kind of object.
  /// @param recycler A recycler capable of reclaiming an instance of this kind of object.
  /// @param bindFunt Function making this object "current" in its area of responsibility. Normally,
  ///   this will modify GL's global state binding the object to some attachment point.
  explicit GLObject(TypeTag typeTag, FactoryVariant factory, RecyclerVariant recycler, BindFunc bindFunc);

  /// Constructs from an existing handle and takes ownership of it.
  ///
  /// @param handle An existing GL object handle.
  /// @param recycler A recycler capable of reclaiming an instance of this kind of object.
  /// @param bindFunt Function making this object "current" in its area of responsibility. Normally,
  ///   this will modify GL's global state binding the object to some attachment point.
  explicit GLObject(TypeTag typeTag, Handle handle, RecyclerVariant recycler, BindFunc bindFunc);

  /// Constructs from an existing handle.
  /// NOTE: This will NOT take ownership of the handle - i.e. won't destroy the GL object behind!
  /// This is intended to wrap "default" objects which are provided and managed by the GL context.
  ///
  /// @param handle An existing GL object handle.
  /// @param bindFunt Function making this object "current" in its area of responsibility. Normally,
  ///   this will modify GL's global state binding the object to some attachment point.
  explicit GLObject(TypeTag typeTag, Handle handle, BindFunc bindFunc);

  /// Binds the object to the specified target attachment point.
  void bind(::gl::GLenum target) const;

private:
  static Handle create(const GLObject::FactoryVariant& factory);

  void assertValidHandle() const;

  /// Object's deleter adapter for use in std::unique_ptr.
  struct Deleter {
    /// Declares the type to pass to the deleter function.
    using pointer = Handle;

    /// No-op deleter.
    explicit Deleter(TypeTag typeTag);

    /// Deleter using the provided recycler variant.
    explicit Deleter(TypeTag typeTag, RecyclerVariant recycler);

    /// Deletes the GL object stored in unique_ptr.
    void operator()(Handle handle);

  private:
    RecyclerVariant _recycler{};
    TypeTag _typeTag = TypeTag::GLObject;
  };

  std::unique_ptr<::gl::GLuint, Deleter> _handleHolder;
  BindFunc _bindFunc;
};

}  // namespace keg::gl
