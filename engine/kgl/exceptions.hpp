#pragma once

#include <stdexcept>
#include <string>
#include <string_view>

#include <glbinding/gl33core/types.h>

#include <util/macros.hpp>

namespace keg {
namespace gl {

class OpenGLException : public std::exception {
public:
  explicit OpenGLException(const ::gl::GLenum errorCode);
  ::gl::GLenum errorCode() const { return _errorCode; }

  const char* what() const noexcept override;

private:
  ::gl::GLenum _errorCode{0};
};


std::string_view describeGLError();
std::string_view describeThisGLError(const ::gl::GLenum error);
void assertNoGLError(std::string_view lastOp = "");

}  // namespace gl
}  // namespace keg


#if defined(NDEBUG)
#define ASSERT_GL_OK(glCall) glCall
#else
// FIXME: This can't return the result of the `glCall` expression.
#define ASSERT_GL_OK(glCall) \
  do { \
    glCall; \
    ::keg::gl::assertNoGLError(STRINGIFY(glCall)); \
  } while (false)
#endif

// TODO: Try out the alternative library solution:
// #include <glbinding-aux/debug.h>
// glbinding::aux::enableGetErrorCallback();
