/// An abstract interface allowing to set shader unifirm variables.

#pragma once
#include <cstdint>
#include <string_view>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/fwd.hpp>
#undef GLM_FORCE_SILENT_WARNINGS


namespace keg::gl {

class ShaderUniformsSink {
public:
  virtual ~ShaderUniformsSink() {};

  virtual bool varExists(std::string_view varName) const noexcept = 0;

  // TODO: Overload for all possible argument types?
  // TODO: Support Uniform Buffer Objects. See https://www.khronos.org/opengl/wiki/Uniform_Buffer_Object
  virtual void setUniformVar(std::string_view varName, int val) = 0;
  virtual void setUniformVar(std::string_view varName, bool val) = 0;
#if 0
  // TODO: Somehow, using this to set texture unit index for a sampler2D variable fails.
  virtual void setUniformVar(std::string_view varName, uint val) = 0;
#endif
  virtual void setUniformVar(std::string_view varName, float val) = 0;
  virtual void setUniformVar(std::string_view varName, const glm::vec2& val) = 0;
  virtual void setUniformVar(std::string_view varName, const glm::vec3& val) = 0;
  virtual void setUniformVar(std::string_view varName, const glm::vec4& val) = 0;
  virtual void setUniformVar(std::string_view varName, const glm::mat4& val) = 0;
  virtual void setUniformVar(std::string_view varName, const std::vector<glm::mat4>& val) = 0;
};

}  // namespace keg
