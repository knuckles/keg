#include "Material.hpp"

#include <stdexcept>

#include <glm/vec3.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <kgl/ShaderUniformsSink.hpp>
#include <kgl/Texture.hpp>


#define SET_MATERIAL_PROP_VAR(varName)        \
  if (shaders.varExists(#varName)) { \
    shaders.setUniformVar(#varName, _properties.varName); \
  }

namespace keg::gl {

std::ostream& operator<<(std::ostream& os, const MaterialProperties::Color& color) {
  fmt::print(os, "[{}, {}, {}]", color.x, color.y, color.z);
  return os;
}

std::ostream& operator<<(std::ostream& os, const MaterialProperties& properties) {

#define LOG_PROP(props, propName) \
  << " - '" << #propName << "': " << props.propName << '\n'

#define LOG_MATERIAL_PROPS(props) \
  LOG_PROP(props, opacity) \
  LOG_PROP(props, reflectivity) \
  LOG_PROP(props, shininess) \
  LOG_PROP(props, shininessStrength) \
  LOG_PROP(props, refractionIndex) \
  LOG_PROP(props, diffuseColor) \
  LOG_PROP(props, ambientColor) \
  LOG_PROP(props, specularColor) \
  LOG_PROP(props, emissiveColor) \
  LOG_PROP(props, transparentColor) \
  LOG_PROP(props, reflectiveColor);

  os << "MaterialProperties:\n" LOG_MATERIAL_PROPS(properties)

#undef LOG_PROP
#undef LOG_MATERIAL_PROPS

  return os;
}


Material::Material(std::string name, MaterialProperties properties, MaterialTextures textures)
  : _name{std::move(name)}
  , _properties{std::move(properties)}
  , _textures{std::move(textures)}
{}

Material::~Material() = default;

void Material::use(gl::ShaderUniformsSink& shaders) {
  SET_MATERIAL_PROP_VAR(reflectivity)
  SET_MATERIAL_PROP_VAR(shininess)
  SET_MATERIAL_PROP_VAR(diffuseColor)
  SET_MATERIAL_PROP_VAR(ambientColor)
  SET_MATERIAL_PROP_VAR(specularColor)
  SET_MATERIAL_PROP_VAR(emissiveColor)

  // TODO: See if occupying more texture units in advance is better than using a single unit for
  // different textures.

  if (shaders.varExists("hasAlbedoTexture")) {
    // FIXME: This gets "some" texture of the kind in case there are more than one.
    const auto texIt = _textures.find(TextureKind::Diffuse);
    const bool gotTexture = texIt != _textures.end();
    shaders.setUniformVar("hasAlbedoTexture", gotTexture ? 1 : 0);
    const unsigned albedoTextureUnit = 0;
    if (gotTexture) {
      texIt->second->use(albedoTextureUnit);  // TODO: Maybe move this out.
      shaders.setUniformVar("albedoTexture", ::gl::GLint{albedoTextureUnit});
    }
    else {
      gl::Texture::reset(albedoTextureUnit);
    }
  }

  if (shaders.varExists("hasSpecularTexture")) {
    // FIXME: This gets "some" texture of the kind in case there are more than one.
    const auto texIt = _textures.find(TextureKind::Specular);
    const bool gotTexture = texIt != _textures.end();
    shaders.setUniformVar("hasSpecularTexture", gotTexture ? 1 : 0);
    const unsigned specularTextureUnit = 1;
    if (gotTexture) {
      texIt->second->use(specularTextureUnit);  // TODO: Maybe move this out.
      shaders.setUniformVar("specularTexture", ::gl::GLint{specularTextureUnit});
    }
    else {
      gl::Texture::reset(specularTextureUnit);
    }
  }

  if (shaders.varExists("matAmbientMul")) {
    // TODO: These should probably read from some resource and passed in through the ctor.
    shaders.setUniformVar("matAmbientMul", glm::vec3{0.1, 0.1, 0.1});
  }
}

}  // namespace keg::gl
