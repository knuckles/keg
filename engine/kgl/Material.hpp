#pragma once

#include <map>
#include <memory>
#include <string>

#include <glm/vec3.hpp>


namespace keg::gl {
class Program;
class Texture;
class ShaderUniformsSink;

struct MaterialProperties {
  /// Packed layout allows us using this instead of float[3] - e.g. to read into it from ASSIMP.
  using Color = glm::tvec3<float, glm::qualifier::packed_lowp>;
  static_assert(sizeof(Color) == 3 * sizeof(float));

  float opacity = 1;  /// Defines the opacity of the material: 0..1.
  float reflectivity = 0;
  float shininess = 0;  /// The exponent of the phong specular equation.
  float shininessStrength = 1;  /// Scales the specular color of the material: 0..1
  float refractionIndex = 1;

  Color diffuseColor;
  Color ambientColor;
  Color specularColor;
  Color emissiveColor;
  /// The color to be multiplied with the color of translucent light to construct the final
  /// 'destination color' for a particular position in the screen buffer.
  Color transparentColor;
  Color reflectiveColor;
};

std::ostream& operator<<(std::ostream& os, const MaterialProperties::Color& color);
std::ostream& operator<<(std::ostream& os, const MaterialProperties& props);


enum class TextureKind {
  Diffuse = 1,
  Specular = 2,
  Ambient = 3,
  Emissive = 4,
  Height = 5,
  Normals = 6,
  Shininess = 7,
  Opacity = 8,
  Displacement = 9,
  Lightmap = 10,
  Reflection = 11,
  BaseColor = 12,
  NormalCamera = 13,
  EmissionColor = 14,
  Metalness = 15,
  DiffuseRoughness = 16,
  AmbientOcclusion = 17,
};

using MaterialTextures = std::multimap<TextureKind, std::shared_ptr<gl::Texture>>;


class Material {
public:
  explicit Material(std::string name, MaterialProperties properties, MaterialTextures textures);
  virtual ~Material();

  /// Use this material with the given shaders. This will bind textures to texture units and set
  /// material's properties to the given shader uniform variables.
  /// @param shaders - shader program to set material's properties to:
  /// `int hasAlbedoTexture` - set to 1 or 0 depending on whether this material provides an albedo
  ///   texture. Optional.
  /// `sampler2D albedoTexture` - set to albedo texture, if `hasAlbedoTexture` is present and is 1.
  /// `int hasSpecularTexture` - set to 1 or 0 depending on whether this material provides a
  ///   specular texture. Optional.
  /// `sampler2D specularTexture` - set to specular texture, if `hasSpecularTexture` is present and
  ///   is 1.
  void use(gl::ShaderUniformsSink& shaders);

private:
  std::string _name;
  MaterialProperties _properties;
  MaterialTextures _textures;
};

}  // namespace keg::gl
