#include "data_types.hpp"

#include <kgl/bindings.hpp>

namespace keg::gl {
using namespace ::gl;

namespace vertex_attributes {

const Id POSITIONS    = Id{VS_LOC_VPOS};
const Id COLORS       = Id{VS_LOC_VCOL};
const Id NORMALS      = Id{VS_LOC_VNORM};
const Id UVS          = Id{VS_LOC_VUV};
const Id BONE_INDICES = Id{VS_LOC_BONE_IDS};
const Id BONE_WEIGHTS = Id{VS_LOC_BONE_WEIGHTS};

// static
const std::array<Id, 6> ALL_VATTRIBUTE_IDS {
  POSITIONS,
  COLORS,
  NORMALS,
  UVS,
  BONE_INDICES,
  BONE_WEIGHTS,
};

};  // namespace vertex_attributes


template<>
GLenum glNumericTypeId<GLbyte>() { return GL_BYTE; }
template<>
GLenum glNumericTypeId<GLubyte>() { return GL_UNSIGNED_BYTE; }
template<>
GLenum glNumericTypeId<GLshort>() { return GL_SHORT; }
template<>
GLenum glNumericTypeId<GLushort>() { return GL_UNSIGNED_SHORT; }
template<>
GLenum glNumericTypeId<GLint>() { return GL_INT; }
template<>
GLenum glNumericTypeId<GLuint>() { return GL_UNSIGNED_INT; }
template<>
GLenum glNumericTypeId<GLfloat>() { return GL_FLOAT; }
template<>
GLenum glNumericTypeId<GLdouble>() { return GL_DOUBLE; }

}  // namespace keg::gl
