#pragma once

#include <glbinding/gl33core/types.h>

#include <kgl/GLObject.hpp>
#include <util/macros.hpp>


namespace keg::gl {

/// Render buffer is a storage which can be attached to a frame buffer for the purpose of capturing
/// a color-, depth-, or stencil-component of the rendering result.
class RenderBuffer : public GLObject
{
public:
  /// Create a new render buffer.
  /// @param width - buffer width.
  /// @param height - buffer height.
  /// @param internalFormat - specifies the internal format to be used for the renderbuffer object's
  /// storage and must be a color-renderable, depth-renderable, or stencil-renderable format
  RenderBuffer(
    ::gl::GLsizei width,
    ::gl::GLsizei height,
    ::gl::GLenum internalFormat
  );
};

}  // namespace keg::gl
