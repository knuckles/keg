/// This contains abstractions over GPU shader programs.
/// See https://www.khronos.org/opengl/wiki/Shader_Compilation

#pragma once

#include <limits>
#include <map>
#include <memory>
#include <string_view>
#include <vector>

#include <glbinding/gl33core/types.h>

#include <kgl/data_types.hpp>
#include <kgl/GLObject.hpp>
#include <kgl/ShaderUniformsSink.hpp>
#include <util/macros.hpp>


namespace keg::gl {

/// @brief This represents a compiled shader program text.
class Shader : public GLObject {
public:
  enum class Kind {
    Fragment,
    Vertex,
    Geometry,
  };
  static const size_t MAX_PROGRAM_LENGTH = std::numeric_limits<::gl::GLint>::max();

  NON_COPYABLE(Shader)
  DEFAULT_MOVABLE(Shader)

protected:
  Shader(Kind kind, std::string_view program);

private:
};


class FragmentShader : public Shader {
public:
  FragmentShader(std::string_view program);
};


class VertexShader : public Shader {
public:
  VertexShader(std::string_view program);
};


class GeometryShader : public Shader {
public:
  GeometryShader(std::string_view program);
};


class Program : public GLObject, public ShaderUniformsSink {
public:
  /// Specifies which variable corresponds to vertex attribute.
  using BindingsMap = std::map<vertex_attributes::Id, std::string_view>;
  using ShadersInitList = std::initializer_list<std::shared_ptr<const Shader>>;

  NON_COPYABLE(Program)
  NON_MOVABLE(Program)

  /**
   * @brief Program
   * @param attributeBindings
   * @param shaders - a list of shaders to link into the program.
   * NOTE: Input shaders can be thrown away once the program is linked.
   */
  Program(const ShadersInitList& shaders);

  bool varExists(std::string_view varName) const noexcept override;

  // ShaderUniformsSink API:
  void setUniformVar(std::string_view varName, ::gl::GLint val) override;
  void setUniformVar(std::string_view varName, bool val) override;
  void setUniformVar(std::string_view varName, ::gl::GLfloat val) override;
  void setUniformVar(std::string_view varName, const glm::vec2& val) override;
  void setUniformVar(std::string_view varName, const glm::vec3& val) override;
  void setUniformVar(std::string_view varName, const glm::vec4& val) override;
  void setUniformVar(std::string_view varName, const glm::mat4& val) override;
  void setUniformVar(std::string_view varName, const std::vector<glm::mat4>& val) override;

  void makeCurrent();

private:
  ::gl::GLint findVarIndex(std::string_view varName) const noexcept;
  ::gl::GLint getVarIndex(std::string_view varName) const;
};

}  // namespace keg::gl
