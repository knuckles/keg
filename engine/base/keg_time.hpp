#pragma once

#include <cstdint>
#include <chrono>
#include <optional>


namespace keg::core {

/// Game engine clock.
/// Implements the "Clock" named requirements.
class EngineClock {
public:
  EngineClock();

  // "Clock" requirements:

  using duration = std::chrono::microseconds;
  using rep = duration::rep;
  using period = duration::period;
  using time_point = std::chrono::time_point<EngineClock>;

  static constexpr bool is_steady = true;

  time_point now();

private:
  uint64_t _num = 1;
  uint64_t _den = 1;
};

using EngineTime = EngineClock::time_point;


/// Clock which determines the progression of the game logic.
/// Implements the "Clock" named requirements.
class LogicClock {
public:
  // "Clock" requirements:

  using duration = std::chrono::milliseconds;
  using rep = duration::rep;
  using period = duration::period;
  using time_point = std::chrono::time_point<LogicClock>;

  /// This clock provides virtual time, which may even completely stop at some point.
  static constexpr bool is_steady = false;

  time_point now();

  // Extra APIs:

  bool paused() const;
  void pause();
  void resume();
  void togglePaused();

  void adjust(duration d);
  void resetAdjustment();

private:
  duration _adjustment{0};
  std::optional<time_point> _pausedAt;
};

using LogicTime = LogicClock::time_point;


struct FrameTimes {
  LogicClock::time_point logicalTime;
  EngineClock::duration timeSincePrevFrame;
};


/// Swaps duration representation with another user-specified type.
/// This is useful in case one wants to format a duration with fractional part.
/// @param Rep - new duration representation type.
/// @param dur - duration value to convert.
template <typename Rep, typename SrcRep, typename SrcPeriod>
auto durationRep(const std::chrono::duration<SrcRep, SrcPeriod>& dur) {
  using namespace std::chrono;
  return duration_cast<duration<Rep, SrcPeriod>>(dur);
};


/// Converts the given duration to the requested duration unit expressed as a floating-point value.
template <typename AsDuration, typename SrcRep, typename SrcPeriod>
float toFloatDuration(const std::chrono::duration<SrcRep, SrcPeriod>& dur) {
  return durationRep<float>(dur)/AsDuration{1};
}


/// Converts the given duration to seconds expressed as a floating-point value.
template <typename SrcRep, typename SrcPeriod>
float toFloatSeconds(const std::chrono::duration<SrcRep, SrcPeriod>& dur) {
  return toFloatDuration<std::chrono::seconds>(dur);
}


/// Converts the given duration to milliseconds expressed as a floating-point value.
template <typename SrcRep, typename SrcPeriod>
float toFloatMs(const std::chrono::duration<SrcRep, SrcPeriod>& dur) {
  return toFloatDuration<std::chrono::milliseconds>(dur);
}

}  // namespace keg::core
