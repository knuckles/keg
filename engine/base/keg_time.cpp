#include "keg_time.hpp"

#include <SDL.h>


namespace keg::core {

EngineClock::EngineClock()
{
  const auto freq = SDL_GetPerformanceFrequency();
  if (freq > period::den) {
    _den = freq / period::den;
  }
  else {
    _num = period::den / freq;
  }
}

EngineClock::time_point EngineClock::now() {
  return time_point{duration{SDL_GetPerformanceCounter() * _num / _den}};
}

LogicClock::time_point LogicClock::now() {
  if (_pausedAt) {
    return *_pausedAt + _adjustment;
  }
  return time_point{duration{SDL_GetTicks64()}} + _adjustment;
}

bool LogicClock::paused() const {
  return _pausedAt.has_value();
}

void LogicClock::pause() {
  if (paused())
    return;

  _pausedAt = time_point{duration{SDL_GetTicks64()}};
}

void LogicClock::resume() {
  _pausedAt.reset();
}

void LogicClock::togglePaused() {
  if (paused()) {
    resume();
  }
  else {
    pause();
  }
}

void LogicClock::adjust(duration d) {
  _adjustment += d;
}

void LogicClock::resetAdjustment() {
  _adjustment = duration{};
}

}  // namespace keg::core
