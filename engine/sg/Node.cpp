#include <sg/Node.hpp>

#include <algorithm>
#include <deque>
#include <numeric>
#include <stdexcept>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
#undef GLM_ENABLE_EXPERIMENTAL

#include <kgl/Program.hpp>


namespace keg {
namespace sg {

// Node's PUBLIC:

Node::Node(std::string_view label)
  : _label{label},
    _pos{0, 0, 0},
    _rotation(glm::identity<glm::mat4>()),
    _scale(1) {
}

Node::~Node() {
  for (auto& child : _children) {
    child->_parent = nullptr;
  }
}

bool Node::isDirectParentOf(Node* node) const {
  auto foundIt = std::find_if(_children.begin(), _children.end(),
      [node](const auto& it) { return it.get() == node; });
  return foundIt != _children.end();
}

const std::string& Node::label() const { return _label; }

Node* Node::parent() const { return _parent; }

const Node* Node::root() const {
  const Node* node = this;
  while (true) {
    const auto* parent = node->parent();
    if (!parent)
      return node;
    node = parent;
  }
}

size_t Node::countAllChildren() const {
  // TODO: Keep this up to date after each child addiion/removal.
  return std::accumulate(
      _children.begin(), _children.end(),  // Input range
      _children.size(),  // Initial value
      [](const size_t acc, const auto& nodePtr) {
        return acc + nodePtr->countAllChildren();
      }
  );
}

glm::vec3 Node::worldPosition() const {
  return _parent ? _parent->worldPosition() + localPosition() : localPosition();
}

glm::quat Node::worldRotation() const {
  return _parent ? _parent->worldRotation() * localRotation() : localRotation();
}

glm::vec3 Node::worldScale() const {
  return _parent ? _parent->worldScale() * localScale() : localScale();
}

void Node::setLocalTransform(const glm::mat4& transform) {
  glm::vec3 skew;
  glm::vec4 perspective;
  glm::decompose(transform, _scale, _rotation, _pos, skew, perspective);
}

void Node::lookAt(const glm::vec3& target, const glm::vec3& up) {
  // FIXME: This doesn't seem to have any effect:

  const auto& viewMatrix = glm::lookAt(localPosition(), target, up);
  const auto camMatrix = glm::inverse(viewMatrix);
  glm::vec3 scale;
  glm::vec3 translation;
  glm::vec3 skew;
  glm::vec4 perspective;
  glm::decompose(camMatrix, scale, _rotation, translation, skew, perspective);
}

glm::mat4 Node::localTransform() const {
  const auto id = glm::identity<glm::mat4>();
  return glm::translate(id, _pos) * glm::mat4_cast(_rotation) * glm::scale(id, _scale);
}

glm::mat4 Node::worldTransform() const {
  if (!_parent)
    return localTransform();
  return _parent->worldTransform() * localTransform();
}

void Node::addChild(std::shared_ptr<Node> node) {
  _children.insert(node);
  node->_parent = this;

  onChildAdded(*node);
}

std::shared_ptr<Node> Node::removeChild(Node* child) {
  auto childIt = _children.find(child->shared_from_this());
  if (childIt == _children.end())
    throw std::invalid_argument("Given node is not my child.");

  return removeChild(childIt);
}

std::shared_ptr<Node> Node::removeChild(typename ChildrenContainer::iterator childIt) {
  std::shared_ptr<Node> freeNode = std::move(*childIt);
  _children.erase(childIt);
  freeNode->_parent = nullptr;

  onChildRemoved(*freeNode);

  return freeNode;
}

void Node::setControler(std::unique_ptr<NodeController> controller) {
  _controller = std::move(controller);
  if (_controller)
    _controller->onAttached(this);
}

void Node::update(const core::FrameTimes& time) {
  if (_controller)
    _controller->update(this, time);
  doUpdate(time);
}

bool Node::renderable() const { return false; }

void Node::render(const glm::mat4& viewProjTransform, gl::ShaderUniformsSink& shaders) const {
  // TODO: I'm not sure I like these here. Maybe these should be set by the Renderer.
  shaders.setUniformVar("modelTransform", worldTransform());
  shaders.setUniformVar("mvpTransform", viewProjTransform * worldTransform());

  doRender(shaders);
}

// Node's PROTECTED:

void Node::traverseBreadthFirst(Node::VisitorFunc visit) {
  _visitQueue.clear();
  _visitQueue.reserve(countAllChildren());
  _visitQueue.push_back(this);

  for (size_t i = 0; i < _visitQueue.size(); ++i) {
    Node* node = _visitQueue[i];
    assert(node);

    if (!visit(node))
      break;

    std::transform(
        node->children().begin(),
        node->children().end(),
        std::back_inserter(_visitQueue),
        [](auto& childSmartPtr) { return childSmartPtr.get(); }
    );
  }
}

void Node::traverseBreadthFirst(ConstVisitorFunc visit) const {
  const_cast<Node*>(this)->traverseBreadthFirst([&visit] (Node* node) {
    return visit(node);
  });
}

void Node::traverseUp(VisitorFunc visit) {
  Node* node = this;
  while (node) {
    if (!visit(node))
      break;
    node = node->parent();
  }
}

void Node::traverseUp(ConstVisitorFunc visit) const {
  const_cast<Node*>(this)->traverseUp([&visit] (Node* node) {
    return visit(node);
  });
}

std::shared_ptr<Node> Node::findByName(const std::string_view name) {
  std::shared_ptr<Node> result;

  traverseBreadthFirst([&] (Node* node) {
    if (node->label() == name) {
      result = node->shared_from_this();
      return false;
    }
    return true;
  });

  return result;
}

std::shared_ptr<const Node> Node::findByName(const std::string_view name) const {
  return const_cast<Node*>(this)->findByName(name);
}

void Node::onChildAdded(Node& /*node*/) noexcept {
}

void Node::onChildRemoved(Node& /*node*/) noexcept {
}

void Node::doUpdate(const core::FrameTimes& /*time*/) noexcept {
}

void Node::doRender(gl::ShaderUniformsSink& /*uniforms*/) const noexcept {
}

// Node's END

NodeController::~NodeController() = default;

}  // namespace sg
}  // namespace keg
