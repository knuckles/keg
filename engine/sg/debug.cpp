#include <sg/debug.hpp>

#include <iostream>

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <kgl/Mesh.hpp>
#include <sg/MeshNode.hpp>


namespace keg::sg {

namespace {
void printNode(const keg::sg::Node& n, int level) {
  for (int i = level; i --> 0; ) {
    std::cerr << " ";
  }
  fmt::print(std::cerr, "* [{}] `{}`", n.renderable() ? 'R' : ' ', n.label());
  if (auto* const meshNode = dynamic_cast<const sg::MeshNode*>(&n); meshNode) {
    fmt::print(std::cerr, " ({} verts)", meshNode->mesh().countVertices());
  }
  std::cerr << '\n';
}
}  // namespace

void printSG(const keg::sg::Node* from, int level) {
  printNode(*from, level);
  for (const auto& n : from->children()) {
    printSG(n.get(), level + 1);
  }
  std::cerr << std::flush;
}

}  // namespace keg::sg
