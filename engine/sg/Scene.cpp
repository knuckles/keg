#include <sg/Scene.hpp>

// #include <base/keg_time.hpp>
#include <sg/CameraNode.hpp>


namespace keg::sg {

// Scene's PUBLIC:

Scene::Scene(std::string_view label)
  : _root{std::make_shared<Node>(label)}
  , _camera{std::make_shared<CameraNode>()}
{
  _root->addChild(_camera);
}

void Scene::update(const core::FrameTimes& time) {
  _root->traverseBreadthFirst([&time] (Node* node) {
    node->update(time);
    return true;
  });
}

void Scene::render(const glm::mat4& projection, gl::ShaderUniformsSink& uniforms) const {
  const auto vpTransform = projection * _camera->viewTransform();

  // This is a very simplistic and inefficient way of rendering the scene.
  //
  // TODO: Rendered objects should be culled and ordered by distance and material.
  // See https://realtimecollisiondetection.net/blog/?p=86
  //
  // Meshes sharing a material (shader program and uniforms) can be drawn with a single call to
  // glMultiDrawArrays/glMultiDrawElements which is more efficient.
  // See: https://www.khronos.org/opengl/wiki/Vertex_Rendering#Multi-Draw
  //
  // TODO: Consider usind instanced drawing as well.
  // See: https://www.khronos.org/opengl/wiki/Vertex_Rendering#Instancing

  _root->traverseBreadthFirst([&] (const Node* node) {
    if (node->renderable())
      node->render(vpTransform, uniforms);
    return true;
  });
}

}  // namespace keg::sg
