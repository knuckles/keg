#include "Animation.hpp"

#include <cmath>
#include <stdexcept>
#include <type_traits>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/ext/quaternion_common.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <base/keg_time.hpp>


namespace keg::sg {

namespace {
template <typename T>
using FC = Animation::Channel::FramesContainer<T>;

template <typename T>
bool timeLessThanFrameTime (
  const Animation::Channel::Dur time,
  typename FC<T>::const_reference frame
) {
  return time < frame.first;
}

template <typename T>
static bool compareFrameTimes(
  typename FC<T>::const_reference frame1,
  typename FC<T>::const_reference frame2
) {
  return frame1.first < frame2.first;
}

template<typename T, glm::qualifier Q>
glm::qua<T, Q> interpolate(const glm::qua<T, Q>& q1, const glm::qua<T, Q>& q2, const T factor) {
  // Interpolation between quaternions must be done using "spherical linear interpolation".
  return glm::slerp(q1, q2, factor);
}

template<glm::length_t L, typename T, typename U, glm::qualifier Q>
glm::vec<L, T, Q> interpolate(glm::vec<L, T, Q> const& v1, glm::vec<L, T, Q> const& v2, U factor) {
  return glm::mix(v1, v2, factor);
}

/// @tparam T - GLM's floating point vector or quaternion
/// @param frames - mapping from time to animation's property value (see `T`). Must be sorted!
template <typename T>
T calcFrame(
    const FC<T>& frames,
    const Animation::Channel::Dur time
) {
  if (!frames.size())
    throw std::logic_error("No frames.");

  if (frames.size() == 1)
    return frames.begin()->second;

  const auto animLen = frames.rbegin()->first;
  const auto animTimePoint = time % animLen;

  const auto nextFrameIt = std::upper_bound(
    frames.begin(), frames.end(),
    animTimePoint,
    timeLessThanFrameTime<T>
  );
  const auto prevFrameIt = std::prev(
      nextFrameIt != frames.begin() ? nextFrameIt : frames.end()
  );
  assert(prevFrameIt != frames.end());
  const auto& [nextTime, nextKey] = *nextFrameIt;

  // Reset previous frame time to first frame's time when wrapping the animation around.
  // This avoids miscalculation of `factor` which otherwise could exceed 1.
  const auto prevTime = nextFrameIt == frames.begin() ? nextFrameIt->first : prevFrameIt->first;
  const auto& prevKey = prevFrameIt->second;

  const auto factor =
    typename T::value_type((animTimePoint - prevTime).count()) / (nextTime - prevTime).count();
  assert(factor >= 0.0 && factor <= 1.0);

  return interpolate(prevKey, nextKey, factor);
}

}  // namespace


glm::vec3 Animation::Channel::interpolatePosition(const Dur time) const {
  return calcFrame(positionFrames, time);
}

glm::quat Animation::Channel::interpolateRotation(const Dur time) const {
  return calcFrame(rotationFrames, time);
}

glm::vec3 Animation::Channel::interpolateScaling(const Dur time) const {
  return calcFrame(scalingFrames, time);
}

Animation::Animation(std::string_view name, std::vector<Channel> channels)
  : _name(name) {
  for (auto& channel : channels) {
#if defined(KEG_DEBUG)
    for (const auto& posFrame : channel.positionFrames) {
      assert(!std::isnan(posFrame.second.x));
      assert(!std::isnan(posFrame.second.y));
      assert(!std::isnan(posFrame.second.z));
    }
#endif
    std::sort(
      channel.positionFrames.begin(), channel.positionFrames.end(),
      compareFrameTimes<glm::vec3>
    );

    std::sort(
      channel.rotationFrames.begin(), channel.rotationFrames.end(),
      compareFrameTimes<glm::quat>
    );

    std::sort(
      channel.scalingFrames.begin(), channel.scalingFrames.end(),
      compareFrameTimes<glm::vec3>
    );

    const auto& [it, channelNameIsUnique] = _channels.insert({channel.name, std::move(channel)});
    assert(channelNameIsUnique);
  }
}

Animation::~Animation() = default;

}  // namespace keg::sg
