#include <memory>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_all.hpp>

#include <sg/debug.hpp>
#include <sg/Node.hpp>
#include <sg/utils.hpp>

using namespace keg::sg;

TEST_CASE("Common parent search", "[utils]") {
  auto root = std::make_shared<Node>("root");

  auto a1 = std::make_shared<Node>("a1");
  root->addChild(a1);

  auto b1 = std::make_shared<Node>("b1");
  a1->addChild(b1);

  auto b2 = std::make_shared<Node>("b2");
  a1->addChild(b2);

  auto c1 = std::make_shared<Node>("c1");
  b2->addChild(c1);

  /** Resulting tree:

     root
      |
      a1
     /  \
    b1  b2
         \
         c1

  */

  auto* cp = findNearestCommonParent(b1.get(), c1.get());
  REQUIRE(cp == a1.get());
}

TEST_CASE("Printing Scene Graph", "[debug]") {
  auto scene = std::make_shared<Node>("root");
  auto n1 = std::make_shared<Node>("n1");
  n1->addChild(std::make_shared<Node>("n2"));
  scene->addChild(n1);
  printSG(scene.get());
}
