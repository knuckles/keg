#pragma once

#include <chrono>
#include <map>
#include <string>
#include <string_view>
#include <vector>
#include <utility>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/vec3.hpp>
#include <glm/ext/quaternion_float.hpp>
#undef GLM_FORCE_SILENT_WARNINGS


namespace keg::sg {

class Animation {
public:
  struct Channel {
    using Dur = std::chrono::milliseconds;

    template <typename T>
    using FramesContainer = std::vector<std::pair<Dur, T>>;

    using PositionFrames = FramesContainer<glm::vec3>;
    using RotationFrames = FramesContainer<glm::quat>;
    using ScalingFrames = FramesContainer<glm::vec3>;

    // TODO: Support values of `aiAnimBehaviour` other than just interpolation.
    /// @see `aiAnimBehaviour`
    glm::vec3 interpolatePosition(const Dur time) const;
    glm::quat interpolateRotation(const Dur time) const;
    glm::vec3 interpolateScaling(const Dur time) const;

    std::string name;

    /// Mappings from animation times to transformation components for each frame.
    PositionFrames positionFrames;
    RotationFrames rotationFrames;
    ScalingFrames scalingFrames;
  };

  explicit Animation(std::string_view name, std::vector<Channel> channels);
  ~Animation();

  const std::string& name() const { return _name; }
  const std::map<std::string, Channel>& channels() const { return _channels; }

private:
  std::string _name;
  std::map<std::string, Channel> _channels;
};

}  // namespace keg::sg
