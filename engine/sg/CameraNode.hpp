#pragma once

#include <string_view>

#include <sg/Node.hpp>


namespace keg::sg {

class CameraNode : public Node {
public:
  /// @brief Construct the camera.
  explicit CameraNode(std::string_view label = "");

  /// @return view transform matrix as defined by the camera's position and orientation.
  glm::mat4 viewTransform() const;
};

}  // namespace keg::sg
