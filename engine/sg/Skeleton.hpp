#pragma once

#include <map>
#include <memory>
#include <optional>
#include <set>
#include <string>
#include <vector>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/mat4x4.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <base/keg_time.hpp>


namespace keg::sg {
class Animation;
class Node;

class Skeleton {
public:
  explicit Skeleton(
      std::shared_ptr<Node> structure,
      std::map<std::string, std::pair<unsigned int, glm::mat4x4>> boneIndices
  );

  const std::vector<glm::mat4>& boneTransforms() const { return _boneTransforms; }

  void addAnimation(std::shared_ptr<Animation> anim);
  void removeAnimation(std::shared_ptr<Animation> anim);

  void update(const core::LogicTime& time);

private:
  bool updateBoneNode(const core::LogicTime& time, Node* bone);

  std::shared_ptr<Node> _structure;
  std::map<std::string, std::pair<unsigned int, glm::mat4x4>> _boneIndices;
  std::vector<glm::mat4> _boneTransforms;

  // We want to be able to apply multiple animations simultaneously
  std::set<std::shared_ptr<Animation>> _animations;
};

}  // namespace keg::sg
