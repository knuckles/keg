#pragma once

#include <sg/Node.hpp>


namespace keg::gl {
class Material;
class Mesh;
}  // namespace keg::gl

namespace keg::sg {

class Model;

class MeshNode : public Node {
public:
  explicit MeshNode(
      std::shared_ptr<gl::Mesh> mesh,
      std::shared_ptr<gl::Material> material = nullptr,
      std::string label = ""
  );

  gl::Mesh& mesh() { return *_mesh; }
  const gl::Mesh& mesh() const { return *_mesh; }
  const gl::Material* material() const { return _material.get(); }

  /// @see `Node` interface:
  bool renderable() const override;

protected:
  /// @param shaders - shader program used for rendering. The following uniform vars are expected:
  /// `int hasBones` (optional)
  /// `mat4 boneTransforms[@MAX_BONES_PER_MESH@]` (optional)
  /// `int hasAlbedoTexture`
  /// `sampler2D albedoTexture`
  /// `int hasSpecularTexture`
  /// `sampler2D specularTexture`
  void doRender(gl::ShaderUniformsSink& shaders) const noexcept override;

private:
  friend class Model;
  Model* _parentModel = nullptr;

  std::shared_ptr<gl::Mesh> _mesh;
  std::shared_ptr<gl::Material> _material;
};

}  // namespace keg::sg
