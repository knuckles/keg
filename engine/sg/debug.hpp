#pragma once

#include <sg/Node.hpp>


namespace keg::sg {

void printSG(const Node* scene, int level = 0);

}  // namespace keg::sg
