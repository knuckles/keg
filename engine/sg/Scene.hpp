#pragma once

#include <cstdint>
#include <memory>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/mat4x4.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <sg/Node.hpp>


namespace keg::core {
struct FrameTimes;
}

namespace keg::gl {
class ShaderUniformsSink;
}

namespace keg::sg {

class CameraNode;

class Scene {
public:
  /// Construct a new scene.
  explicit Scene(std::string_view label);

  std::shared_ptr<Node> root() { return _root; }
  std::shared_ptr<CameraNode> camera() const { return _camera; }

  void update(const core::FrameTimes& time);

  /// @param projection transformation to calculate node's MVP matrix.
  /// @param[out] uniforms sink for the shader uniform parameters.
  /// Renderable nodes are expected to set their relevant variables.
  void render(const glm::mat4& projection, gl::ShaderUniformsSink& uniforms) const;

private:
  std::shared_ptr<Node> _root;
  std::shared_ptr<CameraNode> _camera;
};

}  // namespace keg::sg
