#include "Skeleton.hpp"

#include <iostream>
#include <stdexcept>

#include <absl/functional/bind_front.h>
#include <g3log/g3log.hpp>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <kgl/Mesh.hpp>
#include <sg/Animation.hpp>
#include <sg/MeshNode.hpp>
#include <sg/utils.hpp>
#include <util/containers.hpp>
#include <util/logging.hpp>


namespace keg::sg {

namespace {

const glm::mat4 NO_TRANSFORM(1.0f);

[[maybe_unused]]
std::ostream& operator<<(std::ostream& os, const glm::vec3& vec) {
  os << "vec3{"<< vec.x << ", " << vec.y << ", " << vec.z << "}";
  return os;
}

[[maybe_unused]]
std::ostream& operator<<(std::ostream& os, const glm::quat& q) {
  const auto& eulerAngles = glm::eulerAngles(q);
  os << eulerAngles;
  return os;
}

} // namespace

Skeleton::Skeleton(
    std::shared_ptr<Node> structure,
    std::map<std::string, std::pair<unsigned int, glm::mat4x4>> boneIndices
) : _structure(structure),
    _boneIndices(std::move(boneIndices)) {
  if (!_structure)
    throw std::invalid_argument("Bone structure missing.");
  if (_boneIndices.size() > gl::Mesh::MAX_BONES)
    throw std::invalid_argument(
        "Max bone count exceeded: " + std::to_string(_boneIndices.size())
    );
  _boneTransforms.resize(_boneIndices.size());
  std::fill(_boneTransforms.begin(), _boneTransforms.end(), NO_TRANSFORM);
  LOG(DEBUG) << "Skeleton created with " << _boneIndices.size() << " bones "
             << "under root node '" << _structure->label() << "'.\n";
}

void Skeleton::addAnimation(std::shared_ptr<Animation> anim) {
  _animations.insert(anim);
#if defined(KEG_DEBUG)
  for (const auto& c : anim->channels()) {
    const auto node = _structure->findByName(c.second.name);
    if (!node) {
      LOG(TRACE) << "No node to animate channel '" << c.second.name << "'.\n";
    }
  }
#endif  // defined(KEG_DEBUG)
}

void Skeleton::removeAnimation(std::shared_ptr<Animation> anim) {
  _animations.erase(anim);
}

void Skeleton::update(const core::LogicTime& time) {
  _structure->traverseBreadthFirst( absl::bind_front(&Skeleton::updateBoneNode, this, time) );
}

bool Skeleton::updateBoneNode(const core::LogicTime& time, Node* bone) {
  for (const auto& anim : _animations) {
    const auto channelIt = anim->channels().find(bone->label());
    if (channelIt == anim->channels().cend()) {
      continue;  // No animation for this bone.
    }

    const auto& channel = channelIt->second;

    const auto timeDuration = time.time_since_epoch();
    bone->setPosition(channel.interpolatePosition(timeDuration));
    bone->setRotation(channel.interpolateRotation(timeDuration));
    bone->setScale(channel.interpolateScaling(timeDuration));

    const auto indexIt = _boneIndices.find(bone->label());
    if (indexIt == _boneIndices.cend()) {
      // Although it may seem erroneous to not have a bone index, this can be a case for
      // bones whose purpose is to control model's external elements - e.g. character's
      // weapons. This bone simply does not deform mesh vertices, but may still be useful for other
      // purposes.
      continue;
    }

    const auto index = indexIt->second.first;
    const auto& offset = indexIt->second.second;
    assert(index < _boneTransforms.size());
    _boneTransforms[index] =
        glm::inverse(_structure->worldTransform()) *
        bone->worldTransform() *
        offset;
  }  // for _animations

  return true;  // Continue scene graph traversal.
}

}  // namespace keg::sg
