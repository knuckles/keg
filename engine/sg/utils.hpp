#pragma once

#include <chrono>
#include <memory>

namespace keg::sg {

class Node;

const Node* findNearestCommonParent(const Node* n1, const Node* n2);

}  // namespace keg::sg
