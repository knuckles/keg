#pragma once

#include <functional>
#include <set>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#define GLM_FORCE_SILENT_WARNINGS
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/ext/quaternion_float.hpp>
#undef GLM_FORCE_SILENT_WARNINGS

#include <base/keg_time.hpp>


namespace keg::gl {
class ShaderUniformsSink;
}

namespace keg::sg {

class Node;


/// Node controller interface.
/// Instance of such can be assigned to nodes in order to hook into rendering loop and
/// perform arbitrary actions on their corresponding nodes.
class NodeController {
public:
  virtual void onAttached(const Node* node) = 0;
  virtual void update(Node* node, const core::FrameTimes& time) = 0;
  virtual ~NodeController();
};


/// Normal scene graph node.
/// TODO: This is getting fat. There's an idea that SG nodes should not reference
/// renderables, but rather the opposite should be the case. See this discussion:
/// https://gamedev.stackexchange.com/questions/46452/rolling-my-own-scene-graph
class Node : public std::enable_shared_from_this<Node> {
public:
  explicit Node(std::string_view label = "");
  virtual ~Node();

  bool isDirectParentOf(Node* node) const;

  const std::string& label() const;

  Node* parent() const;
  const Node* root() const;

  const std::set<std::shared_ptr<Node>>& children() const { return _children; }

  size_t countAllChildren() const;

  /// Calculated world space pose and scale properties.
  glm::vec3 worldPosition() const;
  glm::quat worldRotation() const;
  glm::vec3 worldScale() const;
  glm::mat4 worldTransform() const;

  /// Node-space pose and scale properties.
  const glm::vec3& localPosition() const { return _pos; }
  const glm::quat& localRotation() const { return _rotation; }
  const glm::vec3& localScale() const { return _scale; }
  glm::mat4 localTransform() const;

  /// Node-space pose and scale setters.
  void setPosition(const glm::vec3& newPos) { _pos = newPos; }
  void setRotation(const glm::quat& newRot) { _rotation = newRot; }
  void setRotation(const glm::vec3& newRot) { _rotation = glm::quat{newRot}; }
  void setScale(const glm::vec3& newScale) { _scale = newScale; }
  void setLocalTransform(const glm::mat4& transform);

  // TODO: Move to CameraNode. This will require to rework how camera node controller is attached.
  /// Sets the rotation so that camera is facing the |target|.
  void lookAt(const glm::vec3& target, const glm::vec3& up);

  void addChild(std::shared_ptr<Node> node);
  std::shared_ptr<Node> removeChild(Node* node);
  void setControler(std::unique_ptr<NodeController> controller);

  /// Per-frame call telling the Node to update its state.
  void update(const core::FrameTimes& time);  // TODO: Make virtual for Scene to override?

  /// Whether this node itself has visually renderable content.
  /// This base implementation always returns false.
  virtual bool renderable() const;

  /// Outputs its content, if any, onto the graphical context.
  /// @param viewProjTransform - combined view-projection matrix. Defines the "camera" position
  /// together with the way objects get projected onto the 2D screen plane (e.g. the perspective
  /// effect).
  /// @param shaders - shader program to use for rendering. The following uniforms are expected:
  /// `mat4 modelTransform` - model transform matrix,
  /// `mat4 mvpTransform` - complete model-view-projection matrix.
  /// @note subclasses may require more uniforms to be present!
  void render(const glm::mat4& viewProjTransform, gl::ShaderUniformsSink& shaders) const;

  /// Function prototypes for node tree traversal. Return `false` to interrupt.
  using VisitorFunc = std::function<bool (Node*)>;
  using ConstVisitorFunc = std::function<bool (const Node*)>;

  void traverseBreadthFirst(VisitorFunc visit);
  void traverseBreadthFirst(ConstVisitorFunc visit) const;

  void traverseUp(VisitorFunc visit);
  void traverseUp(ConstVisitorFunc visit) const;

  std::shared_ptr<Node> findByName(const std::string_view name);
  std::shared_ptr<const Node> findByName(const std::string_view name) const;

protected:
  virtual void onChildAdded(Node& node) noexcept;
  virtual void onChildRemoved(Node& node) noexcept;

  virtual void doUpdate(const core::FrameTimes& time) noexcept;

  /// Make the node-specific GL call(s) necessary to render it.
  /// @param shaders - shader program to use for rendering.
  virtual void doRender(gl::ShaderUniformsSink& shaders) const noexcept;

  glm::vec3 _pos;
  glm::quat _rotation;
  glm::vec3 _scale;

private:
  using ChildrenContainer = std::set<std::shared_ptr<Node>>;

  std::shared_ptr<Node> removeChild(typename ChildrenContainer::iterator childIt);

  std::string _label;
  // TODO: We used to have weak_ptr here, maybe we should put it back.
  Node* _parent = nullptr;
  ChildrenContainer _children;
  std::vector<Node*> _visitQueue;
  std::unique_ptr<NodeController> _controller;
};

}  // namespace keg::sg
