#pragma once

#include <memory>

#include <sg/Node.hpp>


namespace keg::gl {
class Material;
class Mesh;
}  // namespace keg::gl

namespace keg::sg {

class Skeleton;

/// A Model is a "live" part of the 3D scene. It may consist of one or more, possibly
/// animated, meshes. If there is an animation skeleton, it is used by all meshes of this model.
/// Because of the fact that model is itself a hierarchy, it is structured using scene nodes.
class Model : public Node {
public:
  explicit Model(std::unique_ptr<Skeleton> skeleton, std::string label = "");
  virtual ~Model() override;

  const Skeleton* skeleton() const { return _skeleton.get(); }
  Skeleton* skeleton() { return _skeleton.get(); }


protected:
  // Node:
  void onChildAdded(Node& node) noexcept override;
  void onChildRemoved(Node& node) noexcept override;
  void doUpdate(const core::FrameTimes& time) noexcept override;

private:
  std::unique_ptr<Skeleton> _skeleton;
};

}  // namespace keg::sg
