#include <sg/utils.hpp>

#include <stdexcept>
#include <vector>

#include <sg/Node.hpp>

namespace keg::sg {

namespace {
std::vector<const Node*> pathFromRoot(const Node* n) {
  std::vector<const Node*> path;
  while (n) {
    path.push_back(n);
    n = n->parent();
  }
  std::reverse(path.begin(), path.end());
  return path;
}

}  // namespace

const Node* findNearestCommonParent(const Node* n1, const Node* n2) {
  const Node* commonParent = nullptr;

  const auto& path1 = pathFromRoot(n1);
  const auto& path2 = pathFromRoot(n2);
  for (
       auto i1 = path1.begin(), i2 = path2.begin();
       i1 != path1.end() && i2 != path2.end() && *i1 == *i2;
       ++i1, ++i2
  ) {
    commonParent = *i1;
  }
  return commonParent;
}

}  // namespace keg::sg
