#include "CameraNode.hpp"


namespace keg::sg {

CameraNode::CameraNode(std::string_view label)
  : Node(std::string{label})
{
}

glm::mat4 CameraNode::viewTransform() const {
  return glm::inverse(this->worldTransform());
}

}  // namespace keg::sg
