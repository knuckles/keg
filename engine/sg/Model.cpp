#include <sg/Model.hpp>

#include <stdexcept>

#include <sg/MeshNode.hpp>
#include <sg/Skeleton.hpp>

namespace keg::sg {

namespace {

void traverseMeshNodes(Node* from, std::function<void (MeshNode*)> visit) {
  from->traverseBreadthFirst([&](Node* n) {
    if (auto* meshNode = dynamic_cast<MeshNode*>(n); meshNode)
      visit(meshNode);
    return true;
  });
}

}  // namespace

Model::Model(std::unique_ptr<Skeleton> skeleton, std::string label)
  : Node(std::move(label)),
    _skeleton(std::move(skeleton)) {
  if (!_skeleton)
    throw std::invalid_argument("Skeleton missing.");
}

Model::~Model() = default;

void Model::onChildAdded(Node& node) noexcept {
  // TODO: This is stupid.
  traverseMeshNodes(&node, [&](MeshNode* meshNode) {
    if (!meshNode->_parentModel)
      meshNode->_parentModel = this;
  });
}

void Model::onChildRemoved(Node& node) noexcept {
  // TODO: This is stupid.
  traverseMeshNodes(&node, [&](MeshNode* meshNode) {
    if (meshNode->_parentModel == this)
      meshNode->_parentModel = nullptr;
  });
}

void Model::doUpdate(const core::FrameTimes& frameTimes) noexcept {
  _skeleton->update(frameTimes.logicalTime);
}

}  // namespace keg::sg
