#include <sg/MeshNode.hpp>

#include <stdexcept>

#include <kgl/Material.hpp>
#include <kgl/Mesh.hpp>
#include <kgl/Program.hpp>
#include <sg/Model.hpp>
#include <sg/Skeleton.hpp>


namespace keg::sg {

namespace {
const std::vector<glm::mat4> NO_BONE_TRANSFORMS;
}

MeshNode::MeshNode(
    std::shared_ptr<gl::Mesh> mesh,
    std::shared_ptr<gl::Material> material,
    std::string label
) : Node(std::move(label)),
    _mesh(mesh),
    _material(material) {
  if (!_mesh)
    throw std::invalid_argument("No mesh specified.");
}

bool MeshNode::renderable() const { return true; }

void MeshNode::doRender(gl::ShaderUniformsSink& shaders) const noexcept {
  if (shaders.varExists("hasSkeleton")) {
    // Since a single skeleton may control multiple meshes it's non-trivial to set bone transforms
    // from a single place (like a Model) because technically meshes may use different shaders.
    const bool hasSkeleton =
        _mesh->providesAttribute(gl::vertex_attributes::BONE_WEIGHTS)
        && _parentModel
        && _parentModel->skeleton();
    shaders.setUniformVar("hasSkeleton", hasSkeleton);
    shaders.setUniformVar("boneTransforms",
      hasSkeleton ? _parentModel->skeleton()->boneTransforms() : NO_BONE_TRANSFORMS
    );
  }

  if (_material) {
    _material->use(shaders);
  }

  _mesh->draw();
}

}  // namespace keg::sg
