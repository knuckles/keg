#include <Keg.hpp>

#include <memory>
#include <optional>
#include <mutex>

#include <fmt/core.h>
#include <g3log/g3log.hpp>
#include <g3log/logworker.hpp>
#include <SDL.h>
#include <SDL2pp/SDL.hh>
#include <SDL2pp/Window.hh>

#include <base/keg_time.hpp>
#include <extras/FreeFlyCameraController.hpp>
#include <sg/Scene.hpp>
#include <util/sdl_helpers.hpp>
#include <input/InputController.hpp>
#include <render/Renderer.hpp>


namespace {

std::lock_guard<std::mutex> lockCtorMutex() {
  static std::mutex lifetimeMutex;
  return std::lock_guard{lifetimeMutex};
}

}  // namespace

namespace keg {
Keg* Keg::_instance = nullptr;

// TODO: This class pretty much represents the OpenGL context and it might be best to formalize this
// as such and also make it possible to manage multiple GL contexts that way: i.e. expose functions
// specific to the context state from it.
struct Keg::Guts {
  Guts(int winWidth, int winHeight, std::string_view logsPath)
    : logWorker{g3::LogWorker::createLogWorker()}
    , sdl{SDL_INIT_VIDEO}  // Init SDL
  {
    // Init logging.
    logWorker->addDefaultLogger("keg", std::string{logsPath});
    g3::initializeLogging(logWorker.get());

    LOG(INFO) << "Current video driver: '" << SDL_GetCurrentVideoDriver() << "'";

    // Set SDL's GL parameters:
    for (const auto& attr : std::map<SDL_GLattr, int> {
        {SDL_GL_CONTEXT_MAJOR_VERSION, 3},
        {SDL_GL_CONTEXT_MINOR_VERSION, 3},
        {SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE},
        {SDL_GL_DOUBLEBUFFER, 1},
        {SDL_GL_ACCELERATED_VISUAL, 1},
        {SDL_GL_MULTISAMPLEBUFFERS, 2},
        {SDL_GL_MULTISAMPLESAMPLES, 4},
        {SDL_GL_RED_SIZE, 8},
        {SDL_GL_GREEN_SIZE, 8},
        {SDL_GL_BLUE_SIZE, 8},
        {SDL_GL_ALPHA_SIZE, 8},
        //{SDL_GL_DEPTH_SIZE, 16},  // TODO: 32 bits works on a Macbook but not on a Linux laptop.
    }) {
      util::SDLException::check(SDL_GL_SetAttribute(attr.first, attr.second));
    }

    // TODO: Pass this and other settings (e.g. multisampling) from the outside.
    SDL_SetRelativeMouseMode(SDL_TRUE);

    window.emplace(
      "KEG",
      SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
      winWidth, winHeight,
      SDL_WINDOW_ALLOW_HIGHDPI
        | SDL_WINDOW_OPENGL
        | SDL_WINDOW_SHOWN
    );

    // Create OpenGL context:
    glContext = SDL_GL_CreateContext(window->Get());
    {
      int majVer = 0;
      int minVer = 0;
      util::SDLException::check(SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majVer));
      util::SDLException::check(SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &minVer));
      LOG(INFO) << "GL context created for API version: " << majVer << "." << minVer;
    }

    // Synchronize display updates with the vertical retrace.
    SDL_GL_SetSwapInterval(1);

    renderer.emplace();
  }

  ~Guts() {
    if (glContext)
      SDL_GL_DeleteContext(glContext);
  }

  std::unique_ptr<g3::LogWorker> logWorker;
  SDL2pp::SDL sdl;
  std::optional<SDL2pp::Window> window;
  SDL_GLContext glContext;

  core::LogicClock logicClock;
  core::EngineClock engineClock;
  input::InputController inputController;
  std::optional<render::Renderer> renderer;
};  // Guts


// static
Keg& Keg::instance() {
  if (!_instance) {
    throw std::runtime_error("No instance of Keg engine exists.");
  }
  return *_instance;
}

Keg::Keg(int winWidth, int winHeight, std::string_view logsPath) {
  // This lock prevents construction of multiple instances from different threads.
  auto ctorLock = lockCtorMutex();
  if (_instance) {
    throw std::runtime_error("An instance of Keg engine already exists.");
  }

  _guts = std::make_unique<Guts>(winWidth, winHeight, logsPath);

  _instance = this;
}

Keg::~Keg() {
  _guts.reset();  // I want this gone befone the `_instance` pointer is nulled.
  _instance = nullptr;
}

core::LogicClock& Keg::logicClock() {
  return _guts->logicClock;
}

core::CallbackId Keg::addInputCallback(SDL_Keycode key, input::InputCallback callback) {
  return _guts->inputController.addInputCallback(key, callback);
}

bool Keg::removeInputCallback(SDL_Keycode key, core::CallbackId callbackId) {
  return _guts->inputController.removeInputCallback(key, callbackId);
}

std::unique_ptr<sg::NodeController> Keg::createFreeFlyCameraController() const {
  return std::make_unique<sg::FreeFlyCameraController>(_guts->inputController.inputBuffer());
}

render::Renderer& Keg::renderer() {
  return _guts->renderer.value();
}

void Keg::addRenderPass(RenderPass pass) {
  _renderPasses.emplace_back(std::move(pass));
}

void Keg::clearRenderPasses() {
  _renderPasses.clear();
}

core::CallbackId Keg::addPerfCallback(PerfCallback callback) {
  _perfCallbacks.emplace_back(std::move(callback));
  return 0;  // FIXME: Implement
}

bool Keg::removePerfCallback(core::CallbackId callbackId) {
  // FIXME: Implement
  return false;
}

void Keg::enterMainLoop(bool &canContinue) {
  unsigned int framesDrawn = 0;
  unsigned int framesPerSec = 0;
  core::EngineClock::time_point lastEngineTime{};
  core::EngineClock::time_point lastFpsUpdateAt{};

  while (canContinue) {
    if (!processEvents()) {
      return;
    }

    const auto currentEngineTime = _guts->engineClock.now();
    const core::FrameTimes frameTime{_guts->logicClock.now(), currentEngineTime - lastEngineTime};
    lastEngineTime = currentEngineTime;

    if (!_renderPasses.empty()) {
      const auto beforeUpdate = _guts->engineClock.now();
      for (auto &pass : _renderPasses) {
        // Update the entire scene.
        pass.scene->update(frameTime);
      }
      const auto afterUpdate = _guts->engineClock.now();

      for (auto &pass : _renderPasses) {
        // Render the scene.
        _guts->renderer->render(pass);
      }
      const auto afterRender = _guts->engineClock.now();

      // Publish perf counters.
      for (auto &callback : _perfCallbacks) {
        callback(
          core::toFloatMs(afterUpdate - beforeUpdate),
          core::toFloatMs(afterRender - afterUpdate),
          framesPerSec
        );
      }
    }

    SDL_GL_SwapWindow(_guts->window->Get());

    if (currentEngineTime - lastFpsUpdateAt >= std::chrono::seconds(1)) {
      framesPerSec = framesDrawn;
      lastFpsUpdateAt = currentEngineTime;
      framesDrawn = 0;
    }
    framesDrawn++;

    SDL_Delay(1);
  }  // while (canContinue)
}

bool Keg::processEvents() {
  _guts->inputController.inputBuffer().update();
  for (const auto& event : _guts->inputController.inputBuffer().frameEvents()) {
    switch (event.type) {
      case SDL_QUIT:
        return false;

      case SDL_KEYDOWN: {
        _guts->inputController.runCallbacksFor(event.key.keysym.sym);
        break;
      }

      case SDL_WINDOWEVENT: {
        switch (event.window.event) {
            // At least on Mac OS with HIDPI screens (but might be also true for OSes), actual
            // pixel size is double that of initially requested window size, so update the viewport
            // now that we know the actual pixel dimensions.
          case SDL_WINDOWEVENT_SHOWN: [[fallthrough]];

          // SDL_WINDOWEVENT_RESIZED is sent as a result of external event.
          // But this one will always come:
          case SDL_WINDOWEVENT_SIZE_CHANGED: {
            const auto drawableSize = _guts->window->GetDrawableSize();
            _guts->renderer->setViewportSize(drawableSize.x, drawableSize.y);
            break;
          }
        }  // switch (event.window.event);
        break;
      }  // case SDL_WINDOWEVENT
    }  // switch (event.type)
  }
  return true;
}

}  // namespace keg
