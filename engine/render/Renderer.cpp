#include "Renderer.hpp"

#include <g3log/g3log.hpp>
#include <glbinding/glbinding.h>
#include <glm/gtx/transform.hpp>
#include <SDL.h>

#include <kgl/bindings.hpp>
#include <kgl/exceptions.hpp>
#include <sg/CameraNode.hpp>
#include <sg/Scene.hpp>
#include <util/enums.hpp>
#include <util/fs_utils.hpp>
#include <util/meta.hpp>


namespace {

#if (DEFERRED_SHADING)
  // TODO: Embed these into a render pass object
  static const float unitQuad[]{
    // 0      1     2 |   3     4
    // POSITIONS      | Texture Coords
    // X      Y     Z |   U     V
    -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,  // top-left
    -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,  // bottom-left
     1.0f,  1.0f, 0.0f, 1.0f, 1.0f,  // top-right
     1.0f, -1.0f, 0.0f, 1.0f, 0.0f,  // bottom-right
  };
#endif

}  // namespace


namespace keg::render {

// PUBLIC:

Renderer::Renderer() {
  // Initialize bindings. Must be done before any calls to ::gl functions are made.
  glbinding::initialize(
    [](const char * name) {
      return reinterpret_cast<glbinding::ProcAddress>(SDL_GL_GetProcAddress(name));
    }
  );

  {
    LOG(DEBUG) << "GL vendor: " << ::gl::glGetString(::gl::GL_VENDOR);
    LOG(DEBUG) << "GL renderer: " << ::gl::glGetString(::gl::GL_RENDERER);
    LOG(DEBUG) << "GLSL version: " << ::gl::glGetString(::gl::GL_SHADING_LANGUAGE_VERSION);
  }
  {
    int maxVerts = 0;
    ASSERT_GL_OK(::gl::glGetIntegerv(::gl::GL_MAX_ELEMENTS_VERTICES, &maxVerts));
    LOG(DEBUG) << "Max verts: " << maxVerts;
  }

  // FIXME: Resources dir path should be passed from the outside.
  const std::string shaders_res_dir = "engine/res/shaders";

  // == Compile shaders.

  auto skinningVS = std::make_shared<gl::VertexShader>(
    util::readTextFile(shaders_res_dir + "/skinning_vs.glsl")
  );  // Use this for models, including bone-animated ones.

  auto vertexPassoverVS = std::make_shared<gl::VertexShader>(
    util::readTextFile(shaders_res_dir + "/pos_uv_passover_vs.glsl")
  );  // Use this for simple vertex position and UVs passover.

  _textShaders.emplace(
    gl::Program::ShadersInitList{
      vertexPassoverVS,
      std::make_shared<gl::FragmentShader>(
        util::readTextFile(shaders_res_dir + "/text_fs.glsl")
      )
    }
  );

#if (!DEFERRED_SHADING)
  auto directRenderFS = std::make_shared<gl::FragmentShader>(
    util::readTextFile(shaders_res_dir + "/direct_render_fs.glsl")
  );
  _directRenderingShaders.emplace(
    gl::Program::ShadersInitList{skinningVS, directRenderFS}
  );

#else  // Deferred shading enabled:
  auto gBufferFS = std::make_shared<gl::FragmentShader>(
    util::readTextFile(shaders_res_dir + "/gbuffer_fs.glsl")
  );

  auto deferredShadingFS = std::make_shared<gl::FragmentShader>(
    util::readTextFile(shaders_res_dir + "/lighting_fs.glsl")
  );

  _geometryPassShaders.emplace(
    gl::Program::ShadersInitList{skinningVS, gBufferFS}
  );
  _lightingPassShaders.emplace(
    gl::Program::ShadersInitList{vertexPassoverVS, deferredShadingFS}
  );

  // Used for deferred shading. Actual buffer texture attachment happens later, once the drawable
  // size is known - when the window is shown.
  _gBuffer.emplace();

  _quadVBO.emplace(
    absl::MakeConstSpan(unitQuad),
    gl::StrideLayout::AttributesList{
      {gl::vertex_attributes::POSITIONS, 3},  // X, Y, Z
      {gl::vertex_attributes::UVS,       2},  // U, V
    }
  );
  _quadVAO.emplace();
  _quadVAO->bind();
  _quadVAO->attachVertexBuffer(*_quadVBO);
#endif  // #if (!DEFERRED_SHADING)

  ASSERT_GL_OK(::gl::glDepthFunc(::gl::GL_LESS));
  ASSERT_GL_OK(::gl::glEnable(::gl::GL_DEPTH_TEST));
}

void Renderer::setShadingMode(const ShadingMode mode) {
  _lightingPassShaders->makeCurrent();
  _lightingPassShaders->setUniformVar("shadingMode", util::enumIntValue(mode));
}

void Renderer::useLighting(const bool lighting) {
  _lightingPassShaders->makeCurrent();
  _lightingPassShaders->setUniformVar("useLights", lighting);
}

void Renderer::setViewportSize(int width, int height) {
  // TODO: Validate parameters.
  _viewportWidth = width;
  _viewportHeight = height;

  ASSERT_GL_OK(::gl::glViewport(0, 0, _viewportWidth, _viewportHeight));

#if (DEFERRED_SHADING)
  _gBuffer->setSize(width, height);

  _lightingPassShaders->makeCurrent();
  _lightingPassShaders->setUniformVar("gPosition", _gBuffer->posBufferTextureUnitIndex());
  _lightingPassShaders->setUniformVar("gNormal", _gBuffer->normalBufferTextureUnitIndex());
  _lightingPassShaders->setUniformVar("gAlbedoSpec", _gBuffer->colorsBufferTextureUnitIndex());

#endif  // #if (DEFERRED_SHADING)
}

glm::mat4 Renderer::projection(const Projection& projection) const {
  return std::visit(
    meta::overloaded{
      [this] (const PerspectiveView& perspective) {
        return glm::perspective(
          glm::radians(perspective.verticalFieldOfView),
          float(_viewportWidth) / _viewportHeight,
          perspective.nearCullPlane,
          perspective.farCullPlane
        );
      },
      [this] (const OrthogonalView& perspective) {
        // NOTE: This implies we'll be setting object pos/size in pixels!
        // This might be desired (HUD and text positioning), or it might be not.
        // Maybe it's best to just set the aspect ratio here, and then use some relative dimensions
        // from 0 to 1.
        return glm::ortho(0.0f, float(_viewportWidth), 0.0f, float(_viewportHeight));
      },
    },
    projection
  );
}

void Renderer::render(const RenderPass& pass) {
  switch(pass.kind) {
    case RenderPass::Kind::World: {
      renderWorld(pass);
      break;
    }
    case RenderPass::Kind::HUD: {
      renderHUD(pass);
      break;
    }
  }
}

// PRIVATE:

void Renderer::renderWorld(const RenderPass& pass) {
  if (!pass.scene) {
    return;
    // TODO: Report error.
  }

  // == Render geometry
#if (DEFERRED_SHADING)
  // We'll render visible scene part into the gBuffer.
  _gBuffer->bind(::gl::GL_FRAMEBUFFER);
  _geometryPassShaders->makeCurrent();
#else
  _directRenderingShaders->makeCurrent();
#endif  // #if (DEFERRED_SHADING)
  ::gl::glFinish();

  // TODO: This color has to come from somewhere else.
  ASSERT_GL_OK(::gl::glClearColor(0.7f, 0.7f, 0.7f, 1.0f));
  ASSERT_GL_OK(::gl::glClear(::gl::GL_COLOR_BUFFER_BIT | ::gl::GL_DEPTH_BUFFER_BIT));
  ASSERT_GL_OK(::gl::glEnable(::gl::GL_CULL_FACE));
  // Enable writes into the depth buffer.
  // ASSERT_GL_OK(::gl::glDepthMask(::gl::GL_TRUE));

  // Render the scene.
  gl::ShaderUniformsSink& uniforms =
#if (DEFERRED_SHADING)
    *_geometryPassShaders
#else
    *_directRenderingShaders
#endif
  ;
  pass.scene->render(projection(pass.projection), uniforms);

#if (DEFERRED_SHADING)
  // Do shading/lighting. Combine gBuffer parts and light it up.
  keg::gl::FrameBuffer::getDefaultFrameBuffer().bind(::gl::GL_FRAMEBUFFER);
  ASSERT_GL_OK(::gl::glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
  ASSERT_GL_OK(::gl::glClear(::gl::GL_COLOR_BUFFER_BIT | ::gl::GL_DEPTH_BUFFER_BIT));
  _lightingPassShaders->makeCurrent();
  _lightingPassShaders->setUniformVar("viewPos", pass.scene->camera()->worldPosition());
  _gBuffer->useAllColorTextures();
  // TODO: Upload lights data.

  // TODO: Maybe wrap this into a gl::Mesh to hide these operations?
  _quadVAO->bind();
  ASSERT_GL_OK( ::gl::glDrawArrays(::gl::GL_TRIANGLE_STRIP, 0, 4) );

  // == Transfer the scene depth information to default framebuffer.
  const glm::ivec4 blitRect{0, 0, _viewportWidth, _viewportHeight};
  keg::gl::FrameBuffer::getDefaultFrameBuffer().blitFrom(
      *_gBuffer, ::gl::GL_DEPTH_BUFFER_BIT, blitRect, blitRect, ::gl::GL_NEAREST
  );
  keg::gl::FrameBuffer::getDefaultFrameBuffer().bind(::gl::GL_FRAMEBUFFER);

#endif  // #if (DEFERRED_SHADING)
}

void Renderer::renderHUD(const RenderPass& pass) {
  // This is needed for the text characters not to appear as just colored rectangles.
  ::gl::glEnable(::gl::GL_BLEND);
  // Disable writes into the depth buffer to avoid nearby text fragments flickering.
  ::gl::glDepthMask(::gl::GL_FALSE);
  ::gl::glBlendFunc(::gl::GL_SRC_COLOR, ::gl::GL_ONE_MINUS_SRC_ALPHA);
  ASSERT_GL_OK(::gl::glDisable(::gl::GL_CULL_FACE));  // Otherwise ui::Text dissappears

  _textShaders->makeCurrent();
  pass.scene->render(projection(pass.projection), *_textShaders);

  // This was set just for the text rendering and needs to be disabled before the next frame.
  ::gl::glDisable(::gl::GL_BLEND);
  ::gl::glDepthMask(::gl::GL_TRUE);
}

}  // namespace keg::render
