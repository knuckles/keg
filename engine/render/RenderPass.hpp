#pragma once

#include <memory>
#include <variant>


namespace keg {

namespace sg {
class Scene;
}

// Different kinds of projections (camera views).

struct PerspectiveView {
  float verticalFieldOfView;
  float nearCullPlane;
  float farCullPlane;
};

struct OrthogonalView {};

using Projection = std::variant<PerspectiveView, OrthogonalView>;


/// TODO: Render pass should include:
/// - some descriptor of rendering parameters (RenderPassKind currently)
/// - scene (contains camera)
/// - projection kind
/// - render target (buffer, screen, etc)
struct RenderPass {

  /// RenderPass::Kind defines a number of rendering parameters set for a render pass.
  /// This is a pretty cheap (i.e "crappy") and weak abstraction as it hardcodes too much specifics.
  /// TODO: Come up with something smarter. Later. For now this is needed to implement the idea of
  /// different passes.
  enum class Kind {
    // "Normal" render pass. Intended for typical 3D objects.
    // Uses perspective projection.
    World,

    // HUD/UI render pass. Intended for text and similar "flat" visuals.
    // This uses orthographic projection and treats node positions as pixels.
    HUD,
  };

  Kind kind;
  std::shared_ptr<sg::Scene> scene;
  Projection projection;
};

}  // namespace keg
