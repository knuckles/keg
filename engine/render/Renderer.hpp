#pragma once

#include <optional>

#include <glm/mat4x4.hpp>

#include <kgl/Program.hpp>
// #if (DEFERRED_SHADING)
#include <kgl/VertexBuffer.hpp>
#include <kgl/VertexArrayObject.hpp>
#include <render/GBuffer.hpp>
#include <render/RenderPass.hpp>
// #endif
#include <util/macros.hpp>


namespace keg::render {

// TODO: These must match constants in SHADER_DEFS in CMake which are propagated into shader code.
enum class ShadingMode {
  MATERIAL, // SHADING_MODE_VIS_NORMALS
  VIS_NORMALS, // SHADING_MODE_MATERIAL
  VIS_POSITIONS,  // SHADING_MODE_VIS_POSITION
  INVALID,
};

class Renderer {
public:
  Renderer();
  NON_COPYABLE(Renderer)
  NON_MOVABLE(Renderer)

  // Rendering debug knobs.
  void setShadingMode(const ShadingMode mode);
  void useLighting(const bool lighting);

  void setViewportSize(int width, int height);
  void render(const RenderPass& pass);

private:
  glm::mat4 projection(const Projection& projection) const;

  void renderWorld(const RenderPass& pass);
  void renderHUD(const RenderPass& pass);

  std::optional<gl::Program> _textShaders;

  // Used in DEFERRED_SHADING mode
  std::optional<gl::Program> _geometryPassShaders;
  std::optional<gl::Program> _lightingPassShaders;
  std::optional<render::GBuffer> _gBuffer;
  // Just a rectangle to draw the final image texture onto.
  std::optional<gl::VertexBuffer> _quadVBO;
  std::optional<gl::VertexArrayObject> _quadVAO;

  // Used in direct rendering mode:
  std::optional<gl::Program> _directRenderingShaders;

  // OpenGL uses ::gl::GLsizei which is int so we stick to the same here.
  int _viewportWidth = 1;
  int _viewportHeight = 1;
};

}  // namespace keg::render {
