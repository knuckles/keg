#include "GBuffer.hpp"

#include <fmt/core.h>

#include <kgl/bindings.hpp>
#include <util/enums.hpp>


namespace keg::render {

void GBuffer::setSize(int width, int height) {
  std::ignore = detachAllColorTextures();
  std::ignore = detachAllRenderBuffers();

  _posBufferTextureUnitIndex = attachColorTexture(
      gl::Texture{
        width,
        height,
        /* internal */ ::gl::GL_RGB16F,
        /* color    */ ::gl::GL_RGB,
        /* numeric  */ ::gl::GL_FLOAT
      }
  );
  _normalBufferTextureUnitIndex = attachColorTexture(
      gl::Texture{
        width,
        height,
        /* internal */ ::gl::GL_RGB16F,
        /* color    */ ::gl::GL_RGB,
        /* numeric  */ ::gl::GL_FLOAT
      }
  );
  _colorsBufferTextureUnitIndex = attachColorTexture(
      gl::Texture{
        width,
        height,
        /* internal */ ::gl::GL_RGBA,
        /* color    */ ::gl::GL_RGBA,  // RGB for albedo + Alpha channel for specular intensity.
        /* numeric  */ ::gl::GL_UNSIGNED_BYTE
      }
  );
  attachDepthRenderBuffer(
      gl::RenderBuffer{
        width,
        height,
        ::gl::GL_DEPTH_COMPONENT
      }
  );
  const auto gBufStatus = status();
  if (gBufStatus != ::gl::GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error{
      fmt::format("gBuffer status incomplete: {}", util::enumIntValue(gBufStatus))
    };
  }
}  // setSize

}  // namespace keg::render
