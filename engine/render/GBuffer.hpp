#pragma once

#include <kgl/FrameBuffer.hpp>


namespace keg::render {

// GBuffer, or geometry buffer, is a kind of frame buffer object which accumulates geometry render
// pass output, typically for a subsequent shading render pass.
class GBuffer : public gl::FrameBuffer {
public:
  void setSize(int width, int height);

  ::gl::GLint posBufferTextureUnitIndex() const { return _posBufferTextureUnitIndex; }
  ::gl::GLint normalBufferTextureUnitIndex() const { return _normalBufferTextureUnitIndex; }
  ::gl::GLint colorsBufferTextureUnitIndex() const { return _colorsBufferTextureUnitIndex; }

private:
  ::gl::GLint _posBufferTextureUnitIndex = -1;
  ::gl::GLint _normalBufferTextureUnitIndex = -1;
  ::gl::GLint _colorsBufferTextureUnitIndex = -1;
};

}  // namespace keg::render
