// Input public API.

#pragma once
#include <functional>


namespace keg::input {
using InputCallback = std::function<void()>;
}
