#include "InputController.hpp"


namespace keg::input {

core::CallbackId InputController::addInputCallback(SDL_Keycode key, InputCallback callback) {
  const core::CallbackId callbackId =
      _inputCallbacks.empty() ? 1 : _inputCallbacks.crbegin()->second.first + 1;
  _inputCallbacks.insert({key, {callbackId, std::move(callback)}});
  return callbackId;
}

bool InputController::removeInputCallback(SDL_Keycode key, core::CallbackId callbackId) {
  const auto iterPair = _inputCallbacks.equal_range(key);
  const auto cbIter = std::find_if(
      iterPair.first, iterPair.second,
      [&callbackId](const auto& mapEntry) { return mapEntry.second.first == callbackId; }
  );
  if (cbIter != _inputCallbacks.end()) {
    _inputCallbacks.erase(cbIter);
    return true;
  }
  return false;
}

void InputController::runCallbacksFor(const SDL_Keycode keyCode) {
  const auto cbIters = _inputCallbacks.equal_range(keyCode);
  for (auto it = cbIters.first; it != cbIters.second; ++it) {
    auto& cbFunc = it->second.second;
    cbFunc();  // FIXME: Handle possible exceptions?
  }
}

}  // namespace keg::input
