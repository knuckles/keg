#include "InputBuffer.hpp"


namespace keg::input {

InputBuffer::InputBuffer() {
}

void InputBuffer::update() {
  _frameEvents.clear();
  SDL_Event event;
  // FIXME: These are not just input events, but much more. Split processing based on event kinds.
  while (SDL_PollEvent(&event)) {
    _frameEvents.push_back(event);
  }
}

}  // namespace keg::input
