#pragma once

#include <vector>

#include <SDL_events.h>


namespace keg::input {

class InputBuffer {
public:
  InputBuffer();

  /// Input events for the current frame.
  const std::vector<SDL_Event>& frameEvents() const { return _frameEvents; }

  void update();

private:
  std::vector<SDL_Event> _frameEvents;
};

}  // namespace keg::core
