#pragma once
#include <map>

#include <SDL_keycode.h>  // For input callback registration.

#include <base/types.hpp>
#include <input/InputBuffer.hpp>  // TODO: private
#include <input/pub.hpp>


namespace keg::input {

class InputController {
public:
  InputBuffer& inputBuffer() {return _inputBuffer;}

  core::CallbackId addInputCallback(SDL_Keycode key, InputCallback callback);

  bool removeInputCallback(SDL_Keycode key, core::CallbackId callbackId);

  void runCallbacksFor(const SDL_Keycode keyCode);

private:
  InputBuffer _inputBuffer;
  std::multimap<SDL_Keycode, std::pair<core::CallbackId, InputCallback>> _inputCallbacks;
};

}  // namespace keg::input
