#version 330 core

uniform mat4 modelTransform;
uniform mat4 mvpTransform;

// Shows whether `boneIds`and `boneWeights` attributes are present.
// In that case vertex is transformed by the matrices looked up inside the `boneTransforms`.
uniform bool hasSkeleton = false;

// TODO: Instead of uploading a mat4 for each bone, upload a pair of quaternion and offset.
// A quat uses a vec4 and offset uses a vec4.
// See https://cs.gmu.edu/~jmlien/teaching/cs451/uploads/Main/dual-quaternion.pdf
// See https://users.cs.utah.edu/~ladislav/kavan07skinning/kavan07skinning.pdf
// See https://github.com/chinedufn/skeletal-animation-system
uniform mat4 boneTransforms[@MAX_BONES_PER_MESH@];

layout(location = @VS_LOC_VPOS@) in vec3 vertexPosition;
layout(location = @VS_LOC_VCOL@) in vec3 vertexColor;
layout(location = @VS_LOC_VNORM@) in vec3 vertexNormal;
layout(location = @VS_LOC_VUV@) in vec2 vertexUV;
// The following assumes each vertex is only affected by at most 4 bones:
layout(location = @VS_LOC_BONE_IDS@) in ivec4 boneIds;  // 4 indices into the `boneTransforms`.
layout(location = @VS_LOC_BONE_WEIGHTS@) in vec4 boneWeights;  // 4 bone weighs.

out vec3 fragmentPosition_world;  // Vertex position, in world space.
out vec3 fragmentNormal_world;  // Normal of the the vertex, in world space. OR LOCAL?
out vec3 fragmentColor;
out vec2 fragmentUV;


void main() {
  vec4 origPos4 = vec4(vertexPosition, 1.0);
  vec4 origNormal4 = vec4(normalize(vertexNormal), 0.0);

  vec4 skinnedPos = vec4(0);
  vec4 skinnedNormal = vec4(0);
  if (!hasSkeleton) {
    skinnedPos = origPos4;
    skinnedNormal = origNormal4;
  }
  else {
    for (int bi = 0; bi < 4; bi++) {
      mat4 effTransform = boneTransforms[boneIds[bi]] * boneWeights[bi];

      skinnedPos += effTransform * origPos4;
      skinnedNormal += vec4(normalize((effTransform * origNormal4).xyz), 0);
    }
  }

  gl_PointSize = 5.0;  // Debugging dots.

  gl_Position = mvpTransform * skinnedPos;  // Screen position.
  fragmentPosition_world = (modelTransform * skinnedPos).xyz;
  fragmentNormal_world = normalize((modelTransform * skinnedNormal).xyz);

  fragmentColor = vertexColor;
  fragmentUV = vertexUV;
}
