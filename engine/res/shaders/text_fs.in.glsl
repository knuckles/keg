#version 330 core

/// This fragment shader paints text characters by utilizing a font atlas of characters represented
/// as a multi-channel signed distance field.

uniform sampler2D fontAtlas;
uniform vec2 msdfUnit;
uniform vec4 textColor;  // Character fill color.

in vec2 fragmentUV;  // Texture coordinates of a character being drawn.
out vec4 color;

float median(float r, float g, float b) {
    return max(min(r, g), min(max(r, g), b));
}

void main() {
  vec3 sample = texture(fontAtlas, fragmentUV).rgb;
  float sigDist = median(sample.r, sample.g, sample.b) - 0.5;
  sigDist *= dot(msdfUnit, 0.5 / fwidth(fragmentUV));
  float opacity = clamp(sigDist + 0.5, 0.0, 1.0);
  color = vec4(textColor.rgb, opacity);
}
