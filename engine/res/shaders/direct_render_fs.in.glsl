#version 330 core

// Texture settings.
uniform int hasAlbedoTexture = 0;
uniform sampler2D albedoTexture;
uniform int hasSpecularTexture = 0;
uniform sampler2D specularTexture;

// Material properties.
uniform vec3 matAmbientMul = vec3(1, 1, 1);

in vec3 fragmentColor;
in vec2 fragmentUV;

out vec3 color;


void main() {
  // Diffuse : "color" of the object
  vec3 diffuseColor = hasAlbedoTexture > 0 ?
      texture(albedoTexture, fragmentUV).rgb :
      fragmentColor;

  // Ambient : simulates indirect lighting
  vec3 ambientColor = matAmbientMul * diffuseColor;

  // Specular : reflective highlight, like a mirror
  vec3 specularColor = hasSpecularTexture > 0 ?
      texture(specularTexture, fragmentUV).rgb :
      vec3(0, 0, 0);

  color = diffuseColor;
  color += ambientColor;
  color += specularColor;
}
