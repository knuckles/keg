#version 330 core

/// This fragment shader performs scene lighting from the components of a G-buffer, expected to
/// provide 3 different components (textures of vec4). See below:
uniform sampler2D gPosition;    // Fragment position. World space.
uniform sampler2D gNormal;      // Fragment normal. World space.
uniform sampler2D gAlbedoSpec;  // Fragment albedo {r, g, b} and specular {, a} values.

uniform vec3 matAmbientMul = vec3(1, 1, 1);  // Material property.

uniform int shadingMode = @SHADING_MODE_MATERIAL@;
uniform bool useLights = true;

// Lights data.
struct Light {
    vec3 position;
    vec3 color;
};
uniform Light lights[@MAX_LIGHTS@];

// Camera position. World space.
uniform vec3 viewPos;

in vec2 fragmentUV;  // Coordinates in the G-buffer textures.

out vec4 fragColor;


void shadeFromMaterial() {
  // Retrieve data from G-buffer
  vec3 fragPos = texture(gPosition, fragmentUV).rgb;
  vec3 fragNormal = texture(gNormal, fragmentUV).rgb;
  vec3 fragAlbedo = texture(gAlbedoSpec, fragmentUV).rgb;
  float fragSpecular = texture(gAlbedoSpec, fragmentUV).a;  // TODO: Unused

  // Calculate lighting from gBuffer values
  vec3 color = fragAlbedo * matAmbientMul;

  if (useLights) {
    // Add effect from all lights.
    for (int i = 0; i < lights.length(); ++i) {
      vec3 lightDir = normalize(lights[i].position - fragPos);
      vec3 diffuse = max(dot(fragNormal, lightDir), 0.0) * fragAlbedo * lights[i].color;
      color += diffuse;
    }

    vec3 viewDir = normalize(viewPos - fragPos);
    if (length(fragNormal) > 0) {
      color *= (1.0 - max(dot(viewDir, fragNormal), 0.0));
    }
  }

  fragColor = vec4(color, 1.0);
}

void main() {
  if (shadingMode == @SHADING_MODE_MATERIAL@) {
    shadeFromMaterial();
  }
  else if (shadingMode == @SHADING_MODE_VIS_NORMALS@) {
    fragColor = texture(gNormal, fragmentUV);
  }
  else if (shadingMode == @SHADING_MODE_VIS_POSITION@) {
    fragColor = texture(gPosition, fragmentUV);
  }
}
