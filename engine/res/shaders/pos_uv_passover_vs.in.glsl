#version 330 core

/// This vertex shader simply passes the positions and texture coordinates to the fragment shader.
/// This can be used for deferred shading of the scene from gBuffer components.
/// Other use-case is rendering of simple static meshes (e.g. text quads).

uniform mat4 modelTransform = mat4(1);
uniform mat4 mvpTransform = mat4(1);

layout(location = @VS_LOC_VPOS@) in vec3 vertexPosition;
layout(location = @VS_LOC_VUV@) in vec2 vertexUV;

out vec2 fragmentUV;

void main() {
  gl_Position = mvpTransform * vec4(vertexPosition, 1.0);
  fragmentUV = vertexUV;
}
