#version 330 core


/// Material properties. @c Material.h

uniform float reflectivity = 0;
uniform float shininess = 0;

uniform vec3 diffuseColor = vec3(0, 0, 0);
uniform vec3 ambientColor = vec3(0, 0, 0);
uniform vec3 specularColor = vec3(0, 0, 0);
uniform vec3 emissiveColor = vec3(0, 0, 0);

uniform int hasAlbedoTexture = 0;
uniform sampler2D albedoTexture;
uniform int hasSpecularTexture = 0;
uniform sampler2D specularTexture;

/// GBuffeer data.
in vec3 fragmentPosition_world;  // Vertex position, in world space.
in vec3 fragmentNormal_world;  // Normal of the the vertex, in world space.
in vec3 fragmentColor;
in vec2 fragmentUV;


// All these will be rendered into separate textures attached to a framebuffer.
// TODO: Sync locations with C++ code.
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;


void main() {
  // == Write out to the gBuffer layers.

  gPosition = fragmentPosition_world;
  gNormal = fragmentNormal_world;

  if (hasAlbedoTexture != 0) {
    // and the diffuse per-fragment color
    gAlbedoSpec.rgb = texture(albedoTexture, fragmentUV).rgb;
  }
  else {
    // Otherwise just use mesh colors and material properties.
    gAlbedoSpec.rgb = fragmentColor + diffuseColor;
  }

  if (hasSpecularTexture != 0) {
    // store specular intensity in gAlbedoSpec's alpha component
    gAlbedoSpec.a = texture(specularTexture, fragmentUV).r;
  }
  else {
    // Otherwise just use material properties.
    gAlbedoSpec.a = length(specularColor);
  }
}
