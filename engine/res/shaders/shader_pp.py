#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This utility is meant to do simple preprocessing of shader files in a way
# similar to what C preprocessor does. Main motivation is to be able to use
# include directives in shader sources.
#
# Supported syntax:
# `#include file_path` inserts the contents of file at path `file_path`.
# Every occurence of `@MYVAR@` is replaced according to definitions provided via command line args.
# WARNING: ONLY ONE (LAST) OCCURENCE PER LINE IS PROCESSED!

import argparse
import pathlib
import re
import sys


parser = argparse.ArgumentParser(description='Preprocess shader file.')
parser.add_argument('inFile', metavar='SRC', type=argparse.FileType('r'),
  help='File to process'
)
parser.add_argument('--outFile', metavar='DEST', type=argparse.FileType('w'),
  default=sys.stdout, help='Path to output file'
)
parser.add_argument('-I', metavar='DIR', action='append', dest='includeDirs',
  help='Add directory to include search path'
)
parser.add_argument('-D', metavar='DEF', action='append', dest='defs',
  help='Define variable value (usage `@MYVAR@` in the code and then `-D MYVAR=42`)'
)

args = parser.parse_args()

defsMap = {}
if args.defs:
  for varDef in args.defs:
    varName, varVal = varDef.split("=")
    defsMap[varName] = varVal


def pp_include(match):
  def locate(filePath):
    for incDirPath in args.includeDirs:
      incFilePath = pathlib.Path(incDirPath, filePath)
      if incFilePath.is_file():
        return incFilePath
    raise IOError('Could not locate included file: "%s"' % filePath)

  with locate(match.group(1)).open('r') as includedFile:
    for line in includedFile.readlines():
      yield line


def pp_var(match):
  varName = match.group(2)
  if varName not in defsMap:
    raise Exception("Undefined variable '%s'" % varName)
  varVal = defsMap[varName]
  return match.string[:match.start(1)] + varVal + match.string[match.end(1):]


RULES = {
  re.compile('^#include\\s+(.*)'): pp_include,
  re.compile('.*(@([A-Za-z_]+)@).*'): pp_var,
}


def pp(line):
  if not line:
    return line
  for expr, handler in RULES.items():
    match = expr.match(line)
    if not match:
      continue
    line = handler(match)

  return line


with args.inFile as src, args.outFile as dest:
  for line in src.readlines():
    for expanded in pp(line):
      dest.write(expanded)
