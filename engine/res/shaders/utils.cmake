set(KGL_DIR "${CMAKE_CURRENT_LIST_DIR}")


function(add_preprocess_shader_command)
  cmake_parse_arguments(PS "" "NAME" "DEFS" ${ARGN})

  set(inFile ${PS_NAME}.in.glsl)
  set(outFile ${PS_NAME}.glsl)
  set(incDirs ${CMAKE_CURRENT_LIST_DIR})

  if(NOT IS_ABSOLUTE ${inFile})
    set(inFile "${CMAKE_CURRENT_SOURCE_DIR}/${inFile}")
  endif()
  if(NOT IS_ABSOLUTE ${outFile})
    set(outFile "${CMAKE_CURRENT_BINARY_DIR}/${outFile}")
  endif()

  set(incFlags )
  foreach(inc ${incDirs})
    list(APPEND incFlags "-I=${inc}")
  endforeach()

  set(defFlags )
  foreach(def ${PS_DEFS})
    list(APPEND defFlags "-D=${def}")
  endforeach()

  add_custom_command(
    OUTPUT ${outFile}
    COMMAND ${KGL_DIR}/shader_pp.py
      ${incFlags}
      ${defFlags}
      --outFile ${outFile}
      ${inFile}
    DEPENDS ${KGL_DIR}/shader_pp.py ${inFile}
    COMMENT "Preprocessing shader file ${inFile}..."
  )
endfunction()


# Creates a target which preprocesses one or more shader source files.
#
# Preprocessing includes making a number of textual replacements based on the supplied list of key-
# value pairs. For each KEY a substring of "@KEY@" in the source file shall be replaced with the
# associated value.
#
# @param NAME - CMake target name.
# @param SHADER_NAMES - a list of shader files base names. Actual file names must end with an
#     extension of `.in.glsl`.
# @param DEFS - a list of "="-separated key-value pairs to perform replacements in the source files.
# @outputs: a number of preprocessed files (one per source) with extension set to `.glsl`.
# @property KEG_SHADER_FILES - a list of output files of this target.
function(add_preprocess_shaders_target)
  cmake_parse_arguments(PS "" "NAME" "SHADER_NAMES;DEFS" ${ARGN})

  set(inFiles )
  set(outFiles )
  foreach(shader ${PS_SHADER_NAMES})
    list(APPEND inFiles ${shader}.in.glsl)
    list(APPEND outFiles ${shader}.glsl)
    add_preprocess_shader_command(NAME ${shader} DEFS ${PS_DEFS})
  endforeach()
  add_custom_target(${PS_NAME} ALL
    SOURCES ${inFiles}
    DEPENDS ${outFiles}
  )
  set_target_properties(${PS_NAME} PROPERTIES
    KEG_SHADER_FILES "${outFiles}"
  )
endfunction()
