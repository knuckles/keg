# Copies of third-party libraries which cannot be installed via a package manager.

# Force 'assimp' library link as shared.
set(BUILD_SHARED_LIBS ON CACHE BOOL "Build shared libraries")

add_subdirectory(soil)
target_include_directories(SOIL
  INTERFACE
  ${CMAKE_CURRENT_LIST_DIR}/soil/src
)
add_library(SOIL::SOIL ALIAS SOIL)

# Add SDL2 C++ wrapper library:
add_subdirectory(sdl2pp)
set_property(TARGET SDL2pp PROPERTY POSITION_INDEPENDENT_CODE ON)
target_include_directories(SDL2pp
  INTERFACE
  ${SDL2PP_INCLUDE_DIRS}
)

set(MSDF_ATLAS_BUILD_STANDALONE YES)
set(MSDF_ATLAS_USE_SKIA NO)
set(MSDF_ATLAS_USE_VCPKG NO)
set(MSDF_ATLAS_NO_ARTERY_FONT YES)
add_subdirectory(msdf-atlas-gen)
